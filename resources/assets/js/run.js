$(document).ready( function(){
  //attach change listener to currency change select field
  $('#currency').change(function(e){
    $('#currencytag').html($(this).val());
  });

  $('#sendto').change(function(e){
    $.ajax({
      url : 'convert',
      type : 'POST',
      dataType : 'json',
      data : {
        amount : $('#balancetag').html(),
        from_currency : $('#ctag').html(),
        to_currency : $('#sendto').val(),
        send_gateway : $('#funding_instrument').html()
      }
    }).done(function(data){
      //update UI with data
      $('#sendtag').html('Your recipient will receive '+data.result+' via '+
          ($('#sendto').val() == 'VSA' ? 'Bank Account '+$('#bank_name').val() : $('#sendto').val()));

    }).fail(function(err, status){
      console.log(err, status)
    });
    //if field if a bank selection, then toggle bank display
    if($(this).val() == 'VSA') {
      $('.bank').removeClass('d-none');
      $('#bank_name').focus();
    }
    else{
      $('.bank').addClass('d-none');
    }
  });

  $('#sendto').change();
  //attach amount change listener to amount field
  $('#amounttosend').keyup(function(e){
    $('#amounttag').html($(this).val());
  });
});
// Showing page loader
$(window).on('load', function () {
  setTimeout(function () {
    $(".page_loader").fadeOut("fast");
  }, 100);
});
