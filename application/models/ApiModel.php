<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ApiModel extends CI_Model{
    
    protected $table = 'keys';

    public function __construct(){
        $this->load->database();
    }

    //save
    public function insert($api){
       return $this->db
            ->insert($this->table, $api);
    }

    //delete
    public function delete($id){
        return $this->db
                ->set(array('date_deleted',time()))
                ->where('api_key',$id)
                ->update($this->table);
    }

    //get api key
    public function get(){
        return $this->db
            ->where('date_deleted', null)
            ->get($this->table);
    }

    //get keys data by email
    public function getByEmail($email){
        return $this->db
            ->where('email',$email)
            ->get($this->table);
    }

    //update
    public function update($update, $key){
        return $this->db
                ->set($update)
                ->where('api_key', $key)
                ->update($this->table);
    }
}