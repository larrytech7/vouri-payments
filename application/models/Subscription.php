<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subscription extends CI_Model{
    
    protected $table = 'payment_subscriptions';
    protected $key = 'subscription_id';

    public function __construct(){
        $this->load->database();
    }

    //save
    public function insert($data){
       return $this->db
            ->insert($this->table, $data);
    }

    //delete
    public function delete($id){
        return $this->db
                ->set(array('date_deleted',time()))
                ->where($this->key,$id)
                ->update($this->table);
    }

    //get all
    public function getAll($criteria = ['user.delete_time'=> null]){
        return $this->db
            ->join('keys', 'keys.api_key = payment_subscriptions.subscription_api_key')
            ->join('user', 'user.email = keys.email')
            ->where('subscription_delete_time', null)
            ->where($criteria)
            ->get($this->table);
    }

    //get subscriptions
    public function get(){
        return $this->db
            ->where('subscription_delete_time', null)
            ->get($this->table);
    }

    //search
    public function find($criteria){
        $criteria['subscription_delete_time'] = null;
        return $this->db
            ->where($criteria)
            ->get($this->table);
    }

    //update
    public function update($update, $mkey){
        return $this->db
                ->set($update)
                ->where($this->key, $mkey)
                ->update($this->table);
    }
}