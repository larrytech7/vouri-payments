<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contactus extends CI_Model{

    protected $table = 'contact_messages';
    
    public function __construct(){
        $this->load->database();
        date_default_timezone_set('Africa/Douala');
    }
    
    public function getMessage($id){
        return $this->db
                ->where('id', $id)
                ->where('delete_time', NULL)
                ->order_by('created_time', 'DESC')
                ->get($this->table);
    }

    public function save($notif){
       return $this->db
             ->insert($this->table, $notif);
    }
    
    public function delete($id){
        $update = array('delete_time' => time());
		
        return $this->db
            ->set($update)
            ->where('message_id', $id)
            ->update($this->table);
    }
    
    public function update($id, $data){
        return $this->db
            ->set($data)
            ->where('id', $id)
            ->update($this->table);
    }
}