<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paymentrequest extends CI_Model{

    protected $table = 'payment_requests';
    
    public function __construct(){
        $this->load->database();
        date_default_timezone_set('Africa/Douala');
    }
    
    public function getRequests(){
        return $this->db
                ->where('delete_time', NULL)
                ->order_by('create_time', 'DESC')
                ->get($this->table);
    }
    public function getRequestsByEmail($email){
        return $this->db
            ->where('email', $email)
            ->where('delete_time', NULL)
            ->order_by('create_time', 'DESC')
            ->get($this->table);
    }

    public function save($data){
       return $this->db
             ->insert($this->table, $data);
    }
    
    public function delete($id){
        $update = array('delete_time' => time());
		
        return $this->db
            ->set($update)
            ->where('request_id', $id)
            ->update($this->table);
    }
    
    public function update($id, $data){
        return $this->db
            ->set($data)
            ->where('request_id', $id)
            ->update($this->table);
    }
}