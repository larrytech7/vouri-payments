<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends CI_Model{

    protected $table = 'notification';
    
    public function __construct(){
        $this->load->database();
        date_default_timezone_set('Africa/Douala');
    }
    
    public function getNotifications($id){
        return $this->db
                ->where('receiver', $id)
                ->where('delete_time', NULL)
                ->order_by('created_time', 'DESC')
                ->get($this->table);
    }

    public function save($notif){
       return $this->db
             ->insert($this->table, $notif);
    }
    
    public function delete($id){
        $update = array('delete_time' => time());
		
        return $this->db
            ->set($update)
            ->where('message_id', $id)
            ->update($this->table);
    }
    
    public function update($id, $data){
        return $this->db
            ->set($data)
            ->where('message_id', $id)
            ->update($this->table);
    }
}
