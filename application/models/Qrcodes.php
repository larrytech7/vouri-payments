<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Qrcodes extends CI_Model{
    
    protected $table = 'qr_codes_generated';
    protected $key = 'qr_id';

    public function __construct(){
        $this->load->database();
    }

    //save
    public function insert($data){
       return $this->db
            ->insert($this->table, $data);
    }

    //delete
    public function delete($id){
        return $this->db
                ->set(array('delete_time',date('Y-m-d H:i:s')))
                ->where($this->key,$id)
                ->update($this->table);
    }

    //get all data
    public function get(){
        return $this->db
            ->where('delete_time', null)
            ->get($this->table);
    }
    //get particular id
    public function getById($id){
        return $this->db
            ->where($this->key, $id)
            ->where('delete_time', null)
            ->get($this->table);
    }


    //update
    public function update($update, $id){
        return $this->db
                ->set($update)
                ->where($this->key, $id)
                ->update($this->table);
    }
}