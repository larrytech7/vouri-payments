<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Model{
    
    protected $table = 'user';

    protected $email;
    protected $name;
    protected $auth_token;
    protected $status; // 0 or 1
    protected $user_id;
    protected $create_time;
    protected $delete_time = null;
    protected $update_time;

    public function __construct(){
        $this->load->database();
    }
    
    //determine if user login is correct
    public function authenticate($email, $password){
        $result = $this->db->from($this->table)
            ->select('user_id, role, name, phone, email, create_time, update_time, status, logged_in, address, city, country, national_idcard, monthly_income, occupation, kyc_level')
        ->where('email', $email)
        ->where('password', $password)
            ->where('auth_token',NULL)
            ->where('delete_time', NULL)
        ->get();
        return $result;
    }

    //save new user
    public function save($user){
        return $this->db
                ->insert($this->table, $user);
    }

    //list all users
    public function getUsers(){
        return $this->db
            ->select('user_id, role, name, phone, email, create_time, update_time, status, logged_in, address, city, country, national_idcard, monthly_income, occupation, kyc_level')
            ->from($this->table)
            ->where('delete_time', NULL)
            ->order_by('user_id', 'DESC')
            ->get();
    }

    //delete user
    public function delete($id){
        return $this->db
                ->where('user_id',$id)
                ->delete($this->table);
    }

    //get user by EMAIL
    public function getUserByEmail($email){
        return $this->db
            ->select('user_id, role, name, phone, email, create_time, update_time, status, logged_in, address, city, country, national_idcard, monthly_income, occupation, kyc_level')
            ->where('email', $email)
            ->get($this->table);
    }

    public function getUserById($id){
        return $this->db
            ->select('user_id, role, name, phone, email, create_time, update_time, status, logged_in, address, city, country, national_idcard, monthly_income, occupation, kyc_level')
            ->where('user_id', $id)
            ->get($this->table);
    }
    
    //update user
    public function update($id, $user){
        $user['update_time'] = date('Y-m-d H:i:s', time());
        return $this->db
                ->set($user)
                ->where('user_id', $id)
                ->update($this->table);
    }

    /**
     * Return transactions done for the current month to check against KYC compliance
     * @param $user_id id of user to return information for
     */
    public function getUserPci($user_id){
        return $this->db
            ->select('user_id, transaction_id, amount, currency, kyc_level, monthly_income')
            ->from('user')
            ->join('transactions','transactions.sender_id = user.user_id')
            ->where('user_id', $user_id)
            ->where('transactions.delete_time', null)
            ->where('transactions.status', 1)
            ->like('transactions.update_time', date('Y-m'))
            ->get();
    }
}