<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Instrument extends CI_Model{

    protected $table = 'payment_instrument';
    
    public function __construct(){
        $this->load->database();
    }
    
    public function getInstrument($id){
        return $this->db
                ->where('instrument_id', $id)
                ->where('delete_time', NULL)
                ->order_by('create_time', 'DESC')
                ->get($this->table);
    }

    public function getUserInstruments($email){
        return $this->db
                ->where('instrument_name', $email)
                ->where('delete_time', NULL)
                ->order_by('create_time', 'DESC')
                ->get($this->table);
    }

    public function getInstruments(){
        return $this->db
                ->where('delete_time', NULL)
                ->order_by('create_time', 'DESC')
                ->get($this->table);
    }

    public function save($notif){
       return $this->db
             ->insert($this->table, $notif);
    }
    
    public function delete($id){
        $update = array('delete_time' => time());
		
        return $this->db
            ->set($update)
            ->where('instrument_id', $id)
            ->update($this->table);
    }
    
    public function update($id, $data){
        return $this->db
            ->set($data)
            ->where('instrument_id', $id)
            ->update($this->table);
    }
}