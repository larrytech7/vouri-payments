<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction extends CI_Model{

    protected $table = 'transactions';

    public function __construct(){
        $this->load->database();
        $this->status = 0;
    }

    public function save($data){
        //save transaction
        return $this->db
            ->insert($this->table, $data);
    }

    public function update($id, $data){
        //update transaction
        $data['update_time'] = date('Y-m-d H:i:s', time());

        return $this->db
            ->set($data)
            ->where('transaction_id', $id)
            ->update($this->table);
    }

    public function getTransactions($userid){
        return $this->db
            ->select('transaction_id,amount, amount_sent,currency,receiving_currency,sender_id,
            receiver_phone, receiver_email, receiver_name, send_gateway, receive_gateway,message, transactions.status,
            transactions.create_time, transactions.delete_time,transactions.update_time, user_id, role, name,
             phone, email, logged_in, auth_token')
            ->where('sender_id', $userid)
            ->where('transactions.delete_time', null)
            ->join('user', 'user.user_id = transactions.sender_id')
            ->order_by('transactions.create_time', 'DESC')
            ->get($this->table);
    }

    public function getAll(){
        return $this->db
            ->select('transaction_id,amount, amount_sent,currency,receiving_currency,sender_id,
            receiver_phone, receiver_email, receiver_name, send_gateway, receive_gateway,message, transactions.status,
            transactions.create_time, transactions.delete_time,transactions.update_time, user_id, role, name,
             phone, email, logged_in, auth_token')
            ->where('transactions.delete_time', null)
            ->join('user', 'user.user_id = transactions.sender_id')
            ->order_by('transactions.create_time', 'DESC')
            ->get($this->table);
    }

    public function getTransactionById($transaction_id){
        return $this->db
            ->where('transaction_id', $transaction_id)
            ->where('transactions.delete_time', null)
            ->join('user', 'transactions.sender_id = user.user_id')
            ->order_by('transactions.create_time', 'DESC')
            ->get($this->table);
    }

}