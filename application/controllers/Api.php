<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once('GatewayTransactionFactory.php');
require_once(APPPATH.'utils/Utility.php');

use Mremi\UrlShortener\Model\Link;
use Mremi\UrlShortener\Provider\Bitly\BitlyProvider;
use Mremi\UrlShortener\Provider\Bitly\OAuthClient;
use chriskacerguis\RestServer\RestController;

class Api extends RestController {

    protected $bitlyProvider;

    public function __construct(){
        //load libraries and helpers
        parent::__construct();
        date_default_timezone_set('Africa/Douala');
        //url shortener
        $this->bitlyProvider = new BitlyProvider(
            new OAuthClient('icep603@gmail.com', 'Creationfox7*'),
            ['connect_timeout' => 60, 'timeout' => 60]
        );
        //models
		$this->load->model('apiModel');
		$this->load->model('qrTransaction');
		$this->load->model('user');
		$this->load->model('paymentrequest');
		$this->load->model('transaction');
		$this->load->model('instrument');
		$this->load->model('subscription');
		$this->load->model('qrcodes');
        //libs
        $this->encryption->initialize([
            'cipher' => 'aes-256'
        ]);
        $this->load->library('user_agent');
        $this->load->library('ciqrcode');
    }

    //signup for a developer API key
    public function register_post(){
        $user['email'] = $this->post('email');
        $user['level'] = 1;
        //create a key for this user and return credentials
        $user['api_key'] = crypt($user['email'], md5(time().'-')).''.crypt($user['email'], time().'-');
        $user['date_created'] = time();
        $added = false;
        try{
            $added = $this->apiModel->insert($user);
        }catch (Exception $ex){
            log_message('error', 'Failure to insert data. '.$ex->getTraceAsString());
        }

        $this->response([
             'status_code' => $added ? RestController::HTTP_OK : RestController::HTTP_INTERNAL_ERROR,
             'message' => $added ? 'YOUR API KEY IS : '.$user['api_key'] : 'Error Creating your API key please try again'
        ]);
    }

    //client/user login
    public function login_post(){

        $email = $this->post('email');
        $password = $this->post('password');
        //valid request, proceed with it
        $authUser = $this->user->authenticate($email, $password);
        if($authUser->num_rows() > 0){
            $this->response([
                'status_code' => RestController::HTTP_OK,
                'message' => 'Login successful',
                'user' => $authUser->result_object()[0]
            ]);
        }else{
            $this->response([
                    'status_code' => RestController::HTTP_NOT_FOUND,
                    'message' => 'Invalid username/password'
                ]);
        }
    }

    /**
     * Signup a new user
     */
    public function signup(){

        $data = json_decode($this->stream, true);
        $token = $data['token'];
        $auth_data = json_decode($this->checkApiAuth($token), true);
        //echo json_encode($data);
        if($auth_data['status_code'] != 200){
            //invalid token. user needs to try again with the correct token
            echo json_encode($auth_data);
        }else{
            //valid request, proceed with it
            $user['name'] = $data['client_name'];
            $user['email'] = $data['client_email'];
            $user['phone'] = $data['client_phone'];
            $user['password'] = password_hash($data['client_password'], PASSWORD_DEFAULT);
            $user['address'] = $data['client_address'];
            $user['city'] = $data['client_city'];
            $user['country'] = $data['client_country'];
            $user['role'] = 'subscriber'; //default user role
            $user['auth_token'] = crypt(''.time(), base64_encode($user['email']));
            $user['user_id'] = crypt($user['email'], sha1(''.time()));

            $savedUser = $this->user->save($user);

            unset($user['password']);

            echo json_encode([
                    'status_code' => $savedUser ? 200 : 401,
                    'data' => $savedUser ? [
                        'activation_link' => site_url('api/user/activate/'.$user['auth_token']),
                        'user' => $user
                    ] : [],
                    'message' => $savedUser ? 'Account created successfully' : 'Error signup user. Please try again'
                ]);
        }
    }

    public function editUser(){
        $data = json_decode($this->stream, true);
        $token = $data['token'];
        $auth = json_decode($this->checkApiAuth($token),true);

        if($auth['status_code'] == 200){
            unset($data['token']);
            $updated = $this->user->updateUser($data, $data['user_id']);

            echo json_encode([
                'status_code' => $updated ? 200 : 401,
                'message' => $updated ? 'OK' : 'Error updating. Check logs'
            ]);
        }else{
            echo json_encode($auth);
        }
    }

    public function deleteUser(){
        $data = json_decode($this->stream, true);
        $token = $data['token'];
        $auth = json_decode($this->checkApiAuth($token),true);

        if($auth['status_code'] == 200){
            $deleted = $this->user->delete($data['userid']); //get this user's profile
            echo json_encode([
                'status_code' => $deleted ? 200 : 401,
                'message' => $deleted ? 'OK' : 'Error deleting'
            ]);
        }else{
            echo json_encode($auth);
        }
    }

    /**
     * Handle request for initiating a subscription payment
     */
    public function paysubscribe_post(){
        $api_key = $this->post('data-api-key');
        $data['subscription_api_key'] = $api_key;
        $data['subscription_callback'] = $this->post('data-callback');
        $data['subscription_type'] = $this->post('data-subscription');
        $data['subscription_name'] = $this->post('data-name'); //name of service provider
        $data['subscription_description'] = $this->post('data-description');
        $data['subscription_amount'] = $this->post('data-amount');
        $data['subscription_currency'] = $this->post('data-currency');
        $data['subscription_client_phone'] = $this->post('v-phone');
        $data['subscription_client_name'] = $this->post('v-name');
        $data['subscription_payment_method'] = $this->post('v-payment-method');
        //Check subscription validity. Check if there's already a subscription with this number
        $subscription = $this->subscription->find([
            'subscription_client_phone' => $data['subscription_client_phone']
        ]);
        try {
            //Convert amount to local currency
            $charge = Utility::convertToLocal($data['subscription_amount'], $data['subscription_currency']);
            //Make payment request
            $payRequest = new GatewayTransactionFactory(strtoupper($data['subscription_payment_method']));
            $paymentResponse = $payRequest->getGateway()->purchase([
                '_amount' => $charge,
                '_tel' => preg_replace('/\s^\+/', '', $data['subscription_client_phone'])
            ])->send();

            //save subscription or update
            if ($subscription->num_rows() <= 0) //new subscription
                $saved = $this->subscription->insert($data);
            else
                $saved = $this->subscription->update($data, $subscription->result_object()[0]->subscription_id);

            //return results
            $this->response([
                'status_code' => $saved && $paymentResponse->isSuccessful() ? RestController::HTTP_OK : RestController::HTTP_NOT_MODIFIED,
                'transaction' => $data,
                'message' => $paymentResponse->isSuccessful() ? 'Transaction was successful. Click Ok to continue' : 'Transaction failed with error. Please try again.'
            ], RestController::HTTP_OK);
        }catch (Exception $ex){
            log_message('error', $ex->getMessage().$ex->getTraceAsString());
            $this->response([
                'status_code' => RestController::HTTP_INTERNAL_ERROR,
                'message' => $ex->getMessage()
            ], RestController::HTTP_INTERNAL_ERROR);
        }
    }

    public function pay_post(){
        $requestData['amount'] = $this->post('amount');
        $requestData['currency'] = $this->post('currency');
        $requestData['phone_number'] = $this->post('phone_number');
        $requestData['payment_method'] = $this->post('payment_method');
        $requestData['email'] = $this->post('email');

        //parse payment method to instantiate appropriate processing gateway
        $gw = (new GatewayTransactionFactory(strtoupper($requestData['payment_method'])))->getGateway();
        switch(strtoupper($requestData['payment_method'])){
            case 'MMO':
                try {
                    //charge sender
                    $response = $gw->purchase([
                        '_amount' => Utility::convertToLocal($requestData['amount'], $requestData['currency']),
                        '_tel' => $requestData['phone_number']
                    ])->send();
                    if ($response->isSuccessful()) {
                        $this->response([
                            'transaction_id' => $response->getTransactionReference(),
                            'transaction_status' => 200,
                            'message' => 'Transaction successful',
                            'error_message' => ''
                        ], RestController::HTTP_OK);
                    } else {
                        $this->response([
                            'transaction_id' => $response->getTransactionReference(),
                            'transaction_status' => -1,
                            'data' => $response->getMessage(),
                            'error_message' => 'Transaction failed. The transaction was unsuccessful. You need to try again'
                        ], RestController::HTTP_OK);
                    }
                }catch (\Omnipay\Common\Http\Exception $ex){
                    $this->response([
                        'transaction_id' => 0,
                        'transaction_status' => -1,
                        'message' => $ex->getTraceAsString(),
                        'error_message' => $ex->getMessage()
                    ], RestController::HTTP_OK);
                }catch(\GuzzleHttp\Exception\ConnectException $con){
                    $this->response([
                        'transaction_id' => 0,
                        'transaction_status' => -1,
                        'message' => $con->getTraceAsString(),
                        'error_message' => 'Connection Error. '.$con->getMessage()
                    ], RestController::HTTP_OK);
                }
                return;
            case 'OMO':
                //TODO: generate redirect url and send response
                $this->response([
                    'transaction_status' => 302,
                    'redirect_url' => '',
                    'error_message' => '',
                    'message' => 'Request requires you to redirect to vouri payment portal'
                ], 302);
                return;
            case 'VSA':
                //TODO: generate redirect url and send response
                $this->response([
                    'transaction_status' => 302,
                    'redirect_url' => '',
                    'error_message' => '',
                    'message' => 'Request requires you to redirect to vouri payment portal'
                ], 302);
                return;
            default:
                $this->response([
                    'transaction_status' => RestController::HTTP_BAD_REQUEST,
                    'error_message' => 'Your request could not be handled as it might be wrongly configured.',
                    'message' => 'Your request could not be handled as it might be wrongly configured.'
                ], RestController::HTTP_BAD_REQUEST);
                return;
        }
    }

    /**
     * Generate the QR code for the particular user
     */
    public function qrMake_post()
    {
        $amount = $this->post('amount');
        $phone_number = $this->post('phone_number');
        $username = $this->post('username');
        $api_key = $this->post('api_key');
        $qrData = join(":", [$phone_number, $username, $amount]);
        //build the request query part
        $query = http_build_query([
            'data' => base64_encode($this->encryption->encrypt($qrData)),
            'api_key' => base64_encode($this->encryption->encrypt($api_key))
        ]);
        $codeUrl = site_url("p2p/v1/qrprocess?" . $query);
        $response = [];
        if (strlen($phone_number) >= 9 && strlen($username) > 3 && preg_match('/(\d+)/', $phone_number ) == 1){
            try {
                $link = new Link;
                $link->setProviderName('bitly');
                $link->setLongUrl($codeUrl);
                $this->bitlyProvider->shorten($link); //shorten url
                //save qr code
                $name = sha1($phone_number . "_" . time()) . '.png';
                $params['data'] = $link->getShortUrl();
                $params['level'] = 'H';
                $params['size'] = 5;//10;
                $params['savename'] = FCPATH . "resources/p2p/qr/" . $name;
                $this->ciqrcode->generate($params); //generate QR code
                //Store data in the database to track user QR code creation
                $this->qrcodes->insert([
                    'phone_number' => $phone_number,
                    'username' => $username,
                    'qr_image' => $name
                ]);
                $response = [
                    'status' => RestController::HTTP_OK,
                    'qr_data' => base_url("resources/p2p/qr/" . $name),
                    'message' => "QR code generated successfully."
                ];

            } catch (Exception $ex) {
                log_message('error', $ex->getTraceAsString());
                $response = [
                    'status' => RestController::HTTP_INTERNAL_ERROR,
                    'message' => "An error occurred while generating QR code. " . $ex->getMessage()
                ];
            } finally {
                $this->response($response, RestController::HTTP_OK);
            }
        }
        else{
            $error = strlen($phone_number) < 9 ? 'Phone number invalid. Must be at least 9 digits' : '';
            $error .= strlen($username) < 4 ? 'user name is invalid. Must be at least 4 characters' : '';
            $this->response(['status' => RestController::HTTP_BAD_REQUEST, "message" => "Bad request. One or more request parameters are invalid. ". $error], RestController::HTTP_BAD_REQUEST);
        }
    }

    /*
	 * Decrypt encrypted data from part of the request that corresponds to the phone
	 * number, username and amount of the transaction
	 * @requestParam - data to decrypt and extract request parameters
	 */
    public function qrProcess_get(){
        $requestData = $this->get('data');
        $api_key = $this->get('api_key');
        //decrypt data
        $data = $this->encryption->decrypt(base64_decode($requestData));
        /**
         * extract data
         * First element is the phone number,
         * Second element is the username of the recipient,
         * Third element is the amount (optional)
         */
        $params = explode(':',$data);
        $this->response([
            'status' => count($params) > 1 ? RestController::HTTP_OK : RestController::HTTP_NOT_FOUND,
            'message' => count($params) > 1 ? 'Data decrypted' : 'Error decrypting info. Information incomplete',
            'data' => [
                'phone_number' => array_key_exists(0, $params) ? $params[0] : '',
                'username' => array_key_exists(1, $params) ? $params[1] : '',
                'amount' => array_key_exists(2, $params) ? $params[2] : '',
                'api_key' => $api_key
            ]
        ]);
    }

    /*
	 * Initiate payment operation towards sender
	 */
    public function qrPay_post(){
        $transaction['recipient_phone_number'] = $this->post('recipient_phone_number');
        $transaction['recipient_username'] = $this->post('recipient_username');
        $transaction['transaction_amount'] = $this->post('amount');
        $transaction['sender_phone_number'] = $this->post('sender_phone_number'); //sender's Phone number
        $transaction['sender_username'] = $this->post('sender_username'); //sender's name

        $gw = (new GatewayTransactionFactory('MMO'))->getGateway();
        $response = $gw->purchase([
            '_amount' => $transaction['transaction_amount'],
            '_tel' => $transaction['sender_phone_number']
        ])->send();
        try{
            if ($response->isSuccessful()) {
                //Save transaction to database
                $this->qrTransaction->insert($transaction);
                //TODO: Send customized SMS about status of transaction
                $this->response([
                        'status' => RestController::HTTP_OK,
                        'message' => 'Transaction successful.',
                        'data' => [
                            'amount' => $transaction['transaction_amount'],
                            'transaction_id' => $response->getTransactionReference()
                        ]
                    ], RestController::HTTP_OK);
            } else {
                $this->response([
                        'status' => RestController::HTTP_NOT_MODIFIED,
                        'message' => 'Transaction unsuccessful. Maybe user has insufficient balance or amount is invalid. Try again'
                    ], RestController::HTTP_NOT_ACCEPTABLE);
            }
        }catch(\Omnipay\Common\Http\Exception $ex){
            $this->response([
                    'status' => RestController::HTTP_INTERNAL_ERROR,
                    'message' => 'Error making payment request. '.$ex->getMessage()
                ], RestController::HTTP_INTERNAL_ERROR);
        }
    }
}
