<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'utils/SendEmail.php');
require_once('GatewayTransactionFactory.php');
use GuzzleHttp\Psr7\Request;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;

class Home extends CI_Controller {

	/**
	 * @date : 2019-12-10
	 * @source : https://www.xe.com
	 * @var array
	 */
	static $rates = [
		'CHF' => [
			'USD' => 1.001,
			'GBP' => 0.756,
			'XAF' => 574.00,
			'CAD' => 1.315,
			'AED' => 3.63,
			'EUR' => 0.88,
		],
		'USD' => [
			'EUR' => 0.881,
			'GBP' => 0.774,
			'NGN' => 361.797,
			'XAF' => 580.65,
			'CAD' => 1.324,
			'AED' => 3.672,
		],
		'AED' => [
			'EUR' => 0.236,
			'GBP' => 0.213,
			'NGN' => 99.454,
			'XAF' => 155.085,
			'CAD' => 0.360,
			'USD' => 0.272,
		],
		'CAD' => [
			'EUR' => 0.656,
			'GBP' => 0.592,
			'NGN' => 257.821,
			'XAF' => 430.319,
			'USD' => 0.755,
			'AED' => 2.775,
		],
		'EUR' => [
			'USD' => 0.880,
			'GBP' => 1.120,
			'NGN' => 412.351,
			'XAF' => 655.957,
			'CAD' => 1.524,
			'AED' => 4.231,
		],
		'GBP' => [
			'EUR' => 0.893,
			'USD' => 0.785,
			'NGN' => 461.953,
			'XAF' => 755.075,
			'CAD' => 1.688,
			'AED' => 4.682,
		],
		'NGN' => [
			'EUR' => 0.00243,
			'GBP' => 0.00217,
			'USD' => 0.00275,
			'XAF' => 1.59125,
			'CAD' => 0.0036,
			'AED' => 0.01,
		],
		'XAF' => [
			'EUR' => 0.00152,
			'GBP' => 0.00136,
			'NGN' => 0.62844,
			'USD' => 0.00172,
			'CAD' => 0.0023,
			'AED' => 0.0064,
		]
	];
	static $gatewayCurrency = [
		'MMO' => 'XAF',
		'OMO' => 'XAF',
		'STR' => 'USD',
		'VSA' => 'USD',
	];
	
	public $covers = [
		'architecture-bg.jpg', 'bridge-bg.jpg', 'cover3.jpg', 'cover4.jpg', 'cover5.jpg', 'cover6.jpg', 'cover7.jpg', 'cover8.jpg', 'cover9.jpg',
		'covera.jpg', 'cover10.jpg',
	];
	public static $API_KEY = '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';
	protected $sendgrid;
	public $email;

    public function __construct(){
        parent::__construct();
		$this->httpAdapter = new GuzzleAdapter(null);
		$this->headers = [
			'Content-Type' => 'application/json',
			'Accept'=>'application/json',
			'Authorization'=>'Basic '.base64_encode('vouri@restadmin:vouri@rest-admin-123'),
			'auth'=>['vouri@restadmin', 'vouri@rest-admin-123']
		];
		//app language configuration
		$this->lang->load('app', $this->session->userdata('lang', 'english'));
		//sendgrid email configuration
		$this->sendgrid = new \SendGrid(Home::$API_KEY);
		$this->email = new \SendGrid\Mail\Mail();
		$this->email->setFrom('no-reply@vouriinc.com', 'Vouri Payments');
		$this->email->addBcc('icep603@gmail.com','Vouri Payments');
        //libs
		$this->encryption->initialize([
			'cipher' => 'aes-256'
		]);
        $this->load->library('table');
        $this->load->library('user_agent');
		$template = array(
			'table_open' => '<table class="table table-md table-striped table-hover">'
		);

		$this->table->set_template($template);
        
		//models
        $this->load->model('user');
        $this->load->model('contactus');
        $this->load->model('paymentrequest');

        //helpers
		$this->load->helper(array('form','date'));
		//$this->output->enable_profiler(true);
    }

	private function verifyCaptcha($http, $url, $data=[]){
		try{
			$client = new \GuzzleHttp\Client();
			$response = $client->request($http, $url, [
				'form_params' => $data
			]);
			return json_decode($response->getBody()->getContents(), true);
		}catch(\Http\Client\Exception\NetworkException $nex){
			return json_encode([
				'status' => 401,
				'success' => false,
				'error' => $nex->getMessage(),
				'trace' => $nex->getTraceAsString(),
				'message' => sprintf('Network error. Unable to connect to site %s', $url)
			]);
		}
	}

	//home
	public function index(){
		//redirect to dashboard if already logged-in
		if($this->session->userdata('loggedin') == 1)
			redirect('/admin', 'location');
		//get any incoming request payment ID;
		$incomingRequest = $this->input->get('pay_id');

		$pRequest = null;
		if($incomingRequest != null){
			//update payment request email
			/**
			 * 2 - Success
			 * 1 - processing
			 * 0 - Pending
			 * -1 - Error
			 */
			$this->paymentrequest->update($incomingRequest, ['status' => 1]);
			$pRequest = $this->paymentrequest->getRequests($incomingRequest)->result_object();
		}
		$this->getPage('content',
			['paymentrequest' => $pRequest,
		'currencies' => Home::$rates]);
	}

	public function back(){
		redirect($this->agent->referrer());
	}

	public function en(){
		//switch language to english
		$this->session->set_userdata('lang','english');
		redirect(site_url(''), 'location');
	}

	public function fre(){
		//switch language to french
		$this->session->set_userdata('lang', 'french');
		redirect(site_url(''), 'location');
	}

	//login
	public function login(){
		//redirect to dashboard if already logged-in
		if($this->session->userdata('loggedin') == true)
			redirect('admin', 'location');

		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|min_length[6]',
			['valid_email' => 'A valid email is required',
			'required' => 'An %s is required']);
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]',
			['required' => 'A %s is required']);

		if ($this->form_validation->run() == FALSE){
			$this->getPage('login');
		}
		else{
			//authenticate and redirect
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$user = $this->user->authenticate($email, password_hash($password, PASSWORD_BCRYPT,
				['salt' => sha1($password)]));
			if($user->num_rows() > 0){
				//success
				$user = $user->result_object()[0];

				$this->session->set_userdata(array(
					'loggedin' => $this->encryption->encrypt(true),
					'user' => $user
				));
				$this->session->set_flashdata('info', $this->encryption->encrypt('Welcome, '.$user->name));
				redirect('admin', 'location');
			}else{
				$this->session->set_flashdata('error', $this->encryption->encrypt('Email/password incorrect'));
				redirect('login', 'location');
			}
		}
	}

	//signup
	public function signup(){
		//$this->session->set_flashdata('info', $this->encryption->encrypt('Sorry our signup process is not active at the moment. Please Try again later'));
		//redirect('login');
		//redirect to dashboard if already logged-in
		if($this->session->loggedin == true)
			redirect('admin', 'location');

		$this->form_validation->set_rules('name', 'Name', 'required',
			['required' => 'Please at least enter a name or pseudo']);
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|min_length[6]|is_unique[user.email]',[
			'valid_email' => 'We require that you enter a valid email',
			'is_unique' => 'This email is already in use. You may login to continue'
		]);
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]',[
			'required' => 'A password is required'
		]);
		$this->form_validation->set_rules('repassword', 'Re Password', 'matches[password]',[
			'matches' => 'Passwords do not match. Please make sure they match before submitting'
		]);

		if ($this->form_validation->run() === FALSE){
			//$this->session->set_flashdata('error', $this->encryption->encrypt(implode('',explode('</p>',implode('',explode('<p>', validation_errors()))))));
			$this->getPage('signup');
		}
		else{
			//signup, send email and and redirect
			$data['user_id'] = crypt($this->input->post('email').time(), ''.time());
			$data['email'] = $this->input->post('email');
			$data['name'] = $this->input->post('name');
			$data['phone'] = $this->input->post('phone');
			$data['password'] = password_hash($this->input->post('password'),PASSWORD_BCRYPT,
				['salt' => sha1($this->input->post('password'))]);
			$data['auth_token'] = crypt(time().$data['email'], ''.time() );
			//VERIFY CAPTCHA before saving user
			$response = $this->verifyCaptcha('POST', 'https://www.google.com/recaptcha/api/siteverify', [
				'response' => $this->input->post('g-recaptcha-response'),
				'secret' => '6Le10boUAAAAAFi9zeehb3I-3bkyeAieSpstwKMT'
			]);
			//var_dump($response);
			//die();
			$saved = false;
			if($response['success']) //captcha validated
				$saved = $this->user->save($data);
			else
				log_message('error', implode("-",array_values($response['error-codes'])));

			if($saved){
				//Send Activation email
				$response = SendEmail::sendMail(['email'=>$data['email'], 'name'=>$data['name']],
					'Vouri Inc Payments - Signup',
					$this->getEmailPage('signupemail',
						['id' => $data['user_id'],
							'name' => $data['name']]));
				try{
					//$response = $this->sendgrid->send($this->email);
					if($response >= 200)
						$this->session->set_flashdata('info', $this->encryption->encrypt('Success! An activation email has been sent for you to confirm your account.'));
					else
						$this->session->set_flashdata('error', $this->encryption->encrypt('Error! Your account has been created but no activation email has been sent. Please contact us for details.'));

				}catch (Exception $ex){
					log_message('error', $ex->getMessage());
					log_message('error', $ex->getFile().$ex->getLine());
					log_message('error', $ex->getTraceAsString());
				}
			}else{
				//error
				$this->session->set_flashdata('error', $this->encryption->encrypt('Account creation failed. Please try again'));
			}
			redirect('signup', 'location');
		}
	}
	//reset password
	public function reset_password($id){
		$userid = base64_decode($id);

		$this->form_validation->set_rules('password', 'New Password', 'required|min_length[6]',[
			'required' => 'A password is required'
		]);
		$this->form_validation->set_rules('re-password', 'Confirm Password', 'matches[password]',[
			'matches' => 'Passwords do not match. Please make sure they match before submitting'
		]);

		if ($this->form_validation->run() === FALSE){
			$this->getPage('reset', ['id' => $userid]);
		}
		else{
			//update, send email and and redirect
			$user_id = $this->input->post('userid');
			$data['password'] = password_hash($this->input->post('password'),PASSWORD_BCRYPT,
				['salt' => sha1($this->input->post('password'))]);
			$saved = $this->user->update($user_id, $data);
			if($saved){
				$this->session->set_flashdata('info', $this->encryption->encrypt('Success! Your password has been update. Login to confirm.'));
			}else{
				//error
				$this->session->set_flashdata('error', $this->encryption->encrypt('Password update failed. Please try again'));
			}
			redirect('home/login/', 'location');
		}
	}
	//initiate password reset
	public function reset(){
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|min_length[6]',[
			'valid_email' => 'We require that you enter a valid email']);

		if ($this->form_validation->run() === FALSE){
			$this->session->set_flashdata('error', $this->encryption->encrypt('Error trying to reset your password. Please try again or contact our support.'));
			redirect('home/login');
		}
		else{
			//Send email and and redirect
			$user_email = $this->input->post('email');
			$saved = $this->user->getUserByEmail($user_email);
			if($saved->num_rows()){
				//Send password reset email
				$response = SendEmail::sendMail(['email'=>$user_email, 'name'=>''],
					'Vouri Inc Payments - Password reset',
					$this->getEmailPage('password_resetemail',
						['link' => site_url('home/reset_password/'.base64_encode($saved->result_object()[0]->user_id))]));
				try{
					//$response = $this->sendgrid->send($this->email);
					if($response >= 200)
						$this->session->set_flashdata('info', $this->encryption->encrypt('Success! Please check your email for a password reset link.'));
					else
						$this->session->set_flashdata('info', $this->encryption->encrypt('Error! Email may not match any of our records. Please contact us for details.'));

				}catch (Exception $ex){
					log_message('error', $ex->getMessage());
					log_message('error', $ex->getFile().$ex->getLine());
					log_message('error', $ex->getTraceAsString());
				}
			}else{
				//error
				$this->session->set_flashdata('error', $this->encryption->encrypt('Error trying to reset your password. Please try again or contact our support.'));
			}
			redirect('home/login', 'location');
		}
	}

	public function activate($id){
		$id = base64_decode($id);
		if($this->user->update($id, ['auth_token'=> null])){
			$this->session->set_flashdata('info', 'Account activated successfully');
		}else{
			$this->session->set_flashdata('info','Error activating account. Please try again later');
		}
		redirect(site_url('login'));
	}

	/**
	 * Receives a post request with sending transaction data
	 */
	public function sendfunds()
	{
		$t['isCompliant'] = true;
		$t['sendfrom'] = $this->input->post('sendfrom') == '' ? $this->session->transaction['sendfrom']
			: $this->input->post('sendfrom'); //sending gateway
		$t['currency'] = $this->input->post('currency') == '' ? $this->session->transaction['currency']
			: $this->input->post('currency'); //sending currency
		$t['amount'] = $this->input->post('amounttosend') == '' ? $this->session->transaction['amount']
			: $this->input->post('amounttosend'); //sending amount
		//check if currency is not part of the currencies we collect by default
		if ($t['sendfrom'] == 'VSA' && !in_array($t['currency'], ['USD'])){ //sending currency different from platform currencies
			$t['amount_to_send'] = intval( $this->getTransactionAmount(
				$t['amount'],
				$t['currency']
			) + (Home::$rates['USD'][$t['currency']] * 0.3 )); //sending Amount. Amount to charge the sender over the chosen gateway
		}else{
			$t['amount_to_send'] = $this->getTransactionAmount(
				$t['amount'],
				$t['currency']
			);
		}
		//check if user is currently authenticated, then check KYC compliance status
		if($this->session->has_userdata('loggedin')){
			//convert amount and check compliance
			$transactions = $this->user->getUserPci($this->session->user->user_id)->result_object();
			$sum = 0;
			foreach($transactions as $transaction){
				//convert the transaction amounts to USD and compare with compliance amount
				$sum += Home::getSendingAmount(round($transaction->amount,2), strtoupper($transaction->currency), 'USD');
			}
			//establish compliance
			$compliance_amount = $this->getComplianceAmount($this->session->user->kyc_level);
			if($compliance_amount < $sum){ //KYC insufficient compliance level
				//user not allowed to do anymore transactions based on his level of compliance
				$this->session->set_flashdata('error', $this->encryption->encrypt($this->lang->line('insufficient_compliance')));
				$t['isCompliant'] = false;
			}
			if(Home::getSendingAmount($t['amount_to_send'], $t['currency'],'USD') > 1100){
				$t['isCompliant'] = false;
				$this->session->set_flashdata('error', $this->encryption->encrypt($this->lang->line('send_limit_exceeded')));
			}
		}

		//temporally store the transaction
		$this->session->set_userdata('transaction', $t);

		$this->getPage('sendpay', $t);
	}

	public function unavailable(){
		$this->getPage('unavailable');
	}

	/**
	 * Check if this user is compliant and has properly confirmed their full ID on the platform
	 * Level 1 : Email and Phone number confirmed. Limit : $3000/Mo
	 * Level 2 : Address, city & Country confirmed. Limit : $5000/Mo
	 * Level 3 : Occupation, ID card confirmed. Limit : $100000/Mo
	 * @param int $level of compliance
	 */
	public static function getComplianceAmount($level){
		switch($level){
			case 1:
				return 3000;
			case 2:
				return 5000;
			case 3:
				return 100000;
			default:
				return 3000;
		}
	}

	public function verifycard(){
		$gw = (new GatewayTransactionFactory('VSA'))->getGateway();

		//perform CARD verification transaction
		try {
			$token = $this->input->post('stripeToken');
			$response = $gw->purchase([
						'amount' => 1,
						'currency' => 'CHF',
						'returnUrl' => site_url('return/visa'),
						'token' => $token,
					])->send();
			//var_dump($response->getData()); return;
			if ($response->isSuccessful()) {
				$this->session->set_flashdata('info', $this->encryption->encrypt($this->lang->line('card_success')));
				//refund. TODO: check return value for the return operation. Might fail for some strange reasons
				$refund = $gw->refund(['charge' => $response->getData()['id'],
					'amount' => 1,
				'transactionReference' => $response->getTransactionReference()])
					->send();

			} elseif ($response->isRedirect()) { //redirect to offsite payment gateway
				$response->redirect();
			} else { //payment failed
				$this->session->set_flashdata('error', $this->encryption->encrypt($this->lang->line('network_error')));
			}
		}catch(\Omnipay\Common\Http\Exception $ex){
			$this->session->set_flashdata('error', $this->encryption->encrypt($this->lang->line('network_error')
				.$ex->getMessage()));
		}catch(\Omnipay\Common\Exception\InvalidCreditCardException $cce){
			$this->session->set_flashdata('error', $this->encryption->encrypt($this->lang->line('card_error').' '.$cce->getMessage()));
		}catch(Exception $ex){
			$this->session->set_flashdata('error', $this->encryption->encrypt('General failure. '
				.$ex->getMessage()));
		}
		redirect(site_url('home'));
	}

	public function privacy(){
		$this->session->set_flashdata('info', $this->lang->line('translate_not_available'));
		$this->load->view('admin/header');
		$this->load->view('public/privacy');
		$this->load->view('admin/footer');
	}

	public function api(){
		$this->getPage('api');
	}

	public function apiCheckout(){
		$this->load->view('public/checkout');
	}

	public function about(){
		$this->getPage('about');
	}

	public function terms(){
		$this->load->view('admin/header');
		$this->load->view('public/terms');
		$this->load->view('admin/footer');
	}

	public function fees(){
		$this->load->view('admin/header');
		$this->load->view('public/fees');
		$this->load->view('admin/footer');
	}

	public function contactus(){
		//send contact message
		$message['email'] = $this->input->post('email');
		$message['phone'] = $this->input->post('phone');
		$message['subject'] = $this->input->post('subject');
		$message['message'] = $this->input->post('message');

		$saved = $this->contactus->save($message); //save message to database
		//send email to a vouri address so they can directly respond
		SendEmail::sendMail(['email' => 'hello-care@vouriinc.com',
			'name' => 'Vouri Payments'],
			'New Contact us Message - '.$message['subject'],
			$this->getEmailPage('contactemail', $message), ['email' => 'icep603@gmail.com', 'name' => 'Vouri Inc Payments'],
			['email'=>$message['email'], 'name' => $message['phone']]);

		if($saved){
			$this->session->set_flashdata('info', $this->lang->line('contact_success'));
		}else{
			$this->session->set_flashdata('error', $this->lang->line('contact_error'));
		}
		redirect('home', 'location');
	}

	public function newsLetter(){
		//get all users and send them newsletter reminder to keep sending and receiving

	}

	public function requestPayment(){
		$requestData['amount'] = $this->input->post('amount');
		$requestData['currency'] = $this->input->post('currency');
		$requestData['gateway'] = $this->input->post('gateway');
		$requestData['email'] = $this->input->post('email');
		$requestData['name'] = $this->input->post('name');
		$requestData['message'] = $this->input->post('message');
		$requestData['request_id'] = crypt(time().$requestData['email']);

		$this->form_validation->set_rules('currency', 'Currency', 'required|min_length[1]');
		$this->form_validation->set_rules('gateway', 'Receiving Gateway', 'required|min_length[1]');

		if($this->form_validation->run() == TRUE)
			if($this->session->loggedin == true){
				if($this->input->post('amount') == '')
					$requestData = $this->session->requestData;
				//save request
				$saved = $this->paymentrequest->save($requestData);
				//send request
				$result = SendEmail::sendMail(['email'=>$requestData['email'], 'name'=>$requestData['name']],
					'Vouri Inc payments - Payment Request',
					$this->getEmailPage('paymentrequestemail', $requestData));
				if($result > 0 && $saved){
					$this->session->set_flashdata('info', 'Your payment request has been sent successfully');
				}else{
					$this->session->set_flashdata('error', 'Error sending your payment request. Please try again');
				}
				$this->session->unset_userdata('temp_url');
				$this->session->unset_userdata('requestData');
				redirect('admin', 'location');
			}else{
				//not authenticated, redirect for authentication
				$this->session->set_flashdata('info', 'Please login to continue your operation');
				$this->session->set_userdata('temp_url', site_url('paymentrequest'));
				$this->session->set_userdata('requestData', $requestData);
				redirect('login');
			}
		else{
			//form not valid
			$errors = strip_quotes(validation_errors());
			$errors = strip_slashes($errors);
			$errors = trim(preg_replace('/\s+/', ' ', $errors));;
			$this->session->set_flashdata('error', $this->encryption->encrypt($errors));
			redirect('home', 'redirect');
		}
	}

	public function convert(){
		$amount = $this->input->post('amount');
		$currency = $this->input->post('from_currency');
		$to_currency = $this->input->post('to_currency');
		$gateway = $this->input->post('send_gateway');

		if ($gateway == 'VSA' && !in_array($currency, ['CHF'])) { //sending currency different from platform currencies
			$result = Home::getSendingAmount(
					round(self::getChargeAmount($amount, $currency), 0),
					$currency,
					Home::$gatewayCurrency[$to_currency]
				) - (Home::$rates['CHF'][$currency] * 0.3);
		}else{
			$result = Home::getSendingAmount(
				round(self::getChargeAmount($amount, $currency), 0),
				$currency,
				Home::$gatewayCurrency[$to_currency]
			);
		}

		echo json_encode([
			'status' => 'ok',
			'result' => number_format(intval(round($result,0),10)).' '.Home::$gatewayCurrency[$to_currency]
		]);
	}

	/**
	 * Returns the amount to send converted to the receiving currency
	 * @param $amount - to send for conversion
	 * @param $toCurrency
	 * @param $fromCurrency
	 * @return float - sending amount with currency conversion fee
	 */
	public static function getSendingAmount($amount, $fromCurrency, $toCurrency){
		//$amount = Home::getChargeAmount($amount, $fromCurrency); //first apply transaction fee, then convert balance
		if($toCurrency == $fromCurrency)
			return $amount;
		else{
			return round($amount * Home::$rates[''.$fromCurrency][''.$toCurrency],0);
		}
	}

	/**
	 * @param $amount
	 * @param $currency
	 * @return float - amount to charge the user sending funds
	 */
	static function getTransactionAmount($amount, $currency){
		switch($currency){
			case 'USD':
			case 'CAD':
			case 'EUR':
			case 'AED':
			case 'GBP':
				return $amount + round((5/100) *$amount,2) + 0.5;
			case 'NGN' :
			default: //XAF
				return $amount + round((5/100) *$amount,0); //5% of each transaction
		}
	}

	/**
	 * @param $amount
	 * @param $currency
	 * @return float - Amount to send to the user receiving funds in the given currency
	 */
	static function getChargeAmount($amount, $currency){
		switch($currency){
			case 'USD':
			case 'EUR':
			case 'CAD':
			case 'CHF':
			case 'GBP':
			case 'AED':
					return $amount - round((5/100) *$amount, 2) - 0.5; //return -4%of the sending amount
			case 'NGN' :
			default: //XAF
				return $amount - round((5/100) *$amount,0); //-5% of the transaction
		}
	}

	private function getPage($page, $data = array(), $extra_info = ''){
		$cover = random_element($this->covers);
		$data['info'] = $extra_info;
		$data['cover'] = $cover;
		$data['cover2'] = random_element($this->covers);
		$this->load->view('public/header', $data);
		$this->load->view('public/'.$page, $data);
		$this->load->view('public/footer', $data);
	}

	private function getEmailPage($page, $data){
		return $this->load->view('public/email/'.$page, $data, true);
	}
}
