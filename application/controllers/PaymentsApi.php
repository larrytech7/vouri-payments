<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'utils/SendEmail.php');
require_once('GatewayTransactionFactory.php');
use GuzzleHttp\Psr7\Request;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;

class PaymentsApi extends CI_Controller {

	/**
	 * @date : 2019-03-05
	 * @source : https://www.xe.com
	 * @var array
	 */
	static $rates = [
		'CHF' => [
			'USD' => 1.001,
			'GBP' => 0.756,
			'XAF' => 574.00,
			'CAD' => 1.315,
			'AED' => 3.63,
			'EUR' => 0.88,
		],
		'USD' => [
			'EUR' => 0.881,
			'GBP' => 0.774,
			'NGN' => 361.797,
			'XAF' => 573.625,
			'CAD' => 1.324,
			'AED' => 3.672,
		],
		'AED' => [
			'EUR' => 0.236,
			'GBP' => 0.213,
			'NGN' => 99.454,
			'XAF' => 155.085,
			'CAD' => 0.360,
			'USD' => 0.272,
		],
		'CAD' => [
			'EUR' => 0.656,
			'GBP' => 0.592,
			'NGN' => 257.821,
			'XAF' => 430.319,
			'USD' => 0.755,
			'AED' => 2.775,
		],
		'EUR' => [
			'USD' => 0.880,
			'GBP' => 1.120,
			'NGN' => 412.351,
			'XAF' => 655.957,
			'CAD' => 1.524,
			'AED' => 4.231,
		],
		'GBP' => [
			'EUR' => 0.893,
			'USD' => 0.785,
			'NGN' => 461.953,
			'XAF' => 755.075,
			'CAD' => 1.688,
			'AED' => 4.682,
		],
		'NGN' => [
			'EUR' => 0.00243,
			'GBP' => 0.00217,
			'USD' => 0.00275,
			'XAF' => 1.59125,
			'CAD' => 0.0036,
			'AED' => 0.01,
		],
		'XAF' => [
			'EUR' => 0.00152,
			'GBP' => 0.00136,
			'NGN' => 0.62844,
			'USD' => 0.00170,
			'CAD' => 0.0023,
			'AED' => 0.0064,
		]
	];
	static $gatewayCurrency = [
		'MMO' => 'XAF',
		'OMO' => 'XAF',
		'STR' => 'USD',
		'VSA' => 'USD',
	];
	public static $API_KEY = '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';
	protected $sendgrid;
	public $email;
	protected $bitlyProvider;

    public function __construct(){
        parent::__construct();
		$this->httpAdapter = new GuzzleAdapter(null);
		$this->headers = [
			'Content-Type' => 'application/json',
			'Accept'=>'application/json',
			'Authorization'=>'Basic '.base64_encode('vouri@restadmin:vouri@rest-admin-123'),
			'auth'=>['vouri@restadmin', 'vouri@rest-admin-123']
		];
		//app language configuration
		$this->lang->load('app', $this->session->userdata('lang', 'english'));
		//libs
		$this->encryption->initialize([
			'cipher' => 'aes-256'
		]);
        $this->load->library('user_agent');
        $this->load->library('ciqrcode');

		//models
        $this->load->model('user');
        $this->load->model('contactus');
        $this->load->model('paymentrequest');

        //helpers
		$this->load->helper(array('form','date'));
		//$this->output->enable_profiler(true);
    }

	private function makeRequest($http, $url, $data=[]){
		$req = new Request($http,$url, $this->headers, json_encode($data));
		try{
			$resp = $this->httpAdapter->sendRequest($req);
			return $resp->getBody()->getContents();
		}catch(\Http\Client\Exception\NetworkException $nex){
			return json_encode([
				'transaction_status' => 401,
				'error_message' => $nex->getMessage(),
				'message' => sprintf('Network error. Unable to connect to site %s', $url)
			]);
		}
	}

	public function en(){
		//switch language to english
		$this->session->set_userdata('lang','english');
		redirect(site_url(''), 'location');
	}

	public function fre(){
		//switch language to french
		$this->session->set_userdata('lang', 'french');
		redirect(site_url(''), 'location');
	}

	/**
	 * Check if this user is compliant and has properly confirmed their full ID on the platform
	 * Level 1 : Email and Phone number confirmed. Limit : $3000/Mo
	 * Level 2 : Address, city & Country confirmed. Limit : $5000/Mo
	 * Level 3 : Occupation, ID card confirmed. Limit : $100000/Mo
	 * @param int $level of compliance
	 */
	public static function getComplianceAmount($level){
		switch($level){
			case 1:
				return 3000;
			case 2:
				return 5000;
			case 3:
				return 100000;
			default:
				return 3000;
		}
	}

	/**
	 * Receive payment request and makes request over API
	 */
	public function v1(){
		$requestData['amount'] = $this->input->post('amount');
		$requestData['currency'] = $this->input->post('currency');
		$requestData['phone_number'] = $this->input->post('phone_number');
		$requestData['payment_method'] = $this->input->post('payment_method');
		$requestData['email'] = $this->input->post('email');
		$requestData['api_key'] = $this->input->post('api_key');

		$this->headers['X-API-KEY'] = $requestData['api_key'];
		$response = $this->makeRequest('POST', site_url('api/pay'), $requestData);
		//send response to client
		$this->output
			->set_content_type('application/json', 'utf-8')
			->set_output($response,JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
	}

	/**
	 * Returns the amount to send converted to the receiving currency
	 * @param $amount - to send for conversion
	 * @param $toCurrency
	 * @param $fromCurrency
	 * @return float - sending amount with currency conversion fee
	 */
	public static function getSendingAmount($amount, $fromCurrency, $toCurrency){
		//$amount = Home::getChargeAmount($amount, $fromCurrency); //first apply transaction fee, then convert balance
		if($toCurrency == $fromCurrency)
			return $amount;
		else{
			return round($amount * Home::$rates[''.$fromCurrency][''.$toCurrency],0);
		}
	}

	/**
	 * @param $amount
	 * @param $currency
	 * @return float - amount to charge the user sending funds
	 */
	static function getTransactionAmount($amount, $currency){
		switch($currency){
			case 'USD':
			case 'CAD':
			case 'EUR':
			case 'AED':
			case 'GBP':
				return $amount + round((5/100) *$amount,2) + 0.5;
			case 'NGN' :
			default: //XAF
				return $amount + round((5/100) *$amount,0); //5% of each transaction
		}
	}

	/**
	 * @param $amount
	 * @param $currency
	 * @return float - Amount to send to the user receiving funds in the given currency
	 */
	static function getChargeAmount($amount, $currency){
		switch($currency){
			case 'USD':
			case 'EUR':
			case 'CAD':
			case 'CHF':
			case 'GBP':
			case 'AED':
					return $amount - round((5/100) *$amount, 2) - 0.5; //return -4%of the sending amount
			case 'NGN' :
			default: //XAF
				return $amount - round((5/100) *$amount,0); //-5% of the transaction
		}
	}

	/**
	 * P2P request handler to generate QR payment code used to initiate payment requests
	 * via MoMo
	 */
	public function qrMake(){
        $requestData['amount'] = $this->input->post('amount');
        $requestData['phone_number'] = $this->input->post('phone_number');
        $requestData['username'] = $this->input->post('username');
        $requestData['api_key'] = $this->input->post('api_key');

        $this->headers['X-API-KEY'] = $requestData['api_key'];
        $response = $this->makeRequest('POST', site_url('api/qrMake'), $requestData);
        //echo $response;
		//exit;
        //send response to client
        $this->output
            ->set_content_type('application/json', 'utf-8')
            ->set_output($response,JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
	}

	/*
	 * Decrypt encrypted data from part of the request that corresponds to the phone
	 * number and amount to request in the transaction
	 * @requestParam - data to decrypt and extract request parameters
	 */
	public function qrProcess(){
		$data = $this->input->get('data');
        $requestData['api_key'] = $this->input->get('api_key');
        $query = http_build_query([
            'data' => $data,
            'api_key' => $requestData['api_key']
        ]);

        $this->headers['X-API-KEY'] = $this->encryption->decrypt(base64_decode($requestData['api_key']));
        $response = $this->makeRequest('GET', site_url('api/qrProcess?'.$query), $requestData);
        //send response to client
        $this->output
            ->set_content_type('application/json', 'utf-8')
            ->set_output($response,JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }

	/*
	 * Initiate QR payment operation from previous request operation
	 */
	public function qrPay(){
        $requestData['recipient_phone_number'] = $this->input->post('recipient_phone_number');
        $requestData['recipient_username'] = $this->input->post('recipient_username');
        $requestData['amount'] = $this->input->post('amount');
        $requestData['sender_phone_number'] = $this->input->post('sender_phone_number');
        $requestData['sender_username'] = $this->input->post('sender_username');
        $requestData['api_key'] = $this->input->post('api_key');

        $this->headers['X-API-KEY'] = $this->encryption->decrypt(base64_decode($requestData['api_key']));
        $response = $this->makeRequest('POST', site_url('api/qrPay'), $requestData);
        //send response to client
        $this->output
            ->set_content_type('application/json', 'utf-8')
            ->set_output($response,JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
	}
}
