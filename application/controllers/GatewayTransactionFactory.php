<?php

/**
 * Creates the required gateway based on the constructor parameters passed
 * User: Vanessa
 * Date: 12/25/2018
 * Time: 10:39 AM
 */
use Omnipay\Omnipay;

class GatewayTransactionFactory
{

    protected $gateway;
    protected $stripe_api_keys = [
        'test' => 'sk_test_9mxeeBqmnVlKyd2qPEuFRSES',
        'live' => 'sk_live_ORpnu6lUfXYvkBI3t2AEZCW4'
    ];

    public function __construct($trans){

        switch($trans){
            case 'MMO':
                $this->gateway = Omnipay::create('Momoc');
                $this->gateway->setEmail('larryakah@gmail.com');
                break;
            case 'VSA':
            default:
                $this->gateway = Omnipay::create('Stripe');
                $this->gateway->setApiKey($this->stripe_api_keys['live']);
                break;
        }
    }

    public function getGateway(){
        return $this->gateway;
    }
}