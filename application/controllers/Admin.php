<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once('Home.php');
require_once('GatewayTransactionFactory.php');
require_once(APPPATH.'utils/SendEmail.php');
require_once(APPPATH.'utils/Utility.php');

class Admin extends CI_Controller {

    protected $gatewayCurrencyMap = [
        'STR' => 'USD',
        'MMO' => 'XAF',
        'OMO' => 'XAF',
        'VSA' => 'CHF',
    ];

    public function __construct(){
        parent::__construct();
        $this->checkLogin();
        $this->lang->load('app', $this->session->userdata('lang', 'english'));
		//$this->output->enable_profiler(true);
		
         $config['upload_path']          = './resources/images/uploads/';
         $config['allowed_types']        = 'jpg|png|jpeg';
         $config['max_size']             = 5120; //5MB
         $config['max_width']            = 1600;
         $config['max_height']           = 1400;
        $this->load->library('upload', $config);
        $this->encryption->initialize([
            'cipher' => 'aes-256'
        ]);
        //helpers
        $this->load->helper(array('form'));
        //models
		$this->load->model('user');
		$this->load->model('notification');
		$this->load->model('paymentrequest');
		$this->load->model('instrument');
		$this->load->model('subscription');
        $this->load->model('apiModel');
        //$this->session->unset_userdata('transaction');
        $this->form_validation->set_error_delimiters('<div class="error bg-danger">', '</div>');
        $usr = $this->user->getUserById($this->session->user->user_id)->result_object()[0];
        $this->session->set_userdata('user', $usr);
    }
    //user dashboard
    public function index(){
        // Load recent transactions and display
        $muser = $this->session->user;
        if($muser->role == 'admin')
            $transactions = $this->transaction->getAll();
        else
            $transactions = $this->transaction->getTransactions($muser->user_id);

        $this->getPage('content', array('transactions' =>$transactions->result_object()));
    }

    public function massmessage(){
        $message = $this->input->post('message');
        $users = $this->user->getUsers()->result_object();
        $emails = [];
        foreach($users as $user){
            $emails[$user->email] = $user->name;
            //array_push($emails, [$user->email => $user->name]);
        }
        //send Email
        $result = SendEmail::sendMassEmails($emails, 'Vouri Service updates',
            $this->getEmailPage('notification_email', ['message'=>$message]));
        if($result > 0){
            $this->session->set_flashdata('success', $this->encryption->encrypt('Message sent successfully'));
            foreach($users as $user){
                $this->notification->save([
                    'message_id' => sha1(time().$user->user_id),
                    'message' => $message,
                    'sender' => 'Vouri Payments',
                    'receiver' => $user->user_id
                ]);
            }
        }else{
            $this->session->set_flashdata('error', $this->encrypion->encrypt('Error sending message. Please try again'));
        }

        redirect('admin/notifications');
    }

    public function notifications(){
        $muser = $this->session->user;
        $notifs = $this->notification->getNotifications($muser->user_id);
        $this->getPage('notifications', array('notifications' =>$notifs->result_object()));
    }

    public function getUsers(){
        if($this->session->user->role == 'admin'){
            $users = $this->user->getUsers()->result_object();
            $this->getPage('users', ['users' => $users]);
        }else{
            redirect(site_url('admin'));
        }
    }

    public function getRequests(){
        if($this->session->user->role == 'admin'){
            $requests = $this->paymentrequest->getRequests()->result_object();
            $this->getPage('payrequests', ['requests'=>$requests]);
        }else{
            $requests = $this->paymentrequest
                ->getRequestsByEmail($this->session->user->email)
                ->result_object();
            $this->getPage('payrequests', ['requests'=>$requests]);
        }
    }

    public function approve($id){
        //approve and confirm transaction
        $updated = $this->transaction->update($id, ['status'=> 2]);
        if($updated){
            $this->session->set_flashdata('info', $this->encryption->encrypt('Success! Marked as PAID'));
            //TODO: send email to sender and receiver if there's an email for the receiver
            $transaction = $this->transaction->getTransactionById($id)->result_object();
            $t = $transaction[0];
            SendEmail::sendMail(['email' => $t->email, 'name' => $t->name],
                'Transaction Completed Summary',
                $this->getEmailPage('transaction_completedemail', (array) $t),
                ['email' => $t->receiver_email == null ? 'admin@vouriinc.com' : $t->receiver_email,
                    'name' => $t->receiver_phone == null ? 'Admin' : $t->receiver_phone ]);

        }else{
            $this->session->set_flashdata('error', $this->encryption->encrypt('Error! Try again'));
        }
        redirect('admin');
    }

    public function reject($id){
        //reject transaction
        $updated = $this->transaction->update($id, ['status'=> -1]);
        if($updated){
            $this->session->set_flashdata('info', $this->encryption->encrypt('Success! Marked as FAILED'));
            //TODO: Send failure email to sender
            $transaction = $this->transaction->getTransactionById($id)->result_object();
            $t = $transaction[0];
            SendEmail::sendMail(['email' => $t->email, 'name' => $t->name],
                'Transaction Summary',
                $this->getEmailPage('transaction_erroremail', $t),
                ['email' => $t->receiver_email == null ? 'admin@vouriinc.com' : $t->receiver_email,
                    'name' => $t->receiver_phone == null ? 'Admin' : $t->receiver_phone ]);

        }else{
            $this->session->set_flashdata('error', $this->encryption->encrypt('Error! Try again'));
        }
        redirect('admin');
    }

    //make actual funds transfer
    public function send(){
        $t = $this->session->transaction;
        $t['receive_gateway'] = $this->input->post('sendto'); //receiver's gateway
        $t['receive_currency'] = $this->gatewayCurrencyMap[$this->input->post('sendto')]; //receiver's gateway/currency
        $t['name'] = $this->input->post('name'); //
        $t['email'] = $this->input->post('email');
        $t['phone'] = $this->input->post('phone');
        $t['message'] = $this->input->post('message');
        $t['bank_name'] = $this->input->post('bank_name');
        $t['routing_number'] = $this->input->post('routing_number');
        $t['account_number'] = $this->input->post('account_number');
        $t['account_type'] = $this->input->post('account_type');
       //if user is sending from another currency than the official supported currencies, apply a conversion fee
        if($t['sendfrom'] == 'VSA' && !in_array($t['currency'], ['USD', 'GBP', 'EUR'])){ //subtract a stripe conversion fee from receiving amount
            $t['amount_to_receive'] = Home::getSendingAmount($t['amount'], $t['currency'], $t['receive_currency'])
                - (Home::$rates['GBP'][$t['currency']] * 0.3);
        }else{
            $t['amount_to_receive'] = Home::getSendingAmount($t['amount'], $t['currency'], $t['receive_currency']);
        }

        $t['transaction_id'] = time() + time()+rand(1,time());

        //var_dump($t); return;
        //Save transaction
        $this->transaction->save([
            'transaction_id' => $t['transaction_id'],
            'amount' => $t['amount_to_send'],
            'amount_sent' => $t['amount_to_receive'],
            'currency' => $t['currency'],
            'receiving_currency' => $t['receive_currency'],
            'sender_id' => $this->session->user->user_id,
            'receiver_phone' => $t['phone'],
            'receiver_email' => $t['email'],
            'receiver_name' => $t['name'],
            'bank_name' => $t['bank_name'],
            'bank_route' => $t['routing_number'],
            'bank_account_number' => $t['account_number'],
            'bank_account_type' => $t['account_type'],
            'message' => $t['message'],
            'send_gateway' => $t['sendfrom'],
            'receive_gateway' => $t['receive_gateway'] == 'VSA' ? "Bank Account" : $t['receive_gateway'],
        ]);
        SendEmail::sendMail(['email' => $this->session->user->email,
            'name' => $this->session->user->name],
            'Transaction Summary',
            $this->getEmailPage('transactionemail', $t));
        //Perform transaction
        $gw = (new GatewayTransactionFactory($t['sendfrom']))->getGateway();
        switch($t['sendfrom']){
            case 'VSA':
                //Redirect to card processing platform. TODO : Replace with abehboh app payment page
                $url = 'http://localhost:8800/index.php?';
                $data = http_build_query([
                    'r' => 'payment-processor',
                    'name' => Utility::ssl_encrypt(urlencode($this->session->user->name)),
                    'email' => Utility::ssl_encrypt(urlencode($this->session->user->email)),
                    'amount' => Utility::ssl_encrypt($t['amount_to_send']),
                    'currency' => Utility::ssl_encrypt($t['currency']),
                    'v_transaction_id' => Utility::ssl_encrypt($t['transaction_id']),
                    'X-Auth-Client' => Utility::ssl_encrypt(urlencode('vouriinc.com/process')),
                ]);
                //TODO : Encrypt the entire data params to increase security
                //TODO : Remove comment and comment current redirect
                //redirect($url.$data, 'redirect');
                redirect('home/unavailable');

                /*
                try {
                   $token = $this->input->post('stripeToken');
                    $response = $gw->purchase([
                        'amount' => $t['amount_to_send'],
                        'currency' => $t['currency'],//$this->gatewayCurrencyMap[$t['sendfrom']],
                        'returnUrl' => site_url('return/visa'),
                        //'card' => $card,
                        'token' => $token,
                    ])->send();
                    if ($response->isSuccessful()) {
                        //$this->transaction->update($t['transaction_id'], ['status' => 1]);
                        $this->session->set_flashdata('info', $this->encryption->encrypt('Success! Your transaction is currently being processed and would complete soon'));
                        //send ok email
                        $t['status'] = 0;
                        SendEmail::sendMail(['email' => $this->session->user->email,
                            'name' => $this->session->user->name],
                            'Transaction Summary',
                            $this->getEmailPage('transactionemail', $t));
                        //TODO: Now send corresponding amount to the receiver
                    } elseif ($response->isRedirect()) { //redirect to offsite payment gateway
                        $response->redirect();
                    } else { //payment failed
                        $this->transaction->update($t['transaction_id'], ['status' => -1]);
                        $this->session->set_flashdata('error', $this->encryption->encrypt('Error! ' . $response->getMessage()));
                    }
                }catch(\Omnipay\Common\Http\Exception $ex){
                    $this->session->set_flashdata('error', $this->encryption->encrypt('A network error has occurred. Please ensure you are properly connected to a working network. '
                        .$ex->getMessage()));
                }catch(\Omnipay\Common\Exception\InvalidCreditCardException $cce){
                    $this->session->set_flashdata('error', $this->encryption->encrypt($cce->getMessage().' Please make sure your credit card is valid.'));
                }catch(Exception $ex){
                    $this->session->set_flashdata('error', $this->encryption->encrypt('General failure. '
                        .$ex->getMessage()));
                }
                */
                break;
            case 'MMO':
                try {
                    //charge sender
                    $response = $gw->purchase([
                        '_amount' => $t['amount_to_send'],
                        '_tel' => $this->session->user->phone
                    ])->send();
                    if ($response->isSuccessful()) {
                        $this->session->set_flashdata('info', $this->encryption->encrypt('Success! Your transaction is currently being processed and would complete soon'));
                        //send ok email
                        $t['status'] = 0;
                        SendEmail::sendMail(['email' => $this->session->user->email,
                            'name' => $this->session->user->name],
                            'Transaction Summary',
                            $this->getEmailPage('transactionemail', $t));
                        //TODO: Now send corresponding amount to the receiver
                    } else {
                        $this->transaction->update($t['transaction_id'], ['status' => -1]);
                        $this->session->set_flashdata('error', $this->encryption->encrypt('An error has occurred on your transaction. Ensure your mobile money number is correct try again'));
                    }
                }catch (\Omnipay\Common\Http\Exception $ex){
                    $this->session->set_flashdata('error', $this->encryption->encrypt($ex->getMessage()));
                }catch(\GuzzleHttp\Exception\ConnectException $con){
                    $this->session->set_flashdata('error', $this->encryption->encrypt($con->getMessage()));
                }
                break;
        }
        //RESET transaction
        $this->session->unset_userdata('transaction');
        redirect(site_url('admin'));
    }

    function pay(){
        $gw = (new GatewayTransactionFactory('MMO'))->getGateway();
        $response = $gw->purchase([
            '_amount' => 200,
            '_tel' => 237678656032
        ])->send();
        var_dump($response->getMessage());
    }

    //handle return from visa/mastercard confirmation
    public function handlevisa(){
        // Perform transaction status update;
        //var_dump(Utility::ssl_decrypt(urldecode($this->input->get('token'))));
        //die(var_dump(Utility::ssl_decrypt(urldecode($this->input->get('transaction_status')))));
        $v_transaction_id = Utility::ssl_decrypt(urldecode($this->input->get('v_transaction_id')));
        $transaction_ref = Utility::ssl_decrypt(urldecode($this->input->get('transaction_id')));
        $transaction_status = Utility::ssl_decrypt(urldecode($this->input->get('transaction_status')));
        $error = Utility::ssl_decrypt(urldecode($this->input->get('error')));

        if($error){
            //Error occurred. Inform the user, and update transaction to failed.
            $updated = $this->transaction->update($v_transaction_id,
                [
                    'status'=> -1,
                    'transaction_reference' => $transaction_ref
                ]);
            $trace = Utility::ssl_decrypt($this->input->get('trace'));
            log_message('error', $trace);
            $this->session->set_flashdata('error', $this->encryption->encrypt($this->lang->line('transaction_error')));
            $transaction = $this->transaction->getTransactionById($v_transaction_id)->result_object();
            $t = $transaction[0];
            SendEmail::sendMail(['email' => $t->email, 'name' => $t->name],
                'Transaction Summary',
                $this->getEmailPage('transaction_erroremail', $t),
                ['email' => $t->receiver_email == null ? 'admin@vouriinc.com' : $t->receiver_email,
                    'name' => $t->receiver_phone == null ? 'Admin' : $t->receiver_phone ]);

        }else{
            $this->transaction->update($v_transaction_id, [
                    'status'=> $transaction_status,
                    'transaction_reference' => $transaction_ref
                ]);
            $this->session->set_flashdata('info', $this->encryption->encrypt($this->lang->line('transaction_processing')));
        }
        //RESET transaction
        $this->session->unset_userdata('transaction');
        redirect(site_url('admin'));
    }

    // manage subscriptions
    public function subscriptions(){
        $this->form_validation->set_rules('email', 'Email', 'required|min_length[3]',
            ['required' => 'A valid email is required']);
        /*
        $this->form_validation->set_rules('email', 'Email', 'is_unique[user.email]',[
            'valid_email' => 'We require that you enter a valid email',
            'is_unique' => 'This email is already in use. You may login to continue'
        ]);
        */
        if($this->form_validation->run() == true){
            //save api key
            $user['email'] = $this->input->post('email');
            $user['level'] = 1;
            //create a key for this user and return credentials
            $user['api_key'] = crypt($user['email'], md5(time().'-')).''.crypt($user['email'], time().'-');
            $user['date_created'] = time();
            $added = false;
            try{
                $added = $this->apiModel->insert($user);
            }catch (Exception $ex){
                log_message('error', 'Failure to insert data. '.$ex->getTraceAsString());
            }
            $this->session->set_flashdata($added ? 'success' :'error',
                $this->encryption->encrypt($added ? 'YOUR API KEY IS : '.$user['api_key'] : 'Error Creating your API key please try again')
            );
            redirect('admin/subscriptions');
        }else{
            //get payment instruments for this user or all users if admin
            if($this->session->user->role == 'admin'){
                $subscriptions = $this->subscription->getAll()->result_object();
                $settings['subscriptions'] = $subscriptions;
            }else{
                $subscriptions = $this->subscription->getAll(['user.user_id'=>$this->session->user->user_id])->result_object();
                $settings['subscriptions'] = $subscriptions;
            }
            $apiKey = $this->apiModel->getByEmail($this->session->user->email)->result_object();
            $settings['api_key'] = array_key_exists(0, $apiKey) ? $apiKey[0]->api_key : '';
            $this->getPage('subscriptions', $settings);
        }
    }

   //logout destroying all sessions
    public function logout(){
        //destroy current session
        $this->session->unset_userdata(array('loggedin','user'));
        redirect('login', 'location');
    }

   //view/update user's profile
    public function profile(){
        $settings = []; //get this user's settings

        $this->form_validation->set_rules('name', 'Name', 'required|min_length[3]',
            ['required' => 'A valid name is required']);
        $this->form_validation->set_rules('phone', 'Phone number', 'numeric',
            ['numeric' => 'Ensure you enter your phone number without the country code. E.g 6903.....']);
        /*
        $this->form_validation->set_rules('email', 'Email', 'is_unique[user.email]',[
            'valid_email' => 'We require that you enter a valid email',
            'is_unique' => 'This email is already in use. You may login to continue'
        ]);
        */
        if($this->form_validation->run() == true){
            //save update
            $data['name'] = $this->input->post('name');
            $data['phone'] = $this->input->post('phone');
            $data['address'] = $this->input->post('address');
            $data['city'] = $this->input->post('city');
            $data['country'] = $this->input->post('country');
            $data['monthly_income'] = $this->input->post('income');
            $data['occupation'] = $this->input->post('occupation');
            $data['national_idcard'] = $_FILES['nid']['filename'];
            $result = $this->user->update($this->session->user->user_id, $data);
            if($result){
                $this->session->set_flashdata('info', $this->encryption->encrypt('Account updated successfully'));
            }else{
                $this->session->set_flashdata('error', $this->encryption->encrypt('Error updating account. Please try again later'));
            }
            redirect('settings');
        }else{
            //get payment instruments for this user or all users if admin
            if($this->session->user->role == 'admin'){
                $instruments = $this->instrument->getInstruments()->result_object();
                $settings['instruments'] = $instruments;
            }else{
                $instruments = $this->instrument
                    ->getUserInstruments($this->session->user->email)
                    ->result_object();
                $settings['instruments'] = $instruments;
            }
            $this->getPage('profile', $settings);
        }
    }

    public function instrument($action, $id){
        if($action == 'delete'){
            $deleted = $this->instrument->delete($id);
            if($deleted){
                $this->session->set_flashdata('info', $this->encryption->encrypt($this->lang->line('success')));
            }else{
                $this->session->set_flashdata('error', $this->encryption->encrypt($this->lang->line('error')));
            }
            redirect('settings');
        }
    }

    public function account($delete, $id){
        if($delete == 'delete'){
            $deleted = $this->user->delete($id);
            if($deleted){
                $this->session->set_flashdata('info', $this->encryption->encrypt($this->lang->line('delete_account_message')));
            }else{
                $this->session->set_flashdata('error', $this->encryption->encrypt('Error deleting account. Please try again later'));
            }
            $this->logout();
        }
    }

    //get the required page content for a given request
    private function getPage($page, $data = array()){
        //get compliance limit
        $data['transfer_limit'] = Home::getComplianceAmount($this->session->user->kyc_level);
        $this->load->view('admin/header', $data);
		$this->load->view('admin/'.$page, $data);
		$this->load->view('admin/footer', $data);
    }

    private function getEmailPage($page, $data){
        return $this->load->view('public/email/'.$page, $data, true);
    }

    //check logged-in state
    private function checkLogin(){
        //if user is not logged-in redirect user
        $logged_in = $this->session->has_userdata('loggedin');
		if($this->session->temp_url && $logged_in){
            $url = $this->session->temp_url;
            $this->session->unset_userdata('temp_url');
            redirect($url);
        }
        if(!$logged_in){
            //logged out so redirect to home
			$this->session->set_flashdata('info', $this->encryption->encrypt('You must login to proceed'));
            $this->session->set_userdata('temp_url', site_url('sendfunds'));
            redirect(site_url('login'));
        }
    }
}