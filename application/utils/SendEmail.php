<?php

/**
 * Uses the SendGrid API to send emails.
 * User: Vanessa
 * Date: 12/21/2018
 * Time: 9:34 PM
 */
class SendEmail
{
    public static $API_KEY = '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';

    public static function sendMail($to = ['email' => 'hello-care@vouriinc.com', 'name' => 'Vouri Inc'],
                                    $subject, $content, $bcc = ['email' => 'hello-care@vouriinc.com', 'name' => 'Vouri Inc Payments'],
                                    $from = ['email'=>'no-reply@vouriinc.com', 'name' =>'Vouri Payments']){
        //sendgrid email configuration
        $sendgrid = new \SendGrid(SendEmail::$API_KEY);
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom($from['email'], $from['name']);
        $email->addBcc($bcc['email'], $bcc['name']);
        $email->addBcc('icep603@gmail.com','Vouri');
        //Send email
        $email->setSubject($subject);
        $email->addTo($to['email'],$to['name']);
        $email->addContent('text/html', $content);
        $response = 0;
        try{
            $response = $sendgrid->send($email);
            return $response->statusCode();
        }catch (Exception $ex){
            log_message('error', $ex->getMessage());
            log_message('error', $ex->getFile().$ex->getLine());
            log_message('error', $ex->getTraceAsString());
        }
        return $response;
    }

    public static function sendMassEmails($to,
                                    $subject, $content, $bcc = ['email' => 'hello-care@vouriinc.com', 'name' => 'Vouri Inc Payments'],
                                    $from = ['email'=>'no-reply@vouriinc.com', 'name' =>'Vouri Payments']){
        //sendgrid email configuration
        $sendgrid = new \SendGrid(SendEmail::$API_KEY);
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom($from['email'], $from['name']);
        $email->addTo($bcc['email'], $bcc['name']);
        $email->addBccs($to);
        //$email->addBcc('icep603@gmail.com','Vouri');
        //Send email
        $email->setSubject($subject);
        //$email->addTos($to);
        $email->addContent('text/html', $content);
        $response = 0;
        try{
            $response = $sendgrid->send($email);
            return $response->statusCode();
        }catch (Exception $ex){
            log_message('error', $ex->getMessage());
            log_message('error', $ex->getFile().$ex->getLine());
            log_message('error', $ex->getTraceAsString());
        }
        return $response;
    }

}