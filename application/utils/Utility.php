<?php

/**
 * Uses the SendGrid API to send emails.
 * User: Vanessa
 * Date: 12/21/2018
 * Time: 9:34 PM
 */
class Utility
{
    static $RATES = [
        'USD' => 580,
        'EUR' => 655,
        'GBP' => 720
    ];

    public static function convertToLocal($amount = 0, $currency = 'CFA'){
        switch(strtoupper($currency)){
            case 'CFA':
                return $amount;
            default :
                return intval($amount * Utility::$RATES[strtoupper($currency)]);
        }
    }

    public static function ssl_encrypt($data){
        $method = 'AES-256-CBC';
        $key = 'e$25LxacllokgeH..e$25LxacllokgeH';
        //generate the IV
        $length = openssl_cipher_iv_length($method);
        $iv = openssl_random_pseudo_bytes($length);
        $encrypted = openssl_encrypt($data, $method, $key, OPENSSL_RAW_DATA, $iv);
        return base64_encode($encrypted).':'.base64_encode($iv);
    }

    public static function ssl_decrypt($encrypted){
        $method = 'AES-256-CBC';
        $key = 'e$25LxacllokgeH..e$25LxacllokgeH';
        //set the IV
        list($data, $iv) = explode(':', $encrypted);
        $iv = base64_decode($iv);
        $decrypted = openssl_decrypt($data, $method, $key, 0, $iv);
        return $decrypted;
    }
}