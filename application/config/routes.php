<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

//start api routes
$route['api/account/apply'] = 'api/apiSignup'; //signup for api access
$route['api/account/auth'] = 'api/apiAuth'; //authenticate to get authentication token
$route['api/user/signup'] = 'api/signup';
$route['api/user/login'] = 'api/login';
$route['api/user/activate/(:any)'] = 'api/activateUser/$1';
$route['api/user/transactions'] = 'api/getUserTransactions';
$route['api/user/transaction'] = 'api/getTransaction';

$route['api/convert'] = 'api/convertPayment';
$route['api/refreshtoken'] = 'api/refreshToken';
$route['api/makepayment'] = 'api/makePayment';
$route['api/getaccount'] = 'api/getAccount';
$route['api/users'] = 'api/getUsers';
//payment REST api routes
$route['payments/api/v1'] = 'PaymentsApi/v1';
//P2P QR payment routes
$route['p2p/v1/qrmake'] = 'PaymentsApi/qrMake';
$route['p2p/v1/qrprocess'] = 'PaymentsApi/qrProcess';
$route['p2p/v1/qrpay'] = 'PaymentsApi/qrPay';
//end api routes

//general routes
$route['sendfunds'] = 'home/sendfunds';
$route['convert'] = 'home/convert';
$route['privacy'] = 'home/privacy';
$route['terms'] = 'home/terms';
$route['fees'] = 'home/fees';
$route['api'] = 'home/api';
$route['about'] = 'home/about';
$route['paymentrequest'] = 'home/requestPayment';
$route['notifications'] = 'admin/notifications';
$route['payrequests'] = 'admin/getRequests';
$route['users'] = 'admin/getUsers';
$route['send'] = 'admin/send';
$route['settings'] = 'admin/profile';
$route['paymentprocess/callback'] = 'admin/handlevisa';

//authentication
$route['user/activate/(:any)'] = 'home/activate/$1';
$route['login'] = 'home/login';
$route['signup'] = 'home/signup';
$route['logout'] = 'admin/logout';

$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
