<?php
/**
 * System messages translation for CodeIgniter(tm)
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014-2018, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['profiler_database']        = 'BASE DE DONNEES';
$lang['profiler_controller_info'] = 'CLASSE/METHODE';
$lang['profiler_benchmarks']      = 'BENCHMARKS';
$lang['profiler_queries']         = 'REQUETES';
$lang['profiler_get_data']        = 'DONN&EAcute;ES GET';
$lang['profiler_post_data']       = 'DONN&EAcute;ES POST';
$lang['profiler_uri_string']      = 'URI STRING';
$lang['profiler_memory_usage']    = 'UTILISATION MEMOIRE';
$lang['profiler_config']          = 'VARIABLES CONFIG';
$lang['profiler_session_data']    = 'DONNEES SESSION';
$lang['profiler_headers']         = 'EN-TETES HTTP';
$lang['profiler_no_db']           = "Le driver de la base de donn�es n'est actuellement pas charg�";
$lang['profiler_no_queries']      = "Aucune requ�te n'a �t� ex�cut�e";
$lang['profiler_no_post']         = "Aucune donn�e POST n'existe";
$lang['profiler_no_get']          = "Aucune donn�e GET n'existe";
$lang['profiler_no_uri']          = "Aucune donn�e URI n'existe";
$lang['profiler_no_memory']       = 'Utilisation de la m�moire indisponible';
$lang['profiler_no_profiles']     = 'Pas de donn�es du profil - toute section est desactiv�e.';
$lang['profiler_section_hide']    = 'Cacher';
$lang['profiler_section_show']    = 'Montrer';
$lang['profiler_seconds']         = 'secondes';