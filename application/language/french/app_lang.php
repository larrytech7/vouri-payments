<?php
/**
 * Created by PhpStorm.
 * User: Vanessa
 * Date: 1/21/2019
 * Time: 9:07 PM
 */
$lang['app_name'] = "VouriInc pay";
$lang['app_short_description'] = "Aggregateur mondiale de syst&egrave;mes de paiement mobile et web";
$lang['app_long_desc'] = " Nous sommes le num&eacute;o un dans l'aggregation des syst&egrave;mes de paiement rendu possible avec Omnipay. Nous avons la ferme conviction que les transactions financiers pourrais devenir plus simple et toujours aussi s&eacute;curis&eacute;.";
$lang['menu_dashboard'] = "Dashboard";
$lang['home_page_title_one'] = "Faite le en un click";
$lang['home_page_title_two'] = "Paiement universel tout en un";
$lang['home_page_title_one_desc'] = "l'envoie et retrait d'argent n'a jamais &eacute;t&eacute; aussi simple a travers le monde.";
$lang['home_page_title_two_desc'] = "Une interface simple et efficace a integrer.";
$lang['home_page_type_1'] = "Les transactions rapide et s&eacute;curis&eacute; rendent la vie meilleur pour tous.";
$lang['home_page_type_2'] = " integrez beaucoup de syst&egrave;mes de paiement en ligne &agrave; travers un seul API.";
$lang['send_money'] = "Envoyez l'argent";
$lang['get_money'] = "Recevez l'argent";
$lang['service_fees'] = "Frais de transaction";
$lang['payment_dialog_title'] = "Envoyer de l'argent &agrave; qui que ce soit via le mode de paiement qui vous convient le mieux.";
$lang['payment_dialog_amount'] = "Montant &agrave; envoyer";
$lang['payment_dialog_currency'] = "Choisissez la devise";
$lang['payment_dialog_gateway'] = "Mode de paiement a utiliser";
$lang['payment_dialog_go'] = "Continuer";
$lang['verify_card'] = "V&eacute;rification de Carte";

$lang['password_reset_heading'] = "Changez votre mot de passe ici";
$lang['lost_password'] = "Mot de passe oublier? Changez";
$lang['recover'] = "Recouvrir";
$lang['reset'] = "R&eacute;initialiser";
$lang['reset_password'] = "Recouvrez le mot de passe de votre compte";
$lang['password_confirm'] = "Confirmez votre mot de passe";
$lang['new_password'] = "Nouveau mot de passe";

$lang['card_button_label'] = "Envoyer";
$lang['card_button_label_pay'] = "Payer (x100)";
$lang['followus'] = "Suivez nous sur";
$lang['followus_text'] = "";
$lang['function_title'] = "What you can do";
$lang['f1'] = "Transfert d'argent";
$lang['f1t'] = "Envoyer de l'argent &agrave; partir de n'importe quel syst&egrave;me de paiement en ligne";
$lang['f2'] = "Recevez de l'argent";
$lang['f2t'] = "Envoyer des demande de transfer d'argent &agrave; et soyez payer &agrave; dans votre Wallet de choix.";
$lang['f3'] = "Integrer l'API";
$lang['f3t'] = "Integrez en toute simplicit&eacute; un API universel pour accepter un ensemble de syst&egrave;me de paiement mobile et web.";
$lang['f4'] = "Leverage a single API and support multiple payment options";
$lang['f4t'] = "Nous permettons au utilisateurs d'envoyer et de recevoir les paiements de differents source de moyens de paiements ou de transfert d'argent comme mobile money ou Stripe.<br/>
                        Nous mettons a disposition une interface pour permettre au marchands internet d'integrer une solution de paiement dynamique a son site web de e-commerce";
$lang['f5'] = "Integrations Web/Mobile";
$lang['f6'] = "Transactions r&eacute;ussi";
$lang['vouri_exchange'] = "Devises Vouri";
$lang['source_title'] = "Mis &agrave; jour,  Mars 5, 2019";
$lang['exchange_text'] = "Nos taux de change sont bas&eacute; sur les donn&eacute;es prises sur xe.com et periodiquement ajuster pour avoir un meilleur taux de change pour mieux servir nos clients.";
$lang['verify_button_label'] = "VERIFIER ICI!";
$lang['continue'] = "VERIFIER!";
$lang['network_error'] = "Une erreure de connexion est survenu. Assurez vous d'avoir une connexion Internet fiable.";
$lang['card_error'] = "D&eacute;sol&eacute;! Votre carte n'est pas compatible/accept&eacute; sur Vouri. Veuillez changer de carte et essayer encore.";
$lang['card_success'] = "Bravo! Votre carte est compatible sur Vouri et peut donc effectuer des transactions.";
$lang['verify_card_note'] = 'V&eacute;rifiez votre carte ici. Vous serez temporairement debit&eacute; de 1CHF mais serez rembours&eacute; apr&egrave;s l\'op&eacute;ration de v&eacute;rification';
$lang['login_heading'] = "Connectez-vous afin d'effectuer vos transactions en s&eacute;curit&eacute;";
$lang['login'] = "Connexion";
$lang['options'] = "Actions";
$lang['download_mydata'] = "Download My Transactions";
$lang['delete_account_message'] = "Votre compte &agrave; &eacute;t&eacute; supprimer avec succ&egrave;s";
$lang['delete_account'] = "Supprimer Mon Compte";
$lang['delete_account_note'] = "La suppression de votre compte entrainera aussi la supression de vos don&eacute;es de transactions. Etes vous sur de vouloir continuer cet op&eacute;ration?";
$lang['login_transact'] = "Connectez-vous pour continuer";
$lang['signup'] = "Cr&eacute;er un compte";
$lang['contact'] = "Contactez-nous";
$lang['subject'] = "Objet";
$lang['message'] = "Message";
$lang['dialog_contact'] = "Laisser nous un message ici.";
$lang['signup_note'] = "En vous inscrivant, vous ACCEPTER nos ";
$lang['contact_success'] = "Envoy&eacute;! Nous allons tr&egrave;s bientot vous r&eacute;pondre";
$lang['contact_error'] = "Votre message n'a pas pu etre envoyer avec succ&egrave;s. Veuillez re-essayer encore";
$lang['contact_success'] = "Envoy&eacute;! Nous alons vous repondre bientot";
$lang['contact_error'] = "Votre message n'a pas &eacute;t&eacute; envoyer avec succ&egrave;s. Veuillez re-essayer encore";
$lang['rights_reserved'] = "Tout droits reserv&eacute;";
$lang['keys'] = "L&eacute;gende";
$lang['transaction_history'] = "Historique des transactions";
$lang['transaction_to'] = "Envoy&eacute; &agrave;";
$lang['transaction_from'] = "DE";
$lang['transaction_to_'] = "&Agrave;";
$lang['transaction_charged'] = "Charged";
$lang['transaction_received'] = "Recu";
$lang['transaction_date'] = "Date";
$lang['transaction_status'] = "Status";
$lang['paid'] = "PAY&Eacute;";
$lang['failed'] = "ECHEC";
$lang['pending'] = "EN COURS";
$lang['send'] = "Envoyer";
$lang['receive'] = "Recever";
$lang['exit_page_message'] = "Hey! ne partez pas si vite SVP. Pouvez vous nous dire comment on peut mieux vous aider?";
$lang['email'] = "Email";
$lang['email_text'] = "Entrez votre adresse email valide";
$lang['name'] = "Nom";
$lang['name_text'] = "Votre nom";
$lang['phone'] = "Num&eacute;ro de t&eacute;l&eacute;phone";
$lang['phone_text'] = "Num&eacute;ro de t&eacute;l&eacute;phone";
$lang['password'] = "Mot de passe";
$lang['password_text'] = "Votre mot de passe (plus de 8 caract&egrave;res)";
$lang['password_re'] = "Confirm&eacute; mot de passe";
$lang['password_re_text'] = "Confirm&eacute; votre mot de passe";
$lang['no_account_text'] = "Vous n'avez pas de compte?";
$lang['create_account_text'] = "Cr&eacute;er votre compte gratuitement!";
$lang['features'] = "Service principaux";
$lang['leave_message'] = "Laissez nous un message pour exprimer votre interet a integrer notre API. Il fonctionnera exactement comme le syst&egrave;me actuellement";
$lang['tryit'] = "Essayer!";
$lang['success'] = "Op&eacute;ration r&eacute;ussi!";
$lang['error'] = "Echec!";

$lang['payment_instruments'] = "Vos instruments des transactions";
$lang['instrument_name'] = "Nom/email";
$lang['instrument_type'] = "Type";
$lang['instrument_number'] = "Num&eacute;ro";
$lang['instrument_expiration'] = "Date d'expiration";
$lang['instrument_origin'] = "Origin";
$lang['instrument_fingerprint'] = "Empreinte digitale";
$lang['remove_instrument'] = "Supprimer cet instrument";

$lang['how'] = "Comment ca marche?";
$lang['how_desc'] = "En <b>3</b> etapes seulement, effectuer vos transactions ver l'Afrique.";
$lang['step1'] = "Entrez le montant &agrave; envoyer et la devise en suite choissisez le mode de paiement qui vous convient.";
$lang['step2'] = "Selectionnez le mode de reception de votre destinataire puis entrez ses details personnel.";
$lang['step3'] = "Si vous ne vous etes pas encore connect&eacute;, connectez-vous pour finir la transaction.";
$lang['sender_info'] = 'Details d\'envoie';
$lang['receiver_info'] = 'Details de reception';
$lang['make_payment'] = 'Validation du transfert';

$lang['learn_more'] = "Lire plus";
$lang['translate_not_available'] = "D&eacute;sol&eacute;, la traduction est limit&eacute; sur cette page";
$lang['usechat'] = "Utilisez la chat";
$lang['joinus'] = "Vous souhaiter rejoindre l'equipe?";
$lang['joinus_desc'] = "We look out for great talent passionate about advancing our vision for a better and more organised payment infrastructure.
                        If you are passionate and talented to join our team of dreamers and guardians of the galaxy, drop us a note.";
$lang['feature_one_title'] = "Paiement et transfert d'argent inter-connecter";
$lang['feature_one_desc'] = "Envoyez l'argent &agrave partir de et de plusieurs type de syst&egrave;me de paiement en ligne. Envoyer l'argent d'une carte VISA/MASTERCARD ver un compte mobile/Orange money.";
$lang['feature_two_title'] = "Integrer un seul API pour tout type de paiement";
$lang['feature_two_desc'] = "Avec un seul point d'integration, il vous serait possible d'accepter via votre application, les syst&egrave;m&egrave;me de paiement en ligne les plus populaire
. Ceci augmente le nombre de personnes pouvant acheter sur votre application ou site internet.";
$lang['about_title'] = "Nous sommes Vouri!";
$lang['about_desc'] = "Note mission est de restaurer l'armonie dans ce syst&egrave;me de chaos qui regne dans le monde des paiement et transfert d'argent
                        en creant un pont simple entre les syst&egrave;me de paiement web et mobile les plus populaire pour simplifier ces transactions.";
$lang['about_tag'] = "Prenez avantage de la simplicit&eacute; et focaliser vous sur la plus valu en ver vos utilisateurs.";
$lang['api_title'] = "Faites connaissance avec nos API";
$lang['api_features'] = "API features";
$lang['mobile_sdk'] = "Mobile SDK";
$lang['pause_note'] = "En cours de d&eacute;veloppement.";
$lang['mobile_sdk_1'] = "Connexion et g&eacute;n&eacute;ration de sa cl&eacute; API sur ton dashboard.";
$lang['mobile_sdk_2'] = "Ajoute la dependence &agrave; ton fichier de configuration.";
$lang['mobile_sdk_3'] = "Ajoute ta cl&eacute; API comme parametre du constructeur.";
$lang['mobile_sdk_4'] = "Test avec les coordon&eacute;s de test fourni";
$lang['web_api'] = "Web API";
$lang['api'] = "API";
$lang['web_api_1'] = "Localise ta cl&eacute; API et applicationID.";
$lang['web_api_2'] = "Ajoute ton nom de domain &agrave; la liste des domaine OAuth2";
$lang['web_api_3'] = "Importe la librairie JS dans ton projet via npm ou via le CDN fourni.";
$lang['web_api_4'] = "Test with sample request";
$lang['api_desc'] = "Seamlessly add our intuitive and powerful APIs to your web or mobile applications in seconds. Go live almost instantly after that.";
$lang['api_theme'] = "Designed for humans, by humans and made available to the machines.";
$lang['api_note'] = "While most parts of our API infrastructure is currently under construction, we encourage you to signup to our <?= anchor('signup', 'NewsLetter')?> and be the first to
        see what we have cooked for you. However, we plan for a seamless integration in the following steps below.";
$lang['privacy_extended'] = "Avant de lire cet politique de confidentialit&eacute;, nous vous informons que les termes vu sur Stripe sont aussi applicable a l'utilisation de ce service. Veuillez prendre connaissance de celle ci ";
$lang['privacy_title'] = "Our Privacy Policy";
$lang['privacy_desc'] = "Our number one concern is the privacy of user accounts, user transactions and data without which we won't be able to provide
                        the seamless and hassle free service that we do. We have designed an iron clad policy that works for everyone.";
$lang['privacy_preamble'] = "Preamble";
$lang['privacy_preamble_desc'] = "Vouri Inc Pay obtains Personal Data about you from various sources to provide our Services and to manage our Sites.
'You' may be a visitor to one of our websites, a user of one or more of our Services ('User' or 'Vouri Inc Pay User'), or a customer of a User ('Customer').
If you are a Customer, Vouri Inc Pay will generally not collect your Personal Data directly from you. Your agreement with the relevant Vouri Inc Pay User should explain how Vouri Inc Pay User shares your Personal Data with Vouri Inc Pay,
and if you have questions about this sharing, then you should direct those questions to the Vouri Inc Pay User.
Every data you generate directly on this platform while using its services belong to you and you alone.
                                You can at any time request that your data be completely cleaned and all transactions archived whenever you wish to
                                close your account.";
$lang['privacy_1'] = "Personal Data";
$lang['privacy_1_desc'] = "Personal Data is any information that relates to an identified or identifiable individual. The Personal Data that you provide directly to us through our Sites will be apparent from the context in which you provide
the data. In particular: When you register for a Vouri Inc Pay account we collect your full name, email address, and account log-in credentials.
When you fill-in our online form to contact our sales team, we collect your full name, work email, country. <br/>
When you respond to Vouri Inc Pay emails or surveys we collect your email address, name and any other information you choose to include in the body of your email or responses. If you contact us by phone, we will collect the phone number you use to call Vouri Inc Pay. If you contact us by phone as a Vouri Inc Pay User, we may collect additional information in order to verify your identity.
<br/>
When you respond to Vouri Inc Pay emails or surveys we collect your email address, name and any other information you choose to include in the body of your email or responses. If you contact us by phone, we will collect the phone number you use to call Vouri Inc Pay. If you contact us by phone as a Vouri Inc Pay User, we may collect additional information in order to verify your identity.
<br/>
When we conduct fraud monitoring, prevention and detection activities, we may also receive Personal Data about you from our business partners, financial service providers, identity verification services, and publicly available sources (e.g., name, address, phone number, country), as necessary to confirm your identity and prevent fraud. Our fraud monitoring, detection and prevention services may use technology that helps us assess the risk associated with an attempted transaction that is enabled on the Vouri Inc Pay User�s website or the application that collects information.
<br>
Your account can only be accessed via an email/phone and a password that we advise you to change regularly in order to
                                avoid being victim of any incidental hack. We however take on the responsibility of making sure that access to your
                                account is as secure as possible and only you can have access at any given time. Your account information is never shared with
                                any third party service unless for procedures that have to deal with money laundering and only after prompt notification about this
                                to you via email and/or other means available.
                                Your account activity is monitored and logged in order to detect suspicious activity or access from different regions.";
$lang['privacy_2'] = "Using Personal Data";
$lang['privacy_2_desc'] = "We rely upon a number of legal grounds to ensure that our use of your Personal Data is compliant with applicable law. We use Personal Data to facilitate the business relationships we have with our Users, to comply with our financial regulatory and other legal obligations, and to pursue our legitimate business interests. We also use Personal Data to complete payment transactions and to provide payment-related services to our Users<br>
We rely upon a number of legal grounds to ensure that our use of your Personal Data is compliant with applicable law. We use Personal Data to facilitate the business relationships we have with our Users, to comply with our financial regulatory and other legal obligations, and to pursue our legitimate business interests. We also use Personal Data to complete payment transactions and to provide payment-related services to our Users
<br>
We rely upon a number of legal grounds to ensure that our use of your Personal Data is compliant with applicable law. We use Personal Data to facilitate the business relationships we have with our Users, to comply with our financial regulatory and other legal obligations, and to pursue our legitimate business interests. We also use Personal Data to complete payment transactions and to provide payment-related services to our Users
<br>
We rely upon a number of legal grounds to ensure that our use of your Personal Data is compliant with applicable law. We use Personal Data to facilitate the business relationships we have with our Users, to comply with our financial regulatory and other legal obligations, and to pursue our legitimate business interests. We also use Personal Data to complete payment transactions and to provide payment-related services to our Users";
$lang['privacy_3'] = "Personal Data disclosure";
$lang['privacy_3_desc'] = "Vouri Inc Pay does not sell or rent Personal Data to marketers or unaffiliated third parties. We share your Personal Data with trusted entities, as outlined below <br>
We share Personal Data with other Vouri Inc Pay entities in order to provide our Services and for internal administration purposes
<br>
We share Personal Data with a limited number of our service providers. We have service providers that provide services on our behalf, such as identity verification services, website hosting, data analysis, information technology and related infrastructure, customer service, email delivery, and auditing services. These service providers may need to access Personal Data to perform their services. We authorize such service providers to use or disclose the Personal Data only as necessary to perform services on our behalf or comply with legal requirements. We require such service providers to contractually commit to protect the security and confidentiality of Personal Data they process on our behalf. Our service providers are predominantly located in the European Union and the United States of America, and Africa
<br>
In the event that we enter into, or intend to enter into, a transaction that alters the structure of our business, such as a reorganization, merger, sale, joint venture, assignment, transfer, change of control, or other disposition of all or any portion of our business, assets or stock, we may share Personal Data with third parties for the purpose of facilitating and completing the transaction
<br>
We share Personal Data as we believe necessary: (i) to comply with applicable law, or payment method rules; (ii) to enforce our contractual rights; (iii) to protect the rights, privacy, safety and property of Vouri Inc Pay, you or others; and (iv) to respond to requests from courts, law enforcement agencies, regulatory agencies, and other public and government authorities, which may include authorities outside your country of residence";
$lang['privacy_4'] = "Security and Retention";
$lang['privacy_4_desc'] = "We make reasonable efforts to ensure a level of security appropriate to the risk associated with the processing of Personal Data. We maintain organizational, technical and administrative measures designed to protect Personal Data within our organization against unauthorized access, destruction, loss, alteration or misuse. Your Personal Data is only accessible to a limited number of personnel who need access to the information to perform their duties. Unfortunately, no data transmission or storage system can be guaranteed to be 100% secure. If you have reason to believe that your interaction with us is no longer secure (for example, if you feel that the security of your account has been compromised), please contact us immediately
<br>
If you are a Vouri Inc Pay User, we retain your Personal Data as long as we are providing the Services to you. We retain Personal Data after we cease providing Services to you, even if you close your Vouri Inc Pay account, to the extent necessary to comply with our legal and regulatory obligations, and for the purpose of fraud monitoring, detection and prevention. We also retain Personal Data to comply with our tax, accounting, and financial reporting obligations, where we are required to retain the data by our contractual commitments to our financial partners, and where data retention is mandated by the payment methods that we support. Where we retain data, we do so in accordance with any limitation periods and records retention obligations that are imposed by applicable law.";
$lang['privacy_5'] = "International Data transactions";
$lang['privacy_5_desc'] = "We are a global business. Personal Data may be stored and processed in any country where we have operations or where we engage service providers. We may transfer Personal Data that we maintain about you to recipients in countries other than the country in which the Personal Data was originally collected, including to the United States. Those countries may have data protection rules that are different from those of your country. However, we will take measures to ensure that any such transfers comply with applicable data protection laws and that your Personal Data remains protected to the standards described in this Privacy Policy. In certain circumstances, courts, law enforcement agencies, regulatory agencies or security authorities in those other countries may be entitled to access your Personal Data.
<br>
If you are located in the European Economic Area ('EEA') or Switzerland, we comply with applicable laws to provide an adequate level of data protection for the transfer of your Personal Data to the US. Vouri Inc Pay Inc. is certified under the EU-U.S. and the Swiss-U.S. Privacy Shield Framework and adheres to the Privacy Shield Principles.
<br>
Where applicable law requires us to ensure that an international data transfer is governed by a data transfer mechanism, we use one or more of the following mechanisms: EU Standard Contractual Clauses with a data recipient outside the EEA, verification that the recipient has implemented Binding Corporate Rules, or verification that the recipient adheres to the EU-US and Swiss-US Privacy Shield Framework";
$lang['privacy_6'] = "Service Age limit";
$lang['privacy_6_desc'] = "The Services are not directed to individuals under the age of fifteen (15), and we request that they not provide Personal Data through the Services. However, additional verification information may be required from time to time to ensure the integrity of the user.";
$lang['privacy_7'] = "Update criteria";
$lang['privacy_7_desc'] = "We may change this Privacy Policy from time to time to reflect new services, changes in our Personal Data practices or relevant laws. The 'Last updated' legend at the top of this Privacy Policy indicates when this Privacy Policy was last revised. Any changes are effective when we post the revised Privacy Policy on the Services. We may provide you with disclosures and alerts regarding the Privacy Policy or Personal Data collected by posting them on our website and, if you are a User, by contacting you through your Vouri Inc Pay Dashboard, email address and/or the physical address listed in your Vouri Inc Pay account";
$lang['privacy_8'] = "Money Laundering";
$lang['privacy_8_desc'] = "In order to keep every user free from money laundering, we keenly monitor every account activity in real-time
                                detecting and flagging unusual activities. Accounts found wanting may be suspended or closed without notification
                                or account of what happened.";
$lang['terms_title'] = "Terms and conditions of use!";
$lang['terms_desc'] = "These terms are laid down to guide your usage of this service and avoid running into problems with financial institutions that help us provide this amazing service.";
$lang['terms_1'] = "We will always try to make sure this service is available for you whenever you need it under all conditions herein set as to what extent your usage of
                                the service goes. The service is not a tool to be used for abuse or bullying online or extortion of any form. Any such acts reported shall be severely considered on the perpetrator.
                                We seek to deliver a service free from malicious forms of transactions and we call upon your kind intentions to help us achieve that.
                                We are however, not liable for any damage from the use of this service or social disorder or unrest brought about by unidentified users of this service.";
$lang['terms_2'] = "Every user account is assumed to have been created after accepting these terms and so are subject to its specifications.
                                There'll be virtually no transaction limits for accounts that have confirmed verifications and are in order with our registry system.
                                User's whose accounts have not been completely verified will have certain limits imposed and will regularly be prompted to verify their accounts.
                                Users can opt-in to receive our newsletters and opt-out whenever they want. If however, one still keeps receiving emails even after he/she decides
                                to opt-out, then we shall be available to resolve this within a 12 hour period.";
$lang['terms_3'] = "All verified accounts have a maximum transaction limit of $5000 weekly. Exceeding this amount may not be possible due to financial restrictions amongst regions. User accounts registered in
countries with conflicts have a lower transaction limit of $5000 monthly. All changes to user account limits are notifies the user as they come close to being in effect. Users are actually via their dashboard or via email if enabled.";

$lang['fees'] = "Frais de Transaction";
$lang['fee_desc'] = "The average sending fee on the global market is about <b><em>$8.5</em></b> for every $200.
                                <br/>
                                We however charge a percentage, <b><em style='height: 22px;'>5%</em></b> of the sending amount plus a fixed fee (USD 0.5) for currency conversion if applicable.
                                Users who send payment without registering via the platform are charged an additional fee (<b>0.5%</b> of the sending amount) and can decide
                                who bares the charges, whether the recipient or the sender.
                                Note that the recipient's withdrawal fee from the platform sent to is dependent on that platform and we do not bare the charge for that, at least not yet.
                                Again we are working to be as cost effective as humanly possible.
                                In order to understand the way this works, let's consider the example below.<br>
                                <b>User A</b> sends USD 100 to <b>User B</b> using a credit card to the mobile money wallet of <b>User B</b> <br>
                                <b>User A</b> will be charged USD 105.5 (5% + USD 0.5). <b>User B</b> Will receive 57250 XAF (100 USD x 572.5 from exchange rate) in mobile money account <br>
";
$lang['processing'] = "Delais";
$lang['processing_desc'] = "Tout les transactions mois de $500 sont trait&eacute;s casi instantanement.
                                Les transaction au dessus de $500 peuvent prendre plus de 5 minutes a valider ou jusqu'a 48 heures apr&egrave;s quoi vous serez appeler &agrave; a soit
                                annuler la transaction, soit etre rembours&eacute. Ceci est d'extr&egrave;me importance dans notre qu&egrave;te de prevenir la fraude financi&egrave;re sur la plateforme et toujours
                                pouvoir delivr&eacute; une experience fiable, securis&eacute; et rapide pour tout nos utilisateurs.";
$lang['refund'] = "Politique de Remboursement";
$lang['refund_desc'] = "Tout les remboursement seront faites au plus tard 48 heure apr&egrave;s la dite transaction dans la devise qu'elle &agrave; &eacute;t&eacute; faite.
                                Aucun frais ne serait appliqu&eacute; a celle-ci durant le transfert et tout les frais de service seront rembours&eacute;.
                                Nous encourageons nos utilisateurs &agrave; v&eacute;rifier leur transactions regulierement si aucune notification n'est envoy&eacute; pour qu'elle soit r&eacute;ussi ou &eacute;chouer.";
$lang['limits'] = "Les limites des transactions";
$lang['limits_desc'] = "Une limite s'impose de ($5000) comme montant de transfert pouvant etre envoyer par un compte verifi&eacute; par semaine. Une limite de <b>$2000</b> par transaction s'impose sur d'autres comptes non v&eacute;rifier.
                            Il est conseiller au utilisateurs de v&eacute;rifier leur carte avant de proceder au transactions sur cette plateform.";
$lang['fees_title'] = "Les frais et delai de transaction";
$lang['fees_desc'] = "Pour assurer une utilisation agr&eacute;able du service en toute transparence, nos frais sont clairement souligner ci dessous et nous esperons
qu'elles soit bien comprehensible.";

//new strings
$lang['payment_request_string'] = 'Envoyer une demande de paiement et recevez rapidement votre paiement.';
$lang['payment_requests'] = 'Demandes de paiement.';
$lang['working'] = 'EN COURS';
$lang['get_api_key'] = 'G&eacute;n&eacute;rer une cl&eacute; API.';
$lang['sending_header'] = 'Renseignez l\'info du destinataire';
$lang['amount_requested'] = 'Montant souhaiter.';
$lang['transfer_limit'] = 'Limite de Transfert';
$lang['receiver_name'] = 'Nom du b&eacute;n&eacute;ficiare';
$lang['email'] = 'Email.';
$lang['message_title'] = 'Laisser un message au destinataire?';
$lang['back'] = 'Rentrer.';
$lang['address'] = 'Adresse';
$lang['phone_number'] = 'Num&eacute;ro de t&eacute;l&eacute;phone';
$lang['name_pseudo'] = 'Votre nom';
$lang['city'] = 'Ville';
$lang['income'] = 'Revenu';
$lang['income_title'] = 'Revenu annuelle';
$lang['country'] = 'Pays';
$lang['occupation'] = 'Occupation/Emploi';
$lang['occupation_title'] = 'Quel est Votre emploie pr&eacute;sent?';
$lang['id_document'] = 'T&eacute;l&eacute;charger un document d\'identification (e.g. Passport, CNI, carte de residence)';
$lang['get_subscribe'] = 'Boutton d\'achat.!';
$lang['charge_amount_note'] = 'Vous serez facturer  ';
$lang['sending_source'] = 'Envoyer via?';
$lang['copy_line_1'] = 'Copiez les lignes suivants pour generer votre bouttons de paiement.';
$lang['copy_line_2'] = 'Mettez le code de style et mis en forme dans votre ent&ecirc;te, en haut de votre page.';
$lang['copy_line_3'] = 'Mettez le code Javascript en bas de la page avant la balise de fermeture de body.';
$lang['sub_api_desc'] = 'Vous pouvez utiliser ce boutton sur vos pages de paiement. Remplacez les parametres comme indiquer dans le guide.';
$lang['subscription_api'] = 'Boutton Paiement';
$lang['subscription_api_desc'] = 'Vous ne savez pas programmer ou n\'avez pas assez de temps pour deployer votre service? Nous sommes la pour vous. Avec juste quelque lignes, customisez ces lignes et nous ferons tout le reste.';
$lang['programmatic_api'] = 'API de programmation.';
$lang['programmatic_api_desc'] = 'Vous avez besoin de plus de controle sur votre experience de paiement? Choisissez parmis les API que nous mettons a disposition pour cr&eacute;ez des experiences encore plus accessible pour vos clients.';
$lang['insufficient_compliance'] = 'D&eacute;sol&eacute;! Vous avez depasser votre taux de transaction mensuel selons votre niveau de conformit&eacute;. Pour augmenter votre taux de transfert mensuel, veuillez remplir votre profil en t&eacute;l&eacute;chargant les documents necessaire vous identifiants davantage.';
$lang['send_limit_exceeded'] = 'D&eacute;sol&eacute;! vous ne pouvez pas transferer plus de $1000 ou equivalent par transaction. Veuillez essayer encore.';
$lang['transaction_error'] = 'D&eacute;sol&eacute;!, une erreur s\'est produite avec votre transaction. Veuillez re-essayer s\'il vous plait ou contactez nous sur les r&egrave;seaux sociaux.';
$lang['transaction_processing'] = 'Merci de votre confiance et d\'avoir fait la transaction avec nous. Votre transaction est maintenant en cours et s\'achevera dans un instant.';
$lang['programming_api'] = 'REST API';
$lang['programming_api_usage'] = 'Usage';
$lang['programming_api_post_url'] = 'POST Url';
$lang['programming_api_description'] = 'Faites une requ&ecirc;te POST avec les parametres ci dessous.';
$lang['programming_api_response'] = 'Le resultat est sous format json avec les parametres ci dessous.';
$lang['response_parameter'] = 'Parametres de r&eacute;ponse';
$lang['response_description'] = 'D&egrave;scription';