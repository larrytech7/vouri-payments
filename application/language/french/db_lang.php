<?php
/**
 * System messages translation for CodeIgniter(tm)
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014-2018, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['db_invalid_connection_str']      = "Impossible de d�terminer les param�tres d'acc�s � la base de donn�es en se basant sur la cha�ne de connexion que vous avez fournie.";
$lang['db_unable_to_connect']           = "Impossible de se connecter � la base de donn�es en utilisant les param�tres fournis.";
$lang['db_unable_to_select']            = "Impossible de s�lectionner cette base de donn�es : %s";
$lang['db_unable_to_create']            = "Impossible de cr�er cette base de donn�es : %s";
$lang['db_invalid_query']               = "La requ�te que vous avez fournie est invalide.";
$lang['db_must_set_table']              = "Vous devez sp�cifier une table pour effectuer votre requ�te.";
$lang['db_must_use_set']                = "Vous devez utiliser la m�thode \"set()\" pour mettre � jour une entr�e.";
$lang['db_must_use_index']              = "Vous devez sp�cifier un index pour les mises � jour group�es.";
$lang['db_batch_missing_index']         = "Une ou plusieurs rang�es de la mise � jour group�e ne disposent pas de l'index requis.";
$lang['db_must_use_where']              = "Il faut obligatoirement sp�cifier la clause \"WHERE\" pour mettre � jour une entr�e.";
$lang['db_del_must_use_where']          = "Il faut obligatoirement sp�cifier la clause \"WHERE\" pour supprimer une entr�e.";
$lang['db_field_param_missing']         = "La m�thode \"fetch_fields()\" requiert le nom de la table cible en param�tre.";
$lang['db_unsupported_function']        = "Cette fonctionnalit� n'est pas disponible pour la base de donn�es utilis�e.";
$lang['db_transaction_failure']         = "Erreur de transaction : la transaction est annul�e.";
$lang['db_unable_to_drop']              = "Impossible d'effacer la base de donn�es sp�cifi�e.";
$lang['db_unsupported_feature']         = "Cette fonctionnalit� n'est pas support�e par le syst�me de gestion de bases de donn�es utilis�.";
$lang['db_unsupported_compression']     = "Le format de compression choisi n'est pas support� par votre serveur.";
$lang['db_filepath_error']              = "Impossible d'�crire des donn�es au chemin de fichiers indiqu�.";
$lang['db_invalid_cache_path']          = "Le chemin de mise en cache indiqu� n'est pas valide ou inscriptible.";
$lang['db_table_name_required']         = "Un nom de table est requis pour cette op�ration.";
$lang['db_column_name_required']        = "Un nom de champ est requis pour cette op�ration.";
$lang['db_column_definition_required']  = "Une d�finition de champ est requise pour cette op�ration.";
$lang['db_unable_to_set_charset']       = "Impossible de d�finir le jeu de caract�res de la connexion client : %s";
$lang['db_error_heading']               = "Une erreur de la base de donn�es s'est produite.";