<?php
/**
 * Created by PhpStorm.
 * User: Vanessa
 * Date: 1/21/2019
 * Time: 9:07 PM
 */
$lang['app_name'] = "VOURI PAYMENTS";
$lang['app_short_description'] = "Global payment and money transfer aggregator. MTO";
$lang['app_long_desc'] = "We are the world's leading payment aggregator powering cross provider payments across Africa. Our mission is to democratize access to financial services across Africa. It is not only a thing for politicians but something technology has a role to play.";
$lang['menu_dashboard'] = "Dashboard";
$lang['home_page_title_one'] = "One click payment aggregator";
$lang['home_page_title_two'] = "An all-in-one payment experience";
$lang['home_page_title_one_desc'] = "Effortlessly send and receive money anywhere.";
$lang['home_page_title_two_desc'] = "A single elegant interface helping you";
$lang['home_page_type_1'] = "Frictionless, Fast and secure transactions to and from any mobile wallet.";
$lang['home_page_type_2'] = "Integrate multiple payment gateways in your apps via one single API.";
$lang['send_money'] = "Send";
$lang['get_money'] = "Receive";
$lang['service_fees'] = "Service Fees";
$lang['payment_dialog_title'] = "Send money to anyone via the payment method of your choice.";
$lang['payment_dialog_amount'] = "Amount to Send";
$lang['payment_dialog_currency'] = "Choose currency";
$lang['payment_dialog_gateway'] = "Payment Method to use";
$lang['payment_dialog_go'] = "Continue";
$lang['verify_card'] = "Verify Card";
$lang['card_button_label'] = "Send";
$lang['card_button_label_pay'] = "Pay (x100)";
$lang['followus'] = "Follow us on";
$lang['followus_text'] = "";
$lang['verify_button_label'] = "VERIFY NOW!";
$lang['continue'] = "Continue";
$lang['network_error'] = "A network error has occurred. Please ensure you are properly connected to a working network.";
$lang['card_error'] = "Sorry, Your card can't be accepted on Vouri at the moment. Please try a different card.";
$lang['card_success'] = "Congratulations! Your card is compatible and can be used on Vouri.";
$lang['verify_card_note'] = 'Verify your card below. You\'ll be charged 1CHF and reimbursed once your card is confirmed';
$lang['login_heading'] = "Login to send or receive funds securely";

$lang['password_reset_heading'] = "Change your password here";
$lang['lost_password'] = "Reset forgotten password";
$lang['recover'] = "Recover";
$lang['reset'] = "Reset";
$lang['reset_password'] = "Recover your account password";
$lang['password_confirm'] = "Confirm your password";
$lang['new_password'] = "New password";

$lang['login_transact'] = "Login to continue";
$lang['login'] = "Login";
$lang['logout'] = "Logout";
$lang['options'] = "Actions";
$lang['delete_account_message'] = "Your account was completely deleted";
$lang['delete_account'] = "Delete My Account";
$lang['delete_account_note'] = "Deleting your account will also automatically delete your transaction data and any associated information with it. Are you sure to proceed?";
$lang['download_mydata'] = "Download My Transactions";
$lang['message'] = "Message";
$lang['vouri_exchange'] = "Vouri Exchange rates";
$lang['source_title'] = "Lastly updated,  August 5th, 2019";
$lang['exchange_text'] = "Our exchange rates are based on data from xe.com and are periodically updated to ensure smooth operation and maximum value for our users.";
$lang['dialog_contact'] = "Drop us a message if you can't use our chat or you would like to follow up later eventually.";
$lang['signup'] = "Signup";
$lang['signup_note'] = "By signing up, you AGREE to our ";
$lang['contact'] = "Get In Touch";
$lang['subject'] = "Subject";
$lang['contact_success'] = "Sent! We will get back to you soon";
$lang['contact_error'] = "Your message wasn't sent successfully. Please try again";
$lang['rights_reserved'] = "All rights reserved";
$lang['keys'] = "Key";
$lang['transaction_history'] = "Transaction History";
$lang['transaction_to'] = "Sent To";
$lang['transaction_from'] = "FROM";
$lang['transaction_to_'] = "TO";
$lang['transaction_charged'] = "Charged";
$lang['transaction_received'] = "Received";
$lang['transaction_date'] = "Date";
$lang['transaction_status'] = "Status";
$lang['function_title'] = "What you can do";
$lang['f1'] = "Send money.";
$lang['f1t'] = "Send money from any online wallet to any other at competitive fees and across several countries fast and simple.";
$lang['f2'] = "Receive money";
$lang['f2t'] = "Request to receive money from a user on any payment or money transfer platform.";
$lang['f3'] = "Integrate the API";
$lang['f3t'] = "Seamlessly integrate our APIs in your mobile or web applications and collect payments.";
$lang['f4'] = "Leverage a single API and support multiple payment options";
$lang['f4t'] = "We enable users send and receive payments from different mobile and web payment providers like mobile money and paypal.<br/>
                        We provide a single interface to help e-commerce sites and apps accept payments from several different providers from a single integration point.";
$lang['f5'] = "App/Website Integrations";
$lang['f6'] = "Successful Transactions";
$lang['paid'] = "PAID";
$lang['failed'] = "FAILED";
$lang['pending'] = "PENDING";
$lang['send'] = "Send";
$lang['receive'] = "Receive";
$lang['exit_page_message'] = "Hey, please WAIT!! Please tell us how we may help you better with your transactions";
$lang['email'] = "Email";
$lang['email_text'] = "Enter your valid email";
$lang['name'] = "Name";
$lang['name_text'] = "Enter your name";
$lang['phone'] = "Telephone Number";
$lang['phone_text'] = "Phone number(without country code)";
$lang['password'] = "Password";
$lang['password_text'] = "Your password (more than 8 characters)";
$lang['password_re'] = "Re-Password";
$lang['password_re_text'] = "Confirm your password";
$lang['no_account_text'] = "Do not have an account? ";
$lang['create_account_text'] = "Create your account free!";
$lang['success'] = "Success!";
$lang['error'] = "Error!";

$lang['payment_instruments'] = "Your payment instruments(methods)";
$lang['instrument_name'] = "Name/email";
$lang['instrument_type'] = "Type";
$lang['instrument_number'] = "Number";
$lang['instrument_expiration'] = "Expiry Date";
$lang['instrument_origin'] = "Origin";
$lang['instrument_fingerprint'] = "Fingerprint";
$lang['remove_instrument'] = "Remove this instrument";

$lang['how'] = "How does this work?";
$lang['how_desc'] = "In at most <b>3</b> steps, complete your transaction with Vouri";
$lang['step1'] = "Enter the amount you want to send. Select the currency you want to be debited in, and select your preferred method of payment";
$lang['step2'] = "Select how you want your receiver to receive the money then enter the receiver's personal details";
$lang['step3'] = "If you are not logged-in, login and validate the payment via the payment method selected.";
$lang['sender_info'] = 'Sending Details';
$lang['receiver_info'] = 'Receiving Details';
$lang['make_payment'] = 'Validating payment Transaction';
$lang['simplicity'] = "";

$lang['features'] = "Main features";
$lang['leave_message'] = "Drop us a message to express your interest in our API. It wil work just like the system currently does";
$lang['tryit'] = "Try it!";
$lang['learn_more'] = "Learn more";
$lang['translate_not_available'] = "Sorry, translation is not available for this page";
$lang['usechat'] = "Use the live chat";
$lang['joinus'] = "Want to be part of our team?";
$lang['joinus_desc'] = "We look out for great talent passionate about advancing our vision for a better and more organised payment infrastructure.
                        If you are passionate and talented to join our team of dreamers and guardians of the galaxy, drop us a note.";
$lang['feature_one_title'] = "Cross provider payment and money Transfer";
$lang['feature_one_desc'] = "Send money to and from several different supported money transfer services online. You can send money from a VISA/MASTERCARD to a mobile money account and back.";
$lang['feature_two_title'] = "Integrate a single API for all payments";
$lang['feature_two_desc'] = "With a single point of integration, accept payment from different payment providers. Increase the range of people who can pay on your web or mobile application using a single set of codes.";
$lang['about_title'] = "We are Vouri!";
$lang['about_desc'] = "Our core mission is to restore order in the chaotic world of online and mobile payments
                        by providing seamless interoperability between the most popular online and mobile payment gateways.";
$lang['about_tag'] = "Leverage simplicity and focus on building value for your users.";
$lang['api_title'] = "Meet our Amazing API";
$lang['api_features'] = "API features";
$lang['mobile_sdk'] = "Mobile SDK";
$lang['pause_note'] = "Under Construction.";
$lang['mobile_sdk_1'] = "Login and Create your API key on your dashboard.";
$lang['mobile_sdk_2'] = "Add the Library to your dependency file.";
$lang['mobile_sdk_3'] = "Add your key to the Constructor parameter";
$lang['mobile_sdk_4'] = "Test with these sample credentials";
$lang['web_api'] = "Web API";
$lang['web_api_1'] = "Locate your API key and applicationID";
$lang['web_api_2'] = "Add your domain to the Oauth2 domain list";
$lang['web_api_3'] = "Import the Javascript library.";
$lang['web_api_4'] = "Test with sample request";
$lang['api_desc'] = "Seamlessly add our intuitive and powerful APIs to your web or mobile applications in seconds. Go live almost instantly after that.";
$lang['api_theme'] = "Designed for humans, by humans and made available to the machines.";
$lang['api_note'] = "While most parts of our API infrastructure is currently under construction, we encourage you to signup to our <?= anchor('signup', 'NewsLetter')?> and be the first to
        see what we have cooked for you. However, we plan for a seamless integration in the following steps below.";
$lang['privacy_extended'] = "Before reading the rest of the privacy terms applicable to the use of this service, first read and make sure you agree with the privacy terms ";
$lang['privacy_title'] = "Our Privacy Policy";
$lang['privacy_desc'] = "Our number one concern is the privacy of user accounts, user transactions and data without which we won't be able to provide
                        the seamless and hassle free service that we do. We have designed an iron clad policy that works for everyone.";
$lang['privacy_preamble'] = "Preamble";
$lang['privacy_preamble_desc'] = "Vouri Inc Pay obtains Personal Data about you from various sources to provide our Services and to manage our Sites.
'You' may be a visitor to one of our websites, a user of one or more of our Services ('User' or 'Vouri Inc Pay User'), or a customer of a User ('Customer').
If you are a Customer, Vouri Inc Pay will generally not collect your Personal Data directly from you. Your agreement with the relevant Vouri Inc Pay User should explain how Vouri Inc Pay User shares your Personal Data with Vouri Inc Pay,
and if you have questions about this sharing, then you should direct those questions to the Vouri Inc Pay User.
Every data you generate directly on this platform while using its services belong to you and you alone.
                                You can at any time request that your data be completely cleaned and all transactions archived whenever you wish to
                                close your account.";
$lang['privacy_1'] = "Personal Data";
$lang['privacy_1_desc'] = "Personal Data is any information that relates to an identified or identifiable individual. The Personal Data that you provide directly to us through our Sites will be apparent from the context in which you provide
the data. In particular: When you register for a Vouri Inc Pay account we collect your full name, email address, and account log-in credentials.
When you fill-in our online form to contact our sales team, we collect your full name, work email, country. <br/>
When you respond to Vouri Inc Pay emails or surveys we collect your email address, name and any other information you choose to include in the body of your email or responses. If you contact us by phone, we will collect the phone number you use to call Vouri Inc Pay. If you contact us by phone as a Vouri Inc Pay User, we may collect additional information in order to verify your identity.
<br/>
When you respond to Vouri Inc Pay emails or surveys we collect your email address, name and any other information you choose to include in the body of your email or responses. If you contact us by phone, we will collect the phone number you use to call Vouri Inc Pay. If you contact us by phone as a Vouri Inc Pay User, we may collect additional information in order to verify your identity.
<br/>
When we conduct fraud monitoring, prevention and detection activities, we may also receive Personal Data about you from our business partners, financial service providers, identity verification services, and publicly available sources (e.g., name, address, phone number, country), as necessary to confirm your identity and prevent fraud. Our fraud monitoring, detection and prevention services may use technology that helps us assess the risk associated with an attempted transaction that is enabled on the Vouri Inc Pay User�s website or the application that collects information.
<br>
Your account can only be accessed via an email/phone and a password that we advise you to change regularly in order to
                                avoid being victim of any incidental hack. We however take on the responsibility of making sure that access to your
                                account is as secure as possible and only you can have access at any given time. Your account information is never shared with
                                any third party service unless for procedures that have to deal with money laundering and only after prompt notification about this
                                to you via email and/or other means available.
                                Your account activity is monitored and logged in order to detect suspicious activity or access from different regions.";
$lang['privacy_2'] = "Using Personal Data";
$lang['privacy_2_desc'] = "We rely upon a number of legal grounds to ensure that our use of your Personal Data is compliant with applicable law. We use Personal Data to facilitate the business relationships we have with our Users, to comply with our financial regulatory and other legal obligations, and to pursue our legitimate business interests. We also use Personal Data to complete payment transactions and to provide payment-related services to our Users<br>
We rely upon a number of legal grounds to ensure that our use of your Personal Data is compliant with applicable law. We use Personal Data to facilitate the business relationships we have with our Users, to comply with our financial regulatory and other legal obligations, and to pursue our legitimate business interests. We also use Personal Data to complete payment transactions and to provide payment-related services to our Users
<br>
We rely upon a number of legal grounds to ensure that our use of your Personal Data is compliant with applicable law. We use Personal Data to facilitate the business relationships we have with our Users, to comply with our financial regulatory and other legal obligations, and to pursue our legitimate business interests. We also use Personal Data to complete payment transactions and to provide payment-related services to our Users
<br>
We rely upon a number of legal grounds to ensure that our use of your Personal Data is compliant with applicable law. We use Personal Data to facilitate the business relationships we have with our Users, to comply with our financial regulatory and other legal obligations, and to pursue our legitimate business interests. We also use Personal Data to complete payment transactions and to provide payment-related services to our Users";
$lang['privacy_3'] = "Personal Data disclosure";
$lang['privacy_3_desc'] = "Vouri Inc Pay does not sell or rent Personal Data to marketers or unaffiliated third parties. We share your Personal Data with trusted entities, as outlined below <br>
We share Personal Data with other Vouri Inc Pay entities in order to provide our Services and for internal administration purposes
<br>
We share Personal Data with a limited number of our service providers. We have service providers that provide services on our behalf, such as identity verification services, website hosting, data analysis, information technology and related infrastructure, customer service, email delivery, and auditing services. These service providers may need to access Personal Data to perform their services. We authorize such service providers to use or disclose the Personal Data only as necessary to perform services on our behalf or comply with legal requirements. We require such service providers to contractually commit to protect the security and confidentiality of Personal Data they process on our behalf. Our service providers are predominantly located in the European Union and the United States of America, and Africa
<br>
In the event that we enter into, or intend to enter into, a transaction that alters the structure of our business, such as a reorganization, merger, sale, joint venture, assignment, transfer, change of control, or other disposition of all or any portion of our business, assets or stock, we may share Personal Data with third parties for the purpose of facilitating and completing the transaction
<br>
We share Personal Data as we believe necessary: (i) to comply with applicable law, or payment method rules; (ii) to enforce our contractual rights; (iii) to protect the rights, privacy, safety and property of Vouri Inc Pay, you or others; and (iv) to respond to requests from courts, law enforcement agencies, regulatory agencies, and other public and government authorities, which may include authorities outside your country of residence";
$lang['privacy_4'] = "Security and Retention";
$lang['privacy_4_desc'] = "We make reasonable efforts to ensure a level of security appropriate to the risk associated with the processing of Personal Data. We maintain organizational, technical and administrative measures designed to protect Personal Data within our organization against unauthorized access, destruction, loss, alteration or misuse. Your Personal Data is only accessible to a limited number of personnel who need access to the information to perform their duties. Unfortunately, no data transmission or storage system can be guaranteed to be 100% secure. If you have reason to believe that your interaction with us is no longer secure (for example, if you feel that the security of your account has been compromised), please contact us immediately
<br>
If you are a Vouri Inc Pay User, we retain your Personal Data as long as we are providing the Services to you. We retain Personal Data after we cease providing Services to you, even if you close your Vouri Inc Pay account, to the extent necessary to comply with our legal and regulatory obligations, and for the purpose of fraud monitoring, detection and prevention. We also retain Personal Data to comply with our tax, accounting, and financial reporting obligations, where we are required to retain the data by our contractual commitments to our financial partners, and where data retention is mandated by the payment methods that we support. Where we retain data, we do so in accordance with any limitation periods and records retention obligations that are imposed by applicable law.";
$lang['privacy_5'] = "International Data transactions";
$lang['privacy_5_desc'] = "We are a global business. Personal Data may be stored and processed in any country where we have operations or where we engage service providers. We may transfer Personal Data that we maintain about you to recipients in countries other than the country in which the Personal Data was originally collected, including to the United States. Those countries may have data protection rules that are different from those of your country. However, we will take measures to ensure that any such transfers comply with applicable data protection laws and that your Personal Data remains protected to the standards described in this Privacy Policy. In certain circumstances, courts, law enforcement agencies, regulatory agencies or security authorities in those other countries may be entitled to access your Personal Data.
<br>
If you are located in the European Economic Area ('EEA') or Switzerland, we comply with applicable laws to provide an adequate level of data protection for the transfer of your Personal Data to the US. Vouri Inc Pay Inc. is certified under the EU-U.S. and the Swiss-U.S. Privacy Shield Framework and adheres to the Privacy Shield Principles.
<br>
Where applicable law requires us to ensure that an international data transfer is governed by a data transfer mechanism, we use one or more of the following mechanisms: EU Standard Contractual Clauses with a data recipient outside the EEA, verification that the recipient has implemented Binding Corporate Rules, or verification that the recipient adheres to the EU-US and Swiss-US Privacy Shield Framework";
$lang['privacy_6'] = "Service Age limit";
$lang['privacy_6_desc'] = "The Services are not directed to individuals under the age of fifteen (15), and we request that they not provide Personal Data through the Services. However, additional verification information may be required from time to time to ensure the integrity of the user.";
$lang['privacy_7'] = "Update criteria";
$lang['privacy_7_desc'] = "We may change this Privacy Policy from time to time to reflect new services, changes in our Personal Data practices or relevant laws. The 'Last updated' legend at the top of this Privacy Policy indicates when this Privacy Policy was last revised. Any changes are effective when we post the revised Privacy Policy on the Services. We may provide you with disclosures and alerts regarding the Privacy Policy or Personal Data collected by posting them on our website and, if you are a User, by contacting you through your Vouri Inc Pay Dashboard, email address and/or the physical address listed in your Vouri Inc Pay account";
$lang['privacy_8'] = "Money Laundering";
$lang['privacy_8_desc'] = "In order to keep every user free from money laundering, we keenly monitor every account activity in real-time
                                detecting and flagging unusual activities. Accounts found wanting may be suspended or closed without notification
                                or account of what happened.";
$lang['terms_title'] = "Terms and conditions of use!";
$lang['terms_desc'] = "These terms are laid down to guide your usage of this service and avoid running into problems with financial institutions that help us provide this amazing service.";
$lang['terms_1'] = "We will always try to make sure this service is available for you whenever you need it under all conditions herein set as to what extent your usage of
                                the service goes. The service is not a tool to be used for abuse or bullying online or extortion of any form. Any such acts reported shall be severely considered on the perpetrator.
                                We seek to deliver a service free from malicious forms of transactions and we call upon your kind intentions to help us achieve that.
                                We are however, not liable for any damage from the use of this service or social disorder or unrest brought about by unidentified users of this service.";
$lang['terms_2'] = "Every user account is assumed to have been created after accepting these terms and so are subject to its specifications.
                                There'll be virtually no transaction limits for accounts that have confirmed verifications and are in order with our registry system.
                                User's whose accounts have not been completely verified will have certain limits imposed and will regularly be prompted to verify their accounts.
                                Users can opt-in to receive our newsletters and opt-out whenever they want. If however, one still keeps receiving emails even after he/she decides
                                to opt-out, then we shall be available to resolve this within a 12 hour period.";
$lang['terms_3'] = "All verified accounts have a maximum transaction limit of $5000 weekly. Exceeding this amount may not be possible due to financial restrictions amongst regions. User accounts registered in
countries with conflicts have a lower transaction limit of $5000 monthly. All changes to user account limits are notifies the user as they come close to being in effect. Users are actually via their dashboard or via email if enabled.";

$lang['fees'] = "Transaction Fees";
$lang['fee_desc'] = "The average sending fee on the global market is about <b><em>$8.5</em></b> for every $200.
                                <br/>
                                We however charge a percentage, <b><em style='height: 22px;'>5%</em></b> of the sending amount plus a fixed fee (USD 0.5) for currency conversion if applicable.
                                Users who send payment without registering via the platform are charged an additional fee (<b>0.5%</b> of the sending amount) and can decide
                                who bares the charges, whether the recipient or the sender.
                                Note that the recipient's withdrawal fee from the platform sent to is dependent on that platform and we do not bare the charge for that, at least not yet.
                                Again we are working to be as cost effective as humanly possible.
                                In order to understand the way this works, let's consider the example below.<br>
                                <b>User A</b> sends USD 100 to <b>User B</b> using a credit card to the mobile money wallet of <b>User B</b> <br>
                                <b>User A</b> will be charged USD 105.5 (5% + USD 0.5). <b>User B</b> Will receive 57250 XAF (100 USD x 572.5 from exchange rate) in mobile money account <br>
";
$lang['processing'] = "Processing time";
$lang['processing_desc'] = "All transactions under $500 are done almost instantly.
                                Transactions exceeding $500 can take more time from 5 minutes and up to 48 Hours, after which you will be requested to either re-do/update the transaction or
                                be refunded. This is extremely important in our quest to prevent fraud on the platform while providing speed, reliability and security to our users.";
$lang['refund'] = "Refund Policy";
$lang['refund_desc'] = "All refunds shall be confirmed no more than 48 hours after the transaction in the currency originally charged.
                                No fees shall apply during the process and all service fees applied during the transaction are also refunded.
                                We often encourage users to often check that their transaction has completed on the same day to avoid delays in their refunds.";
$lang['limits'] = "Transaction Limits";
$lang['limits_desc'] = "There's a limit ($5000) as to how much can be sent by a verified account per week and there's a <b>$2000</b> limit per transaction.
                                However, there's a limit of <b>$2000</b> for every non-verified account.
                            User's who pay via unregistered channel are imposed a transaction limit of $5000 which is beyond our control and we would love to offer
                                better flexibility and we are constantly working on it.";
$lang['fees_title'] = "Fees and Transaction times";
$lang['fees_desc'] = "In order to ensure a seamless use of our services and in all transparency our fess are clearly outlined below and we hope they are
                        fully comprehensible";
//recent translations
$lang['security'] = 'Security and Integrity';
$lang['security_note'] = 'We secure your transactions through our end-to-end encrypted network.';
$lang['speed'] = 'Speed and Performance';
$lang['speed_note'] = 'We ensure funds reach their destination as fast as possible beyond industry standards.';
$lang['reliable'] = 'Accessibility and reliability';
$lang['reliable_note'] = 'We offer multiple outlets to let you cash in or out wherever you are at anytime.';
$lang['africa'] = 'Works well for Africa.';
$lang['africa_note'] = 'Finally a true online money system that works well for Africans without any friction.';
$lang['development'] = 'Build and Deploy';
$lang['development_note'] = 'Build your own applications relying on our intuitively designed APIs and tools';
$lang['service_fees'] = 'Transaction fees';
$lang['service_fees_note'] = 'Affordable transaction fees are at the heart of our operations hence unbeatable fees.';
$lang['terms'] = 'Terms and Conditions';
$lang['privacy'] = 'Privacy';
$lang['api'] = 'Api';
$lang['about'] = 'About';
$lang['working'] = 'PROCESSING';
$lang['get_api_key'] = 'Generate your API KEY';
$lang['sending_header'] = 'Complete receiver\'s info';
$lang['payment_request_string'] = 'Send a payment request and get paid easily and fast.';
$lang['payment_requests'] = 'Payment requests.';
$lang['amount_requested'] = 'Amount to request.';
$lang['transfer_limit'] = 'Transfer Limit';
$lang['receiver_name'] = 'Beneficiary\'s Name';
$lang['email'] = 'Email.';
$lang['message_title'] = 'Would you like to leave a message for your recipient?';
$lang['back'] = 'Back.';
$lang['address'] = 'Address';
$lang['phone_number'] = 'Phone Number';
$lang['name_pseudo'] = 'Your name';
$lang['city'] = 'City';
$lang['income'] = 'Income';
$lang['income_title'] = 'Your yearly income';
$lang['country'] = 'Country';
$lang['occupation'] = 'Occupation/Job title';
$lang['occupation_title'] = 'What\'s your current job?';
$lang['id_document'] = 'Upload an Identification document (e.g. Passport, National ID, Residence permit)';
$lang['get_subscribe'] = 'Get Checkout Button now!';
$lang['charge_amount_note'] = 'You will be charged ';
$lang['sending_source'] = 'Send into?';
$lang['copy_line_1'] = 'Copy the following lines below for your subscription button.';
$lang['copy_line_2'] = 'Put the styling in your header';
$lang['copy_line_3'] = 'Put the javascript at the very bottom of your page';
$lang['sub_api_desc'] = 'This is your button you can use on any payment page. Replace the parameters as described';
$lang['subscription_api'] = 'Checkout/Subscribe API';
$lang['subscription_api_desc'] = 'Don\'t know how to code or have less time to get payment up and running? We got you covered. You can now just add a few lines of code readily customizable on your page or component and we\'ll do the heavy lifting for you.';
$lang['programmatic_api'] = 'Programmatic API/SDKs.';
$lang['programmatic_api_desc'] = 'You need more control over payment in your application? You can choose from a couple of SDKs we\'ve elegantly crafted just for you.';
$lang['insufficient_compliance'] = 'Sorry! you are have exceeded your monthly transfer rate based on your compliance rate. To increase your monthly rate, complete your profile by submitting the required documentation about you.';
$lang['send_limit_exceeded'] = 'Sorry, you cannot be able to send more than an equivalent of $1000 per transaction. Please make a change and try again';
$lang['transaction_error'] = 'Sorry, an error occurred with your transaction. Try again or contact us via our social chat channels.';
$lang['transaction_processing'] = 'Thank you for trusting us. Your transaction is now processing and should complete in a moment.';
$lang['programming_api'] = 'REST API';
$lang['programming_api_usage'] = 'Usage';
$lang['programming_api_post_url'] = 'POST Url';
$lang['programming_api_description'] = 'The following request parameters should be sent in a post request';
$lang['programming_api_response'] = 'The response is a json with the following parameters';
$lang['response_parameter'] = 'Response Parameter';
$lang['response_description'] = 'Description';
//new strings
$lang['msg_unavailable'] = 'Incoming remittances currently unavailable. We have insufficient local funds to fulfill your request. Please be patient while we attempt to top up. Thanks for your patience';
$lang['receiver_gateway'] = 'Select how you wish to receive the funds';
$lang['request_message'] = 'Leave a message. Maybe reason for your request.';
$lang['request'] = 'Send Request';