<section class="slice-sm sct-color-1">
    <div class="profile">
        <div class="container">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-3">
                    <div class="sidebar sidebar-inverse bg-base-1 sidebar--style-2 no-border stickyfill">
                        <div class="widget">
                            <!-- Profile details -->
                            <div class="profile-details mb-4">
                                <h2 class="heading heading-6 strong-600 profile-name "><?= $this->session->user->name ?></h2>
                                <h4 class="heading heading-6 strong-600 profile-name "><?= $this->session->user->email ?></h4>
                                <h4 class="heading-5 strong-700"><?= $this->lang->line('transfer_limit').' : USD '.$transfer_limit?></h4>

                            </div>

                            <hr>

                            <ul class="categories categories--style-3 mt-3">
                                <li>
                                    <a href="<?= site_url('admin') ?>" class="active">
                                        <i class="icon-finance-059"></i>
                                        <span class="category-name">
                                            Transactions
                                        </span>
                                    </a>
                                </li>
                                <?php if($this->session->user->role == 'admin') : ?>
                                    <li>
                                        <a href="<?= site_url('payrequests')?>">
                                            <i class="ion-android-sync"></i>
                                        <span class="category-name">
                                            <?= $this->lang->line('payment_requests') ?>
                                        </span>
                                        </a>
                                    </li>
                                <?php endif; ?>
                                <li>
                                    <a href="<?= site_url('notifications')?>">
                                        <i class="ion-email-unread"></i>
                                        <span class="category-name">
                                            Notifications
                                        </span>
                                    </a>
                                </li>
                                <?php if($this->session->user->role == 'admin') : ?>
                                <li class="active">
                                    <a href="<?= site_url('users')?>">
                                        <i class="ion-ios-people"></i>
                                        <span class="category-name">
                                            Users
                                        </span>
                                    </a>
                                </li>
                                <?php endif; ?>
                                <li>
                                    <a href="<?= site_url('settings')?>">
                                        <i class="ion-ios-gear"></i>
                                        <span class="category-name">
                                            Profile
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= site_url('admin/subscriptions')?>">
                                        <i class="ion-code-working"></i>
                                        <span class="category-name">
                                            <?= $this->lang->line('api') ?>
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <i class="ion-android-alert"></i>
                                    <span><?= $this->lang->line('keys') ?></span>
                                    <table class=" table-info table-responsive">
                                        <tbody>
                                        <tr>
                                            <td>VSA</td>
                                            <td>VISA CARD</td>
                                        </tr>
                                        <tr>
                                            <td>MSA</td>
                                            <td>MASTERCARD</td>
                                        </tr>
                                        <tr>
                                            <td>MMO</td>
                                            <td>MTN CAM Mobile Money</td>
                                        </tr>
                                        <tr>
                                            <td>OMO</td>
                                            <td>ORANGE MONEY</td>
                                        </tr>
                                        <tr>
                                            <td>STR</td>
                                            <td>STRIPE</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-lg-9">
                    <div class="main-content">
                        <!-- Page title -->
                        <div class="page-title">
                            <div class="row align-items-center">
                                <div class="col-lg-6 col-12">
                                    <h2 class="heading heading-5 text-capitalize strong-500 mb-0">
                                        <a href="<?= site_url('notifications')?>" class="link text-underline--none">
                                            <i class="ion-ios-arrow-back"></i> Users
                                        </a>
                                    </h2>
                                </div>
                            </div>
                        </div>

                        <hr>

                        <!-- Notifications -->
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <!-- Notifications history table -->
                                <div class="card no-border">
                                    <div>
                                        <table class="table table-sm table-striped table-hover">
                                            <thead>
                                            <tr>
                                                <th>Registered</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>Role</th>
                                                <th>KYC Level</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            <?php foreach($users as $user): ?>
                                            <tr>
                                                <td>
                                                    <?= $user->create_time ?>
                                                </td>

                                                <td>
                                                    <a href="javascript:;" data-fancybox data-src="#<?= $user->user_id?>">
                                                        <?= $user->name ?>
                                                    </a>
                                                </td>
                                                <td>
                                                    <?= $user->email ?>
                                                </td>
                                                <td>
                                                    <?= $user->phone ?>
                                                </td>
                                                <td>
                                                    <?= $user->role ?>
                                                </td>
                                                <td>
                                                    <?= $user->kyc_level ?>
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>