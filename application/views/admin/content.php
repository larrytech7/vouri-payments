<section class="slice-sm sct-color-1">
    <div class="profile">
        <div class="container" style="max-width: 90% !important;">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-3">
                    <div class="sidebar sidebar-inverse bg-base-1 sidebar--style-2 no-border stickyfill">
                        <div class="widget">

                            <!-- Profile details -->
                            <div class="profile-details mb-4">
                                <h2 class="heading heading-5 strong-600 profile-name "><?= $this->session->user->name ?></h2>
                                <h4 class="heading heading-6 strong-600 profile-name "><?= $this->session->user->email ?></h4>
                                <h4 class="heading-5 strong-700"><?= $this->lang->line('transfer_limit').' : USD '.$transfer_limit?></h4>
                            </div>
                            <hr>

                            <ul class="categories categories--style-3 mt-3">
                                <li class="active">
                                    <a href="<?= site_url('admin') ?>">
                                        <i class="icon-finance-059"></i>
                                        <span class="category-name">
                                            Transactions
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= site_url('payrequests')?>">
                                        <i class="ion-android-sync"></i>
                                        <span class="category-name">
                                            <?= $this->lang->line('payment_requests') ?>
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= site_url('notifications')?>">
                                        <i class="ion-email-unread"></i>
                                        <span class="category-name">
                                            Notifications
                                        </span>
                                    </a>
                                </li>
                                <?php if($this->session->user->role == 'admin') : ?>
                                <li>
                                    <a href="<?= site_url('users')?>">
                                        <i class="ion-ios-people"></i>
                                        <span class="category-name">
                                            Users
                                        </span>
                                    </a>
                                </li>
                                <?php endif; ?>
                                <li>
                                    <a href="<?= site_url('settings')?>">
                                        <i class="ion-gear-a"></i>
                                        <span class="category-name">
                                            Profile
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= site_url('admin/subscriptions')?>">
                                        <i class="ion-code-working"></i>
                                        <span class="category-name">
                                            <?= $this->lang->line('api') ?>
                                        </span>
                                    </a>
                                </li>
                                <hr>
                                <li>
                                    <i class="ion-android-alert"></i>
                                    <span><?= $this->lang->line('keys') ?></span>
                                    <table class="table-info table-responsive">
                                        <tbody>
                                            <tr>
                                                <td>VSA</td>
                                                <td>VISA CARD</td>
                                            </tr>
                                            <tr>
                                                <td>MSA</td>
                                                <td>MASTERCARD</td>
                                            </tr>
                                            <tr>
                                                <td>MMO</td>
                                                <td>MTN CAM Mobile Money</td>
                                            </tr>
                                            <tr>
                                                <td>OMO</td>
                                                <td>ORANGE MONEY</td>
                                            </tr>
                                            <tr>
                                                <td>STR</td>
                                                <td>STRIPE</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-lg-9">
                    <div class="main-content">
                        <!-- Page title -->
                        <div class="page-title">
                            <div class="row align-items-center">
                                <div class="col-lg-12 col-12">
                                    <h2 class="heading heading-5 text-capitalize strong-500 mb-0">
                                        <a href="#" class="link text-underline--none">
                                            <i class="ion-ios-arrow-back"></i> <?= $this->lang->line('transaction_history') ?>
                                        </a>
                                        <a href="javascript:print()" class="link text-underline--none pull-right">
                                            <i class="ion ion-ios-download-outline text-primary"></i> <?= $this->lang->line('download_mydata') ?>
                                        </a>
                                    </h2>
                                    <form id="formmomo" method="GET" action="https://developer.mtn.cm/OnlineMomoWeb/faces/transaction/transactionRequest.xhtml" target="_top">
    <input type="hidden" name="idbouton" value="2" autocomplete="off">
    <input type="hidden" name="typebouton" value="PAIE" autocomplete="off">
    <input class="momo mount" type="hidden" placeholder="" name="_amount" value="100" id="montant" autocomplete="off">
    <input class="momo host" type="hidden" placeholder="" name="_tel" value="678656032" autocomplete="off">
    <input class="momo pwd" placeholder="Please enter your password" name="_clP" value="" autocomplete="off" type="hidden">
    <input type="hidden" name="_email" value="larryakah@gmail.com" autocomplete="off">
    <input type="image" id="Button_Image" src="https://developer.mtn.cm/OnlineMomoWeb/console/uses/itg_img/buttons/MOMO_buy_now_EN.jpg" style="width : 250px; height: 100px;" border="0" name="submit" alt="OnloneMomo, le réflexe sécurité pour payer en ligne" autocomplete="off">
</form>
                                </div>
                            </div>
                        </div>
                        <?php if($this->session->flashdata('error')): ?>
                            <div class="alert alert-danger" data-dsimiss="alert">
                                <?= $this->encryption->decrypt($this->session->flashdata('error')) ?>
                                <button type="button" class="close pull-right" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php endif; ?>
                        <?php if($this->session->flashdata('info')): ?>
                            <div class="alert alert-success" data-dsimiss="alert">
                                <?= $this->encryption->decrypt($this->session->flashdata('info')) ?>
                                <button type="button" class="close pull-right" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php endif; ?>
                        <hr>

                        <!-- Transactions -->
                        <div class="row" id="section-to-print">
                            <div class="col-lg-12 col-md-12">
                                <!-- Notifications history table -->
                                <div class="card no-border" >
                                    <div>
                                        <table class="table table-sm table-striped table-hover">
                                            <thead>
                                            <tr>
                                                <th><?= $this->lang->line('transaction_date') ?></th>
                                                <th><?= $this->lang->line('transaction_to') ?></th>
                                                <th><?= $this->lang->line('transaction_from') ?></th>
                                                <th><?= $this->lang->line('transaction_to_') ?></th>
                                                <th><?= $this->lang->line('transaction_charged') ?></th>
                                                <th><?= $this->lang->line('transaction_received') ?></th>
                                                <th><?= $this->lang->line('transaction_status') ?></th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            <?php foreach($transactions as $transaction): ?>
                                            <tr class="parent">
                                                <td><?= $transaction->create_time ?></td>
                                                <td>
                                                    <a href="javascript:;" data-toggle="tooltip" data-placement="top" data-fancybox data-src="<?= '#'.$transaction->transaction_id?>">
                                                    <?= $transaction->receiver_email ?> | <?= $transaction->receiver_phone ?>
                                                    </a>
                                                </td>

                                                <td>
                                                    <?= $transaction->send_gateway ?>
                                                </td>
                                                <td>
                                                    <?= $transaction->receive_gateway ?>
                                                </td>
                                                <td>
                                                    <?= $transaction->amount ?> <?= $transaction->currency ?>
                                                </td>
                                                <td>
                                                    <?= $transaction->amount_sent.' '.$transaction->receiving_currency ?>
                                                </td>

                                                <td>
                                                    <?php if($transaction->status == 0): ?>
                                                                    <span class="badge badge--2 mr-4">
                                                                        <i class="bg-yellow"></i> <?= $this->lang->line('pending') ?>
                                                                    </span>
                                                    <?php endif;
                                                    if($transaction->status == 1):?>
                                                            <span class="badge badge--2 mr-4">
                                                                <i class="bg-primary"></i> <?= $this->lang->line('working') ?>
                                                            </span>
                                                    <?php endif; ?>
                                                    <?php if($transaction->status == 2):?>
                                                            <span class="badge badge--2 mr-4">
                                                                <i class="bg-green"></i> <?= $this->lang->line('paid') ?>
                                                            </span>
                                                    <?php endif;?>

                                                    <?php if($transaction->status == -1):?>
                                                            <span class="badge badge--2 mr-4">
                                                                <i class="bg-red"></i> <?= $this->lang->line('failed') ?>
                                                            </span>
                                                    <?php endif;?>

                                                </td>
                                                <td>
                                                    <?php if ($this->session->user->role == 'admin'): ?>
                                                        <?= anchor('admin/approve/'.$transaction->transaction_id, '<i class="ion-checkmark-circled text-success"></i>', ['class'=>'text-success'])?> &nbsp;
                                                        <?= anchor('admin/reject/'.$transaction->transaction_id, '<i class="ion-trash-b text-danger"></i>', ['class'=>'text-danger'])?>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>