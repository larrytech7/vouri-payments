<section class="slice-sm sct-color-1">
    <div class="profile">
        <div class="container">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-3">
                    <div class="sidebar sidebar-inverse bg-base-1 sidebar--style-2 no-border stickyfill">
                        <div class="widget">
                            <!-- Profile details -->
                            <div class="profile-details mb-4">
                                <h2 class="heading heading-6 strong-600 profile-name "><?= $this->session->user->name ?></h2>
                                <h4 class="heading heading-6 strong-600 profile-name "><?= $this->session->user->email ?></h4>
                                <h4 class="heading-5 strong-700"><?= $this->lang->line('transfer_limit').' : USD '.$transfer_limit?></h4>

                            </div>

                            <hr>

                            <ul class="categories categories--style-3 mt-3">
                                <li>
                                    <a href="<?= site_url('admin') ?>">
                                        <i class="icon-finance-059"></i>
                                        <span class="category-name">
                                            Transactions
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= site_url('payrequests')?>">
                                        <i class="ion-android-sync"></i>
                                        <span class="category-name">
                                            <?= $this->lang->line('payment_requests') ?>
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= site_url('notifications')?>" class="active">
                                        <i class="ion-email-unread"></i>
                                        <span class="category-name">
                                            Notifications
                                        </span>
                                    </a>
                                </li>
                                <?php if($this->session->user->role == 'admin') : ?>
                                    <li class="active">
                                        <a href="<?= site_url('users')?>">
                                            <i class="ion-ios-people"></i>
                                        <span class="category-name">
                                            Users
                                        </span>
                                        </a>
                                    </li>
                                <?php endif; ?>
                                <li>
                                    <a href="<?= site_url('settings')?>">
                                        <i class="ion-person"></i>
                                        <span class="category-name">
                                            Profile
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= site_url('admin/subscriptions')?>">
                                        <i class="ion-code-working"></i>
                                        <span class="category-name">
                                            <?= $this->lang->line('api') ?>
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <i class="ion-android-alert"></i>
                                    <span><?= $this->lang->line('keys') ?></span>
                                    <table class="table-info table-responsive">
                                        <thead>
                                        <tr>
                                            <th>Acronym</th>
                                            <th>Description</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>VSA</td>
                                            <td>VISA CARD</td>
                                        </tr>
                                        <tr>
                                            <td>MSA</td>
                                            <td>MASTERCARD</td>
                                        </tr>
                                        <tr>
                                            <td>MMO</td>
                                            <td>MTN CAM Mobile Money</td>
                                        </tr>
                                        <tr>
                                            <td>OMO</td>
                                            <td>ORANGE MONEY</td>
                                        </tr>
                                        <tr>
                                            <td>STR</td>
                                            <td>STRIPE</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-lg-9">
                    <div class="main-content">
                        <!-- Page title -->
                        <div class="page-title">
                            <div class="row align-items-center">
                                <div class="col-lg-6 col-12">
                                    <h2 class="heading heading-5 text-capitalize strong-500 mb-0">
                                        <a href="<?= site_url('notifications')?>" class="link text-underline--none">
                                            <i class="ion-ios-arrow-back"></i> Messages
                                        </a>
                                    </h2>
                                </div>
                                <div class="col-12">
                                    <?php if($this->session->flashdata('error')):?>
                                        <div class="alert alert-danger" data-dsimiss="alert">
                                            <?= $this->encryption->decrypt($this->session->flashdata('error')) ?>
                                            <button type="button" class="close pull-right" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    <?php endif; ?>
                                    <?php if($this->session->flashdata('info')):?>
                                        <div class="alert alert-success" data-dsimiss="alert">
                                            <?= $this->encryption->decrypt($this->session->flashdata('info')) ?>
                                            <button type="button" class="close pull-right" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>

                        <hr>

                        <!-- Notifications -->
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <!-- Notifications history table -->
                                <div class="card no-border">
                                    <div>
                                        <table class="table table-sm table-striped table-hover">
                                            <thead>
                                            <tr>
                                                <th>From</th>
                                                <th>Message</th>
                                                <th>Date</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            <?php foreach($notifications as $notification): ?>
                                            <tr>
                                                <td>
                                                    <?= $notification->sender ?>
                                                </td>
                                                <td>
                                                    <?= $notification->message ?>
                                                </td>

                                                <td>
                                                    <?= $notification->created_time ?>
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
