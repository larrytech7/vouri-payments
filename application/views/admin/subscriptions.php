<section class="slice-sm sct-color-1">
    <div class="profile">
        <div class="container">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-3">
                    <div class="sidebar sidebar-inverse bg-base-1 sidebar--style-2 no-border stickyfill">
                        <div class="widget">
                            <!-- Profile details -->
                            <div class="profile-details mb-4">
                                <h2 class="heading heading-6 strong-600 profile-name "><?= $this->session->user->name ?></h2>
                                <h4 class="heading heading-6 strong-600 profile-name "><?= $this->session->user->email ?></h4>
                                <h4 class="heading-5 strong-700"><?= $this->lang->line('transfer_limit').' : USD '.$transfer_limit?></h4>

                            </div>

                            <hr>

                            <ul class="categories categories--style-3 mt-3">
                                <li>
                                    <a href="<?= site_url('admin') ?>">
                                        <i class="icon-finance-059"></i>
                                        <span class="category-name">
                                            Transactions
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= site_url('payrequests')?>">
                                        <i class="ion-android-sync"></i>
                                        <span class="category-name">
                                            <?= $this->lang->line('payment_requests') ?>
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= site_url('notifications')?>" class="active">
                                        <i class="ion-email-unread"></i>
                                        <span class="category-name">
                                            Notifications
                                        </span>
                                    </a>
                                </li>
                                <?php if($this->session->user->role == 'admin') : ?>
                                    <li class="active">
                                        <a href="<?= site_url('users')?>">
                                            <i class="ion-ios-people"></i>
                                        <span class="category-name">
                                            Users
                                        </span>
                                        </a>
                                    </li>
                                <?php endif; ?>
                                <li>
                                    <a href="<?= site_url('settings')?>">
                                        <i class="ion-person"></i>
                                        <span class="category-name">
                                            Profile
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= site_url('admin/subscriptions')?>">
                                        <i class="ion-code-working"></i>
                                        <span class="category-name">
                                            <?= $this->lang->line('api') ?>
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <i class="ion-android-alert"></i>
                                    <span><?= $this->lang->line('keys') ?></span>
                                    <table class="table-info table-responsive">
                                        <thead>
                                        <tr>
                                            <th>Acronym</th>
                                            <th>Description</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>VSA</td>
                                            <td>VISA CARD</td>
                                        </tr>
                                        <tr>
                                            <td>MSA</td>
                                            <td>MASTERCARD</td>
                                        </tr>
                                        <tr>
                                            <td>MMO</td>
                                            <td>MTN CAM Mobile Money</td>
                                        </tr>
                                        <tr>
                                            <td>OMO</td>
                                            <td>ORANGE MONEY</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-lg-9">
                    <div class="main-content">
                        <!-- Page title -->
                        <div class="page-title">
                            <div class="row align-items-center">
                                <div class="col-lg-6 col-12">
                                    <h2 class="heading heading-5 text-capitalize strong-500 mb-0">
                                        <a href="<?= site_url('admin/subscriptions')?>" class="link text-underline--none">
                                            <i class="ion-ios-arrow-back"></i> Subscriptions
                                        </a>
                                        <span class="pull-right">API KEY : <code><?= $api_key ?></code></span>
                                    </h2>
                                </div>
                                <div class="col-12">
                                    <?php if($this->session->flashdata('error')):?>
                                        <div class="alert alert-danger" data-dsimiss="alert">
                                            <?= $this->encryption->decrypt($this->session->flashdata('error')) ?>
                                            <button type="button" class="close pull-right" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    <?php endif; ?>
                                    <?php if($this->session->flashdata('success')):?>
                                        <div class="alert alert-success" data-dsimiss="alert">
                                            <?= $this->encryption->decrypt($this->session->flashdata('success')) ?>
                                            <button type="button" class="close pull-right" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>

                        <hr/>

                        <!-- Notifications -->
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <!-- Notifications history table -->
                                <div class="card no-border">
                                    <div>
                                        <?= form_open('', [], ['email'=>$this->session->user->email]) ?>
                                            <button class="btn btn-success" type="submit" <?= strlen($api_key) > 0 ? 'disabled' : '' ?>><?= $this->lang->line('get_api_key')?></button>
                                        <?= form_close() ?>
                                        <table class="table table-sm table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th>API KEy</th>
                                                    <th>Frequency</th>
                                                    <th>Name</th>
                                                    <th>Description</th>
                                                    <th>Amount</th>
                                                    <th>Client Name</th>
                                                    <th>Client Phone</th>
                                                    <th>Payment Method</th>
                                                    <th>Next Charge Date</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <?php foreach($subscriptions as $sub): ?>
                                                <tr>
                                                    <td data-toggle="tooltip" title="<?= $sub->name.' ('.$sub->email.')'?>">
                                                        <?= $sub->subscription_api_key ?>
                                                    </td>
                                                    <td>
                                                        <?= $sub->subscription_type ?>
                                                    </td>
                                                    <td>
                                                        <?= $sub->subscription_name ?>
                                                    </td>
                                                    <td>
                                                        <?= $sub->subscription_description ?>
                                                    </td>
                                                    <td>
                                                        <?= $sub->subscription_amount.' '.$sub->subscription_currency ?>
                                                    </td>
                                                    <td>
                                                        <?= $sub->subscription_client_name ?>
                                                    </td>
                                                    <td>
                                                        <?= $sub->subscription_client_phone ?>
                                                    </td>
                                                    <td>
                                                        <?= $sub->subscription_payment_method ?>
                                                    </td>
                                                    <td>
                                                        <?= date('Y-m-d', mysql_to_unix($sub->subscription_client_phone)) ?>
                                                    </td>
                                                </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <h4><?= $this->lang->line('subscription_api') ?></h4>
                                <hr>
                                <p><?= $this->lang->line('copy_line_1')?></p>
                                <code>
                                    //<?= $this->lang->line('sub_api_desc')?>
                                </code>
                                <pre>
                                    <?= htmlspecialchars('<a href="#" data-toggle="modal" data-target="#checkoutModal" class="btn btn-secondary btn-lg active rounded-0 d-inline" role="button" aria-pressed="false"
                                       data-subscription="Number indicating 1 - monthly recurrent charge, 3 - three month charge, 6 - 6 month charge or 12, yearly charge. Enter 0 for non-recurrent payment"
                                       data-name="APP name"
                                       data-description="Service description"
                                       data-amount="Expected amount + 2%"
                                       data-currency="Charging currency, e.g CFA. (Will be converted to local currency before initiating payment request)"
                                       data-api-key="'.(strlen($api_key) > 0 ? $api_key : 'YOUR API KEY').'"
                                       data-callback="callback link. Data will be sent here after transaction finishes" >
                                        Subscribe
                                    </a>')
                                    ?>
                                    </pre>
                                <p><?= $this->lang->line('copy_line_2')?></p>
                                <pre><?= htmlspecialchars(site_url().'/resources/front/assets/css/subscribe.css') ?></pre>
                                <p><?= $this->lang->line('copy_line_3')?></p>
                                <pre><?= htmlspecialchars(site_url().'/resources/front/assets/js/subscribe.js') ?></pre>
                            </div>
                            <div class="col-lg-12">
                                <h4><?= $this->lang->line('programming_api') ?></h4>
                                <hr>
                                <h5><?= $this->lang->line('programming_api_usage')?></h5>
                                <p><?= $this->lang->line('programming_api_post_url')?>
                                <code>
                                    <?= site_url('payments/api/v1')?>
                                </code>
                                </p>
                                <p><?= $this->lang->line('programming_api_description')?></p>
                                <code>
                                    amount : 100 (mandatory)<br>
                                    currency : CFA (mandatory)<br>
                                    phone_number : 678651010 (mandatory) <br>
                                    payment_method : MMO (mandatory. E.g MMO - MTN Mobile Money, OMO - Orange Money, VSA - Card payments, MPA - M-pesa) <br>
                                    email : abc@xyz.pqr (optional) <br>
                                    api_key : <?= (strlen($api_key) > 0 ? $api_key : 'YOUR API KEY') ?> (mandatory) <br>
                                </code><br>
                                <p><?= $this->lang->line('programming_api_response')?></p>
                                <pre>
                                    <?= '{
                                        "transaction_id" : 123,
                                        "transaction_status" : 200,
                                        "error_message" : "some error message",
                                        "message" : "some message",
                                        "redirect_url" : "some message"
                                    }' ?>
                                </pre><br>
                                <p>
                                    <table class="table table-responsive table-striped">
                                        <thead>
                                            <tr>
                                                <th><?= $this->lang->line('response_parameter')?></th>
                                                <th><?= $this->lang->line('response_description')?></th>
                                            </tr>
                                        </thead>
                                        <tr>
                                            <td>transaction_id</td>
                                            <td>ID or reference of the transaction. Can be used to track the status of the transaction or to request a refund</td>
                                        </tr>
                                        <tr>
                                            <td>transaction_status</td>
                                            <td>Status of the transaction that represents failure or success. 200 for success and any other value for failure. 308 codes represent request which require you to redirect to vouri to complete payments</td>
                                        </tr>
                                        <tr>
                                            <td>error_message</td>
                                            <td>A description of the error message that occurred if the request did not succeed</td>
                                        </tr>
                                        <tr>
                                            <td>message</td>
                                            <td>Additional messages that represent a trace of the specific operations that produced the error.</td>
                                        </tr>
                                        <tr>
                                            <td>redirect_url</td>
                                            <td>The url to redirect to if the status code is a redirect code. Redirect to this url to complete the payment.</td>
                                        </tr>
                                    </table>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
