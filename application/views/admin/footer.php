<!-- FOOTER -->
<footer id="footer" class="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-7">
                    <div class="col">
                        <img class="" src="<?= base_url('resources/front/');?>assets/images/logo/logo.jpg" alt="vouri Inc payments" width="250" height="70">
                        <span class="clearfix"></span>
                        <span class="heading heading-sm c-gray-light strong-400">Vouri Pay.</span>
                        <p class="mt-3">
                            We are the world's leading payment aggregator powering cross provider payments painless for everyone thanks to Omnipay.
                        </p>
                    </div>
                </div>

                <div class="col-lg-2">
                    <div class="col">
                        <h4 class="heading heading-xs strong-600 text-uppercase mb-1">
                            Company
                        </h4>

                        <ul class="footer-links">
                            <li><a href="<?= site_url('about') ?>" title="About us">About</a></li>
                            <li><a href="<?= site_url('api') ?>" title="API Integrations">API</a></li>
                            <li><a href="<?= site_url('privacy') ?>" title="Privacy policies">Privacy</a></li>
                            <li><a href="<?= site_url('terms') ?>" title="Terms and conditions">Terms and Conditions</a></li>
                            <li><a href="<?= site_url('fees') ?>" title="Service fees">Service Fees</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="col">
                        <h4 class="heading heading-xs strong-600 text-uppercase mb-1">
                            Follow us on
                        </h4>
                        No, actually, do not follow us, just send or receive funds securely.
                        <!--
                        <ul class="social-media social-media--style-1-v4">
                            <li>
                                <a href="#" class="facebook" target="_blank" data-toggle="tooltip" data-original-title="Facebook">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="instagram" target="_blank" data-toggle="tooltip" data-original-title="Instagram">
                                    <i class="fa fa-instagram"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="dribbble" target="_blank" data-toggle="tooltip" data-original-title="Dribbble">
                                    <i class="fa fa-dribbble"></i>
                                </a>
                            </li>
                        </ul>
                        -->
                    </div>
                </div>
            </div>
            <div class="col-md-12 copyright">
                <p class="align-center align-items-center">
                    Copyright &copy; <?= date('Y')?>
                    <a href="https://vouriinc.com" target="_parent">
                        <strong><?= $this->lang->line('app_name')?></strong>
                    </a> -
                    <?= $this->lang->line('rights_reserved') ?>
                </p>
            </div>
        </div>
    </div>
</footer>
</div>
</div>
</div><!-- END: st-pusher -->
</div><!-- END: st-container -->
</div><!-- END: body-wrap -->
<?php if(isset($users)): ?>
<?php foreach($users as $user ): ?>
    <div class="d-none">
        <div id="<?= $user->user_id ?>">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="form-header text-center">
                        <div class="form-header-icon">
                            <i class="icon ion-send"></i>
                        </div>
                    </div>
                    <p>
                        User Details
                    </p>
                    <hr>
                    <h1 class="text-center"><span><?= $user->name ?></span></h1>
                    <hr>
                    <div class="row">
                        <div class="col-6">
                            ID
                        </div>
                        <div class="col-lg-6">
                            <?= $user->user_id ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            Role
                        </div>
                        <div class="col-lg-6">
                            <?= $user->role ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            Name
                        </div>
                        <div class="col-lg-6">
                            <?= $user->name ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            Phone
                        </div>
                        <div class="col-lg-6">
                            <?= $user->phone ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            Email
                        </div>
                        <div class="col-lg-6">
                            <?= $user->email ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            Created At
                        </div>
                        <div class="col-lg-6">
                            <?= $user->create_time ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            Updated At
                        </div>
                        <div class="col-lg-6">
                            <?= $user->update_time ?>
                        </div>
                    </div>
                </div>

                <div class="col-md-2"></div>
            </div>
        </div>
    </div>
<?php endforeach; ?>
<?php endif; ?>

<?php if(isset($transactions)): ?>
<?php foreach($transactions as $t ): ?>
    <div class="d-none">
        <div id="<?= $t->transaction_id ?>">
            <div class="row">
                <div class="col-2"></div>
                <div class="col-8">
                    <div class="form-header text-center">
                        <div class="form-header-icon">
                            <i class="ion-cash"></i>
                        </div>
                    </div>
                    <hr>
                    <h3 class="text-center"><span>Transaction details</span></h3>
                    <hr>
                    <div class="row">
                        <div class="col-6">
                            Amount charged
                        </div>
                        <div class="col-lg-6">
                            <?= number_format($t->amount,2).' '.$t->currency ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            Amount Sent
                        </div>
                        <div class="col-lg-6">
                            <?= number_format($t->amount_sent,2).' '.$t->receiving_currency ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            Fee (HT)
                        </div>
                        <div class="col-lg-6">
                            <?= round((4/100) * $t->amount , 2).' '.$t->currency ?>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-lg-6">
                            Sent By
                        </div>
                        <div class="col-lg-6">
                            <?= $t->email ?><br>
                            <?= $t->name ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            Received By
                        </div>
                        <div class="col-lg-6">
                            <?= $t->receiver_phone ?><br>
                            <?= $t->receiver_email ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            Sent Via
                        </div>
                        <div class="col-lg-6">
                            <?= $t->send_gateway ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            Received VIA
                        </div>
                        <div class="col-lg-6">
                            <?= $t->receive_gateway ?>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-lg-6">
                            Message
                        </div>
                        <div class="col-lg-6">
                            <?= $t->message ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            Status
                        </div>
                        <div class="col-lg-6">
                            <?php if($t->status == 0): ?>
                                <span class="badge badge--2 mr-4">
                                    <i class="bg-yellow"></i> <?= $this->lang->line('pending') ?>
                                </span>
                            <?php endif;
                            if($t->status == 1):?>
                                <span class="badge badge--2 mr-4">
                                    <i class="bg-green"></i> <?= $this->lang->line('paid') ?>
                                </span>
                            <?php endif; ?>
                            <?php if($t->status == -1):?>
                                <span class="badge badge--2 mr-4">
                                    <i class="bg-red"></i> <?= $this->lang->line('failed') ?>
                                </span>
                            <?php endif;?>
                            <?php if ($this->session->user->role == 'admin'): ?>
                                <?= anchor('admin/approve/'.$t->transaction_id, '<i class="ion ion-checkmark-circled text-success quote-icon-md"></i>', ['class'=>'text-success'])?>
                                <?= anchor('admin/reject/'.$t->transaction_id, '<i class="ion ion-trash-b text-danger quote-icon-md"></i>', ['class'=>'text-danger'])?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            Sent On
                        </div>
                        <div class="col-lg-6">
                            <?= $t->create_time ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            Received On
                        </div>
                        <div class="col-lg-6">
                            <?= $t->update_time ?>
                        </div>
                    </div>
                </div>

                <div class="col-2"></div>
            </div>
        </div>
    </div>
<?php endforeach; ?>
<?php endif; ?>
<div class="d-none">
    <div id="fancybox_makepayment">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="form-header text-center">
                    <div class="form-header-icon">
                        <i class="icon ion-send"></i>
                    </div>
                </div>
                <!-- <p>Service currently not available but will soon be restored. We are working to fix this. Thanks for your patience.</p>
                -->

                <p>
                    Send money to anyone via the payment method of your choice.
                </p>

                <h1 class="text-center"><span id="amounttag">0.0</span><span class="badge badge-success" id="currencytag">USD</span></h1>
                <?= form_open(site_url('sendfunds'), ['class' => 'form-default'
                    , 'role' => 'form'] )?>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label for="amounttosend">Amount to send </label>  <i class="ion icon-info pull-right" data-toggle="tooltip" data-placement="top" title="How much do you wish to send? Enter the amount in the currency you will indicate below"></i>
                            <div class="input-group">
                                <input type="number" name="amounttosend" id="amounttosend" min="0" step="1" class="form-control form-control-lg" required>
                                        <span class="input-group-addon">
                                            <i class="ion icon-ecommerce-dollar"></i>
                                        </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group has-feedback">
                            <label for="currency">Currency</label>  <i class="icon-info pull-right" data-toggle="tooltip" title="Select the currency you wish to use for the transaction. Note that conversion fees may apply for currencies other than USD"></i>
                            <select id="currency" name="currency" class="form-control form-control-lg">
                                <option value="USD" selected>USD - US Dollar</option>
                                <option value="CAD">CAD - Canadian Dollar</option>
                                <option value="EUR">EUR - Euro</option>
                                <option value="GBP">GBP - British Pound</option>
                                <option value="XAF">XAF - Central African Franc</option>
                                <option value="NGN">NGN - Naira</option>
                                <option value="AED">AED - Emirates Dirham</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group has-feedback">
                            <label for="sendfrom">Send Gateway</label>  <i class="icon-info pull-right" data-toggle="tooltip" title="Select the payment gateway you wish to use for the transaction."></i>
                            <select id="sendfrom" name="sendfrom" class="form-control form-control-lg">
                                <option value="STR">STRIPE</option>
                                <option value="MMO">MTN MOBILE MONEY</option>
                                <option value="VSA" selected>VISA/MASTERCARD </option>
                                <option value="SQR">SQUARE</option>
                            </select>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-styled btn-lg btn-block btn-base-1 mt-4"> Continue</button>
                </form>

            </div>

            <div class="col-md-3"></div>
        </div>
    </div>
</div>

<div class="d-none">
    <div id="fancybox_email">
        <div class="row">
            <div class="col-md-12">
                <div class="form-header text-center">
                    <div class="form-header-icon">
                        <i class="icon ion-send"></i>
                    </div>
                </div>

                <p>
                    Send broadcast email
                </p>
                <?= form_open(site_url('admin/massmessage'), ['class' => 'form-default'
                    , 'role' => 'form'] )?>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label for="message">Message </label>  <i class="ion icon-info pull-right" data-toggle="tooltip" data-placement="top" title="How much do you wish to send? Enter the amount in the currency you will indicate below"></i>
                            <div class="input-group">
                                <textarea name="message" id="message"  rows="5" class="form-control form-control-lg" required></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-styled btn-lg btn-block btn-base-1 mt-4"> Send </button>
                </form>

            </div>
        </div>
    </div>
</div>

<div class="d-none">
    <div id="fancybox_requestpay">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="form-header text-center">
                    <div class="form-header-icon">
                        <i class="icon ion-send"></i>
                    </div>
                </div>
                <p>
                    Send a payment request and get paid easily and fast.
                </p>
                <h1 class="text-center"><span id="">0.0</span><span class="badge badge-success">USD</span></h1>
                <?= form_open(site_url('paymentrequest'), ['class' => 'form-default'
                    , 'role' => 'form'] )?>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label for="amounttosend">Amount requested </label>
                            <div class="input-group">
                                <input type="number" name="amount" id="amounttosend" min="0" step="1" class="form-control form-control-lg" required>
                                    <span class="input-group-addon">
                                        <i class="ion icon-ecommerce-dollar"></i>
                                    </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label for="email">Email </label>
                            <div class="input-group">
                                <input type="email" name="email" class="form-control form-control-lg" required>
                                    <span class="input-group-addon">
                                        <i class="ion icon-basic-mail"></i>
                                    </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label for="phone">Phone number</label>
                            <div class="input-group">
                                <input type="number" name="phone" min="0" step="1" class="form-control form-control-lg" required>
                                    <span class="input-group-addon">
                                        <i class="ion ion-android-phone-portrait"></i>
                                    </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group has-feedback">
                            <label for="currency">Currency</label>
                            <select id="currency" name="currency" class="form-control form-control-lg">
                                <option value="USD" selected>USD - US Dollar</option>
                                <option value="EUR">EUR - Euro</option>
                                <option value="GBP">GBP - British Pound</option>
                                <option value="XAF">XAF - Central African Franc</option>
                                <option value="NGN">NGN - Naira</option>
                                <option value="AED">AED - Saudi Dirham</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group has-feedback">
                            <label for="sendfrom">To receive via</label>
                            <select id="sendfrom" name="gateway" class="form-control form-control-lg">
                                <option value="STR">STRIPE</option>
                                <option value="MMO">MTN MOBILE MONEY</option>
                                <option value="OMO">ORANGE MONEY</option>
                                <option value="EWY">eWAY</option>
                                <option value="SQR">SQUARE</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <textarea name="message" class="form-control form-control-lg" placeholder="Any reason for requesting this payment?"></textarea>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-styled btn-lg btn-block btn-base-1 mt-4"> Send Request</button>
                </form>

            </div>

            <div class="col-md-1"></div>
        </div>
    </div>
</div>

<!-- SCRIPTS -->

<!-- Core -->
<script src="<?= base_url('resources/');?>assets/vendor/jquery/jquery.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/popper/popper.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="<?= base_url('resources/');?>assets/js/slidebar/slidebar.js"></script>
<script src="<?= base_url('resources/');?>assets/js/classie.js"></script>
<script src="<?= base_url('resources/');?>assets/js/run.js"></script>
<script  src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<!-- Bootstrap Extensions -->
<script src="<?= base_url('resources/');?>assets/vendor/bootstrap-notify/bootstrap-growl.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/scrollpos-styler/scrollpos-styler.js"></script>

<!-- Plugins: Sorted A-Z -->
<script src="<?= base_url('resources/');?>assets/vendor/adaptive-backgrounds/adaptive-backgrounds.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/countdown/js/jquery.countdown.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/dropzone/dropzone.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/easy-pie-chart/jquery.easypiechart.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/fancybox/js/jquery.fancybox.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/flatpickr/flatpickr.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/flip/flip.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/footer-reveal/footer-reveal.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/gradientify/jquery.gradientify.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/headroom/headroom.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/headroom/jquery.headroom.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/input-mask/input-mask.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/instafeed/instafeed.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/milestone-counter/jquery.countTo.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/nouislider/js/nouislider.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/paraxify/paraxify.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/select2/js/select2.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/sticky-kit/sticky-kit.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/swiper/js/swiper.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/textarea-autosize/autosize.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/typeahead/typeahead.bundle.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/typed/typed.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/vide/vide.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/viewport-checker/viewportchecker.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/wow/wow.min.js"></script>

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-112810447-2"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-112810447-2');
</script>
<script>
    $(document).ready( function () {
        $('.table').DataTable({
            "order" : [[0, "desc"]]
        });
        //change sendto field
        $('#btn-key').click(function(){
            $(this).prop('disabled', true);
        })

    } );
</script>
<?php if($this->session->info) : ?>
    <script>
        toastr.success('<?= $this->session->info ?>', {
            timeOut : 10000,//10 seconds visibility
            position : 'right-top',
            size : 'lg'
        })
    </script>
<?php endif; ?>
<?php if($this->session->error) : ?>
    <script>
        toastr.error('<?= $this->session->error ?>', {
            timeOut : 10000,//10 seconds visibility
            position : 'right-top',
            size : 'lg'
        })
    </script>
<?php endif; ?>
<!-- Isotope -->
<script src="<?= base_url('resources/');?>assets/vendor/isotope/isotope.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/imagesloaded/imagesloaded.pkgd.min.js"></script>
<!-- App JS -->
<script src="<?= base_url('resources/');?>assets/js/boomerang.min.js"></script>

</body>
</html>