<!DOCTYPE html>
<html>
<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<meta name="robots" content="index, follow">
<meta name="description" content="Online payment aggregator and global payment platform for sending and receiving money in multiple currencies through multiple payment platforms.">
<meta name="keywords" content="payment, aggregator, online, remote, pay, remittance, remittances, global payments, money, transfer, transaction, support, business, corporate, real estate, education, medical, school, education, demo, css, framework">
<meta name="author" content="Vouri Inc">
	<title><?= $this->lang->line('app_name') ?> - <?= $this->lang->line('app_short_description') ?></title>

<!-- Bootstrap -->
<link rel="stylesheet" href="<?= base_url('resources/');?>assets/vendor/bootstrap/css/bootstrap.min.css" type="text/css">

<!-- Fonts -->
<link href="<?= base_url('resources/');?>fonts.googleapis.com/cssfb86.css?family=Nunito:400,600,700,800|Roboto:400,500,700" rel="stylesheet"> 

<!-- Plugins -->
<link rel="stylesheet" href="<?= base_url('resources/');?>assets/vendor/swiper/css/swiper.min.css">
<link rel="stylesheet" href="<?= base_url('resources/');?>assets/vendor/hamburgers/hamburgers.min.css" type="text/css">
<link rel="stylesheet" href="<?= base_url('resources/');?>assets/vendor/animate/animate.min.css" type="text/css">
<link rel="stylesheet" href="<?= base_url('resources/');?>assets/vendor/fancybox/css/jquery.fancybox.min.css">

<!-- Icons -->
<link rel="stylesheet" href="<?= base_url('resources/');?>assets/fonts/font-awesome/css/font-awesome.min.css" type="text/css">
<link rel="stylesheet" href="<?= base_url('resources/');?>assets/fonts/ionicons/css/ionicons.min.css" type="text/css">
<link rel="stylesheet" href="<?= base_url('resources/');?>assets/fonts/line-icons/line-icons.css" type="text/css">
<link rel="stylesheet" href="<?= base_url('resources/');?>assets/fonts/line-icons-pro/line-icons-pro.css" type="text/css">

<!-- Linea Icons -->
<link rel="stylesheet" href="<?= base_url('resources/');?>assets/fonts/linea/arrows/linea-icons.css" type="text/css">
<link rel="stylesheet" href="<?= base_url('resources/');?>assets/fonts/linea/basic/linea-icons.css" type="text/css">
<link rel="stylesheet" href="<?= base_url('resources/');?>assets/fonts/linea/ecommerce/linea-icons.css" type="text/css">
<link rel="stylesheet" href="<?= base_url('resources/');?>assets/fonts/linea/software/linea-icons.css" type="text/css">

	<!-- Datatables CSS -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css" type="text/css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" type="text/css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" type="text/css">

<!-- Global style (main) -->
<link id="stylesheet" type="text/css" href="<?= base_url('resources/');?>assets/css/boomerang.min.css" rel="stylesheet" media="screen">

<!-- Custom style - Remove if not necessary -->
<link type="text/css" href="<?= base_url('resources/');?>assets/css/custom-style.css" rel="stylesheet">

<!-- icon -->
	<link rel="icon" type="image/png" href="<?= base_url('resources/front/');?>assets/images/favicon.png" />

</head>
<body>
<div class="page_loader"></div>

<!-- MAIN WRAPPER -->
<div class="body-wrap" data-template-mode="cards">
    <div id="st-container" class="st-container">
        <div class="st-pusher">
            <div class="st-content">
                <div class="st-content-inner">

                    <!-- Header -->
                    <div class="header">
						<!-- Navbar -->
						<nav class="navbar navbar-expand-lg navbar--bb-1px navbar--bold navbar-light bg-default ">
							<div class="container navbar-container">
								<!-- Brand/Logo -->
									<a class="navbar-brand" href="<?= site_url('/admin');?>">
										<img class="" src="<?= base_url('resources/front/');?>assets/images/logo/logo.jpg" alt="vouri Inc payments" width="250" height="70">
									</a>
								<ul class="navbar-nav ml-lg-auto">
									<li class="nav-item active">
										<a href="javascript:;" class="nav-link active" data-fancybox data-src="#fancybox_makepayment">
										   <i class="ion-ios-paperplane"></i><?= $this->lang->line('send')?>
										</a>
									</li>
									<li class="nav-item">
										<a href="javascript:;" class="nav-link" data-fancybox data-src="#fancybox_requestpay">
										   <?= $this->lang->line('receive') ?>
										</a>
									</li>
									<?php if ($this->session->user->role == 'admin'): ?>
										<li class="nav-item">
											<a href="javascript:;" class="nav-link" data-fancybox data-src="#fancybox_email">
												Mass Email
											</a>
										</li>
									<?php endif; ?>
									<li class="nav-item">
										<a href="<?= site_url('logout'); ?>" class="nav-link">
										   <?= $this->lang->line('logout')?> <i class="icon-logout"></i>
										</a>
									</li>
								</ul>
							</div>
						</nav>
	
					</div>