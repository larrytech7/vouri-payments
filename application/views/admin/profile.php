<section class="slice-sm sct-color-1">
    <div class="profile">
        <div class="container">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-3">
                    <div class="sidebar sidebar-inverse bg-base-1 sidebar--style-2 no-border stickyfill">
                        <div class="widget">
                            <!-- Profile details -->
                            <div class="profile-details mb-4">
                                <h2 class="heading heading-5 strong-600 profile-name "><?= $this->session->user->name ?></h2>
                                <h4 class="heading heading-6 strong-600 profile-name "><?= $this->session->user->email ?></h4>
                                <h4 class="heading-5 strong-700"><?= $this->lang->line('transfer_limit').' : USD '.$transfer_limit?></h4>
                            </div>

                            <hr>

                            <ul class="categories categories--style-3 mt-3">
                                <li>
                                    <a href="<?= site_url('admin') ?>" class="active">
                                        <i class="icon-finance-059"></i>
                                        <span class="category-name">
                                            Transactions
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= site_url('payrequests')?>">
                                        <i class="ion-android-sync"></i>
                                        <span class="category-name">
                                            <?= $this->lang->line('payment_requests') ?>
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= site_url('notifications')?>">
                                        <i class="ion-email-unread"></i>
                                        <span class="category-name">
                                            Notifications
                                        </span>
                                    </a>
                                </li>
                                <?php if($this->session->user->role == 'admin') : ?>
                                    <li class="active">
                                        <a href="<?= site_url('users')?>">
                                            <i class="ion-ios-people"></i>
                                        <span class="category-name">
                                            Users
                                        </span>
                                        </a>
                                    </li>
                                <?php endif; ?>
                                <li>
                                    <a href="<?= site_url('settings')?>">
                                        <i class="ion-person"></i>
                                        <span class="category-name">
                                            Profile
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= site_url('admin/subscriptions')?>">
                                        <i class="ion-code-working"></i>
                                        <span class="category-name">
                                            <?= $this->lang->line('api') ?>
                                        </span>
                                    </a>
                                </li>
                                <hr>
                                <li>
                                    <i class="ion-android-alert"></i>
                                    <span>Keys</span>
                                    <ul>
                                        <li> VSA - VISA CARD</li>
                                        <li> MMO - MTN Mobile Money</li>
                                        <li> OMO - Orange Money</li>
                                        <li> STR - Stripe</li>
                                        <li> EWY - eWay</li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-lg-9">
                    <div class="main-content">
                        <!-- Page title -->
                        <div class="page-title">
                            <div class="row align-items-center">
                                <div class="col-lg-6 col-12">
                                    <h2 class="heading heading-5 text-capitalize strong-500 mb-0">
                                        <a href="#" class="link text-underline--none">
                                            <i class="ion-ios-arrow-back"></i> Profile data
                                        </a>
                                    </h2>
                                </div>
                            </div>
                        </div>
                        <?php if($this->session->flashdata('error')): ?>
                            <div class="alert alert-danger" data-dsimiss="alert">
                                <?= $this->encryption->decrypt($this->session->flashdata('error')) ?>
                                <button type="button" class="close pull-right" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php endif; ?>
                        <?php if($this->session->flashdata('info')): ?>
                            <div class="alert alert-success" data-dsimiss="alert">
                                <?= $this->encryption->decrypt($this->session->flashdata('info')) ?>
                                <button type="button" class="close pull-right" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php endif; ?>
                        <hr>

                        <!-- Notifications -->
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <!-- Notifications history table -->
                                <div class="card no-border">
                                    <div class="align-items-center">
                                        <?= form_open_multipart('settings', ['class' => 'form-default'
                                            , 'role' => 'form'])?>
                                        <div class="row">
                                            <?= validation_errors() ?>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group has-feedback">
                                                    <label for="name"><?= $this->lang->line('name_pseudo')?></label>
                                                    <div class="input-group">
                                                        <input type="text" name="name" id="name" value="<?= set_value('name', $this->session->user->name)?>" class="form-control form-control-lg" placeholder="<?= $this->lang->line('name_pseudo')?>" required>
                                                        <span class="input-group-addon">
                                                            <i class="ion ion-person"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="email">Email </label>
                                                    <div class="input-group">
                                                        <input type="email" name="email" id="email" value="<?= set_value('email', $this->session->user->email)?>" class="form-control form-control-lg" placeholder="email" disabled>
                                                        <span class="input-group-addon">
                                                            <i class="ion ion-email"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="phone"><?= $this->lang->line('phone_number')?></label>
                                                    <div class="input-group">
                                                        <input type="tel" name="phone" id="phone" value="<?= set_value('phone', $this->session->user->phone)?>" class="form-control form-control-lg" placeholder="<?= $this->lang->line('phone_number')?>" required>
                                                        <span class="input-group-addon">
                                                            <i class="ion ion-android-phone-portrait"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="phone"><?= $this->lang->line('address')?></label>
                                                    <div class="input-group">
                                                        <input type="text" name="address" id="address" value="<?= set_value('address', $this->session->user->address)?>" class="form-control form-control-lg" placeholder="<?= $this->lang->line('address')?>">
                                                        <span class="input-group-addon">
                                                            <i class="ion ion-location"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="phone"><?= $this->lang->line('city')?></label>
                                            <div class="input-group">
                                                <input type="text" name="city" id="city" value="<?= set_value('city', $this->session->user->city)?>" class="form-control form-control-lg" placeholder="<?= $this->lang->line('city')?>">
                                                <span class="input-group-addon">
                                                    <i class="ion ion-home"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="phone"><?= $this->lang->line('country')?></label>
                                            <div class="input-group">
                                                <input type="text" name="country" id="country" value="<?= set_value('country', $this->session->user->country)?>" class="form-control form-control-lg" placeholder="<?= $this->lang->line('country')?>">
                                                <span class="input-group-addon">
                                                    <i class="ion ion-flag"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="phone"><?= $this->lang->line('occupation')?></label>
                                            <div class="input-group">
                                                <input type="text" name="occupation" id="occupation" value="<?= set_value('occupation', $this->session->user->occupation)?>" class="form-control form-control-lg" placeholder="<?= $this->lang->line('occupation_title')?>">
                                                <span class="input-group-addon">
                                                    <i class="ion ion-briefcase"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="phone"><?= $this->lang->line('income')?></label>
                                            <div class="input-group">
                                                <input type="number" min="0" name="income" id="income" value="<?= set_value('income', $this->session->user->monthly_income)?>" class="form-control form-control-lg" placeholder="<?= $this->lang->line('income_title')?>">
                                                <span class="input-group-addon">
                                                    <i class="ion ion-cash"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="phone"><?= $this->lang->line('id_document')?></label>
                                            <div class="input-group">
                                                <input type="file" name="nid" id="nid" accept="image/*" class="form-control form-control-lg">
                                                <span class="input-group-addon">
                                                    <i class="ion ion-android-contact"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-styled btn-lg btn-block btn-base-1 mt-4"><i class="ion ion-edit icon-lg"></i> Update</button>
                            </form>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-4"></div>
                            <div class="col-lg-4 col-md-4 align-items-center">
                                <hr>
                                <a href="<?= site_url('admin/account/delete/'.$this->session->user->user_id)?>" onclick="javascript:return confirm('<?= $this->lang->line('delete_account_note')?>')" class="btn btn-styled btn-lg btn-block btn-danger mt-4">
                                    <i class="ion ion-trash-b icon-lg"></i> <?= $this->lang->line('delete_account') ?>
                                </a>
                            </div>
                            <div class="col-lg-4 col-md-4"></div>

                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12 col-12">
                                <p><h4><?= $this->lang->line('payment_instruments') ?></h4></p>
                                <table class="table table-sm table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->lang->line('instrument_name') ?></th>
                                        <th><?= $this->lang->line('instrument_type') ?></th>
                                        <th><?= $this->lang->line('instrument_number') ?></th>
                                        <th><?= $this->lang->line('instrument_expiration') ?></th>
                                        <th><?= $this->lang->line('instrument_origin') ?></th>
                                        <th><?= $this->lang->line('instrument_fingerprint') ?></th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <?php foreach($instruments as $instrument): ?>
                                        <tr class="parent">
                                            <td>
                                                <?= $instrument->instrument_name ?>
                                            </td>
                                            <td>
                                                <?= $instrument->instrument_type ?>
                                            </td>
                                            <td>
                                                <?= $instrument->instrument_number ?>
                                            </td>
                                            <td>
                                                <?= $instrument->instrument_expiration ?>
                                            </td>
                                            <td>
                                                <?= $instrument->instrument_origin ?>
                                            </td>

                                            <td>
                                                <?= $instrument->instrument_fingerprint ?>
                                            </td>
                                            <td class="text-center">
                                                <?= anchor('admin/instrument/delete/'.$instrument->instrument_id,
                                                    '<i class="ion-ios-trash text-danger"></i>',
                                                    ['class'=>'text-danger',
                                                    'data-toggle' => 'tooltip',
                                                    'title' => $this->lang->line('remove_instrument')])?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>