
<!-- Hero Image and Title -->
<div class="hero-body">
    <div class="container">
        <div class="columns is-vcentered">

            <!-- Landing page Title -->
            <div class="column is-5 landing-caption">
                <h2 class="title is-1 is-light is-semibold is-spaced main-title">
                    <?= $this->lang->line('home_page_title_one_desc') ?>
                </h2>
                <h3 class="subtitle is-4 is-dark is-thin">
                    <span id="typed-text" class="typed-text"></span>
                </h3>
                <span id="typed"></span>
                <!-- CTA -->
                <p>
                    <a href="#" id="btn-send" class="button k-button k-primary raised has-gradient is-fat is-bold">
                        <span class="text"><?= $this->lang->line('send_money') ?></span>
                        <span class="front-gradient"></span>
                    </a>
                    <a href="#" id="btn-receive" class="button k-button k-primary raised has-gradient is-fat is-bold">
                        <span class="text"><?= $this->lang->line('get_money') ?></span>
                        <span class="front-gradient"></span>
                    </a>
                </p>
            </div>
            <!-- Hero image -->
            <div class="column is-7">
                <figure class="image">
                    <img src="<?= base_url('resources/front/');?>assets/images/illustrations/world.svg" alt="">
                </figure>
            </div>
        </div>
    </div>
</div>

<!-- Hero footer -->
<div class="hero-foot">
    <div class="container">
        <div class="tabs is-centered">
            <!-- Client / partner list -->
            <ul>
                <li><a><img class="hero-logo" src="<?= base_url('resources/front/');?>assets/images/clients/m-pesa.webp" alt="m-pesa"></a></li>
                <li><a><img class="hero-logo" src="<?= base_url('resources/front/');?>assets/images/clients/omo.jpg" alt="momo"></a></li>
                <li><a><img class="hero-logo" src="<?= base_url('resources/front/');?>assets/images/clients/visa.svg" alt="visa"></a></li>
                <li><a><img class="hero-logo" src="<?= base_url('resources/front/');?>assets/images/clients/mastercard.svg" alt="mastercard"></a></li>
                <li><a><img class="hero-logo" src="<?= base_url('resources/front/');?>assets/images/clients/discover.svg" alt="Discover"></a></li>
                <li><a><img class="hero-logo" src="<?= base_url('resources/front/');?>assets/images/clients/american-express.svg" alt="AE"></a></li>
                <li><a><img class="hero-logo" src="<?= base_url('resources/front/');?>assets/images/clients/momo.png" alt="MOMO"></a></li>
            </ul>
        </div>
    </div>
</div>

</section>
<!-- /Landing page Hero -->

<!-- Icon Features section -->
<section id="start" class="section is-transparent is-relative">
    <!-- Container -->
    <div class="container">

        <div class="has-text-centered">
            <a href="<?= site_url('home/api')?>" class="button k-button k-secondary raised has-gradient is-fat is-bold rounded">
                <span class="text">Integrate the API</span>
                <span class="front-gradient"></span>
            </a>

            <div class="buy-title">We support several Interfaces</div>

            <div class="accepted-currencies">
                <div>
                    <i class="cf fa-cc-visa"></i>
                </div>
                <div>
                    <i class="cf fa-cc-mastercard"></i>
                </div>
                <div>
                    <i class="cf fa-cc-discover"></i>
                </div>
                <div>
                    <i class="cf fa-mobile"></i>
                </div>
                <div>
                    <i class="cf fa-bank"></i>
                </div>
            </div>
        </div>

        <!-- Content wrapper -->
        <div class="content-wrapper is-medium">
            <div class="columns is-vcentered">
                <!-- Feature -->
                <div class="column is-4">
                    <div class="feature">
                        <img src="<?= base_url('resources/front/');?>assets/images/shield.svg" alt="" data-aos="fade-up" data-aos-delay="100" data-aos-offset="200" data-aos-easing="ease-out-quart">
                        <h4 class="title is-6 is-tight is-light"><?= $this->lang->line('security') ?></h4>
                        <p><?= $this->lang->line('security_note') ?></p>
                    </div>
                </div>
                <!-- Feature -->
                <div class="column is-4">
                    <div class="feature">
                        <img src="<?= base_url('resources/front/');?>assets/images/performance.svg" alt="" data-aos="fade-up" data-aos-delay="300" data-aos-offset="200" data-aos-easing="ease-out-quart">
                        <h4 class="title is-6 is-tight is-light"><?= $this->lang->line('speed') ?></h4>
                        <p><?= $this->lang->line('speed_note') ?></p>
                    </div>
                </div>
                <!-- Feature -->
                <div class="column is-4">
                    <div class="feature">
                        <img src="<?= base_url('resources/front/');?>assets/images/accessible.svg" alt="" data-aos="fade-up" data-aos-delay="500" data-aos-offset="200" data-aos-easing="ease-out-quart">
                        <h4 class="title is-6 is-tight is-light"><?= $this->lang->line('reliable') ?></h4>
                        <p><?= $this->lang->line('reliable_note') ?></p>
                    </div>
                </div>
            </div>
            <div class="columns is-vcentered">
                <!-- Feature -->
                <div class="column is-4">
                    <div class="feature">
                        <img src="<?= base_url('resources/front/');?>assets/images/africa.svg" alt="" data-aos="fade-up" data-aos-delay="100" data-aos-offset="200" data-aos-easing="ease-out-quart">
                        <h4 class="title is-6 is-tight is-light"><?= $this->lang->line('africa') ?></h4>
                        <p><?= $this->lang->line('africa_note') ?></p>
                    </div>
                </div>
                <!-- Feature -->
                <div class="column is-4">
                    <div class="feature">
                        <img src="<?= base_url('resources/front/');?>assets/images/code.svg" alt="" data-aos="fade-up" data-aos-delay="300" data-aos-offset="200" data-aos-easing="ease-out-quart">
                        <h4 class="title is-6 is-tight is-light"><?= $this->lang->line('development') ?></h4>
                        <p><?= $this->lang->line('development_note') ?></p>
                    </div>
                </div>
                <!-- Feature -->
                <div class="column is-4">
                    <div class="feature">
                        <img src="<?= base_url('resources/front/');?>assets/images/fees.svg" alt="" data-aos="fade-up" data-aos-delay="500" data-aos-offset="200" data-aos-easing="ease-out-quart">
                        <h4 class="title is-6 is-tight is-light"><?= $this->lang->line('service_fees') ?></h4>
                        <p><?= $this->lang->line('service_fees_note') ?></p>
                    </div>
                </div>
            </div>

            <!-- Play video button
            <div class="cta-wrapper has-text-centered">
                <div class="video-button levitate js-modal-btn" data-video-id="6WG7D47tGb0">
                    <img src="<?= base_url('resources/front/');?>assets/images/icons/play.svg" alt="">
                </div>
            </div>
            -->
        </div>
        <!-- Content wrapper -->
    </div>
    <!-- /Container -->
</section>

<!-- Side Features section -->
<section id="big-gradient" class="section is-transparent">
    <!-- Container -->
    <div class="container">
        <!-- Divider -->
        <div class="divider is-centered"></div>
        <!-- Title & subtitle -->
        <h2 class="title is-light is-semibold has-text-centered is-spaced"><?= $this->lang->line('how') ?></h2>
        <h4 class="subtitle is-6 is-light has-text-centered is-compact"><?= $this->lang->line('how_desc') ?></h4>

        <!-- Content wrapper -->
        <div class="content-wrapper is-large">
            <div class="columns is-vcentered">

                <!-- Feature content -->
                <div class="column is-5 is-offset-1">
                    <div class="side-feature-content">
                        <h3 class="title is-4 is-light">
                            <img src="<?= base_url('resources/front/')?>assets/images/icons/one.svg" alt="">
                            <span><?= $this->lang->line('sender_info') ?></span>
                        </h3>
                        <div class="divider is-long"></div>
                        <p class="is-light">
                            <?= $this->lang->line('step1') ?>
                        </p>
                        <div class="cta-wrapper">
                            <a href="<?= site_url('signup') ?>" class="button k-button k-primary raised has-gradient is-bold">
                                <span class="text"><?= $this->lang->line('signup')?></span>
                                <span class="front-gradient"></span>
                            </a>
                        </div>
                    </div>
                </div>

                <!-- Feature image -->
                <div class="column is-7">
                    <img class="side-feature" src="<?= base_url('resources/assets/images/backgrounds/step1.PNG')?>" alt="">
                </div>
            </div>

            <div class="columns is-vcentered">
                <!-- Feature image desktop -->
                <div class="column is-7 is-hidden-mobile">
                    <img class="side-feature" src="<?= base_url('resources/front/assets/images/illustrations/bounty.svg')?>" alt="">
                </div>

                <!-- Feature content -->
                <div class="column is-5">
                    <div class="side-feature-content">
                        <h3 class="title is-4 is-light">
                            <img src="<?= base_url('resources/front/')?>assets/images/icons/two.svg" alt="">
                            <span><?= $this->lang->line('receiver_info') ?></span>
                        </h3>
                        <div class="divider is-long"></div>
                        <p class="is-light"><?= $this->lang->line('step2') ?></p>
                        <div class="cta-wrapper">
                            <a href="<?= site_url('signup')?>" class="button k-button k-primary raised has-gradient is-bold">
                                <span class="text"><?= $this->lang->line('signup')?></span>
                                <span class="front-gradient"></span>
                            </a>
                        </div>
                    </div>
                </div>

                <!-- Feature image only for mobile -->
                <div class="column is-7 is-hidden-desktop is-hidden-tablet">
                    <img class="side-feature" src="<?= base_url('resources/front/')?>assets/images/illustrations/blockchain-blocks.svg" alt="">
                </div>
            </div>

            <div class="columns is-vcentered">
                <!-- Feature content -->
                <div class="column is-5 is-offset-1">
                    <div class="side-feature-content">

                        <h3 class="title is-4 is-light">
                            <img src="<?= base_url('resources/front/')?>assets/images/icons/three.svg" alt="">
                            <span><?= $this->lang->line('make_payment') ?></span></h3>
                        <div class="divider is-long"></div>
                        <p class="is-light"><?= $this->lang->line('step3') ?></p>
                        <div class="cta-wrapper">
                            <a href="<?= site_url('signup')?>" class="button k-button k-primary raised has-gradient is-bold">
                                <span class="text"><?= $this->lang->line('signup')?></span>
                                <span class="front-gradient"></span>
                            </a>
                        </div>
                    </div>
                </div>

                <!-- Feature image -->
                <div class="column is-7">
                    <img class="side-feature" src="<?= base_url('resources/front/')?>assets/images/illustrations/crypto-mining.svg" alt="">
                </div>
            </div>
        </div>
        <!-- /Content wrapper -->
    </div>
    <!-- /Container -->
</section>
<!-- /Side Features section -->

<!-- App section -->
<section class="section is-medium">
    <!-- Container -->
    <div class="container">
        <div class="divider is-centered"></div>
        <!-- Title -->
        <h2 class="title is-light is-semibold has-text-centered is-spaced"><?= $this->lang->line('vouri_exchange') ?></h2>
        <h4 class="subtitle is-6 is-light has-text-centered is-compact"><?= $this->lang->line('exchange_text') ?></h4>
        <!-- Content wrapper -->
        <div class="content-wrapper is-large">

            <!-- Row -->
            <div class="columns is-vcentered">
                <div class="column is-5">
                    <!-- Side feature -->
                    <div class="side-feature-content">
                        <!-- Title -->
                        <h3 class="title is-4 is-light">
                            <img src="<?= base_url('resources/front/')?>assets/images/icons/ico/community.svg" alt="">
                            <span></span>
                        </h3>
                        <!-- Divider -->
                        <div class="divider is-long"></div>
                        <p class="is-light"><?= $this->lang->line('source_title') ?></p>
                        <!-- CTA -->
                    </div>
                </div>
                <!-- Featured image -->
                <div class="column is-7">
                    <div class="table table-active">
                        <table class="table table-condensed table-striped">
                            <?php foreach($currencies as $currency => $exchange ): ?>
                                <tr>
                                    <td></td>
                                    <td><?= $currency ?></td>
                                    <?php foreach($exchange as $ex => $val): ?>
                                        <td><?= $ex.' '.$val ?></td>
                                    <?php endforeach; ?>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                </div>
            </div>

        </div>
        <!-- /Content wrapper -->
    </div>
    <!-- /Container -->
</section>
<!-- /App section -->

<!-- Contact section -->
<section class="section is-medium is-darkest" id="contact">
    <!-- Container -->
    <div class="container">
        <!-- Divider -->
        <div class="divider is-centered"></div>
        <!-- Title & subtitle -->
        <h2 class="title is-light is-semibold has-text-centered is-spaced"><?= $this->lang->line('contact') ?></h2>
        <h4 class="subtitle is-6 is-light has-text-centered is-compact"><?= $this->lang->line('dialog_contact') ?></h4>

        <!-- Content wrapper -->
        <div class="content-wrapper is-large">
            <div class="columns">
                <div class="column is-8 is-offset-2">
                    <!-- Contact icons -->
                    <div class="contact-icons">
                        <!-- Phone -->
                        <a href="tel:+237678656032" class="contact-icon" data-aos="fade-up" data-aos-delay="100" data-aos-offset="200" data-aos-easing="ease-out-quart">
                            <img class="is-phone" src="<?= base_url('resources/front/');?>assets/images/icons/phone.svg" alt="Phone">
                        </a>
                        <a href="https://web.facebook.com/vouriinc/" class="contact-icon" data-aos="fade-up" data-aos-delay="100" data-aos-offset="200" data-aos-easing="ease-out-quart" target="_blank">
                            <img class="is-phone" src="<?= base_url('resources/front/');?>assets/images/icons/social/messenger.svg" alt="Facebook">
                        </a>
                        <!-- Twitter -->
                        <a href="https://twitter.com/VouriP" class="contact-icon" data-aos="fade-up" data-aos-delay="300" data-aos-offset="200" data-aos-easing="ease-out-quart">
                            <img class="is-phone" src="<?= base_url('resources/front/');?>assets/images/icons/social/twitter.svg" alt="Twitter">
                        </a>
                    </div>
                </div>
            </div>

            <div class="columns">
                <div class="column is-6 is-offset-3">
                    <!-- Form -->
                    <?= form_open(site_url('home/contactus'),
                        ['class' => 'form-default', 'role' => 'form'] )?>
                    <input type="hidden" name="subject" value="Inquiry from Vouri payments">
                    <!-- Field -->
                        <div class="control-material is-secondary">
                            <input class="material-input " type="text" name="name" id="name" required>
                            <input type="hidden" name="subject" value="Service Contact/Inquiry">
                            <span class="material-highlight"></span>
                            <span class="bar"></span>
                            <label><?= $this->lang->line('name') ?></label>
                        </div>
                        <!-- Field -->
                        <div class="control-material is-secondary">
                            <input class="material-input " type="email" name="email" id="email" required>
                            <span class="material-highlight"></span>
                            <span class="bar"></span>
                            <label><?= $this->lang->line('email') ?></label>
                        </div>
                    <!-- Field -->
                        <div class="control-material is-secondary">
                            <input class="material-input " type="tel" name="phone" id="phone">
                            <span class="material-highlight"></span>
                            <span class="bar"></span>
                            <label><?= $this->lang->line('phone') ?></label>
                        </div>
                        <!-- Field -->
                        <div class="control-material is-secondary">
                            <textarea rows="3" name="message" id="message" required></textarea>
                            <span class="material-highlight"></span>
                            <span class="bar"></span>
                            <label><?= $this->lang->line('message') ?></label>
                        </div>

                        <!-- Submit -->
                        <div class="has-text-centered">
                            <button class="button is-button k-button k-primary raised has-gradient is-fat is-bold is-submit">
                                <span class="text"><?= $this->lang->line('send') ?> message</span>
                                <span class="front-gradient"></span>
                            </button>
                        </div>
                    </form>
                    <!-- /Form -->
                </div>
            </div>


        </div>
        <!-- Content wrapper -->
    </div>
    <!-- Container -->
</section>
<!-- Contact section -->
</div>