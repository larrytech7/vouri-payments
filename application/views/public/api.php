
<div class="hero-body has-text-centered">
    <div class="columns">
        <div class="column is-12 is-offset-2">
            <div class="token-image">
                <img src="<?= base_url('resources/front/');?>assets/images/api.svg" alt="">
            </div>
            <h1 class="title is-2 is-light is-semibold is-spaced main-title"><?= $this->lang->line('api_title')?></h1>
            <h2 class="subtitle is-5 is-light is-thin">
                <?= $this->lang->line('api_desc')?>
            </h2>
            <!-- CTA -->
            <p>
                <span id="typed-text"></span>
            </p>
        </div>
    </div>
</div>
</section>

<section class="section is-medium">
    <div class="container">

        <!-- Content wrapper -->
        <div class="content-wrapper">

            <div class="columns is-vcentered">
                <!-- Feature content -->
                <div class="column is-5 is-offset-1">
                    <div class="side-feature-content">

                        <h3 class="title is-4 is-light"><?= $this->lang->line('subscription_api')?></h3>
                        <div class="divider"></div>
                        <p class="is-light"><?= $this->lang->line('subscription_api_desc') ?></p>
                        <br>
                        <a href="<?= site_url('home/apiCheckout')?>" class="button k-button k-primary raised has-gradient is-bold">
                            <span class="text"><?= $this->lang->line('get_subscribe') ?></span>
                            <span class="front-gradient"></span>
                        </a>
                    </div>
                </div>

                <!-- Feature image -->
                <div class="column is-7">
                    <img class="side-feature" src="<?= base_url('resources/front/')?>assets/images/code.svg" alt="CODE">
                </div>
            </div>

            <div class="columns is-vcentered">
                <!-- Feature image -->
                <div class="column is-5">
                    <img class="side-feature" src="<?= base_url('resources/front/')?>assets/images/code.svg" alt="CODE">
                </div>

                <!-- Feature content -->
                <div class="column is-7">
                    <div class="side-feature-content">

                        <h3 class="title is-4 is-light"><?= $this->lang->line('programmatic_api') ?></h3>
                        <div class="divider"></div>
                        <p class="is-light"><?= $this->lang->line('programmatic_api_desc') ?></p>
                        <!--
                        <h4 class="title is-4">
                            <?= $this->lang->line('api_features') ?>
                            Available in <div class="countdown" data-countdown-date="11/30/2019" data-countdown-label="show"></div>
                        </h4>
                        -->
                        <br>
                        <a href="<?= site_url('')?>" class="button k-button k-primary raised has-gradient is-bold">
                            <span class="text"><?= $this->lang->line('tryit') ?></span>
                            <span class="front-gradient"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- App section -->
<section class="section is-medium is-dark">
    <!-- Container -->
    <div class="container">
        <!-- Divider -->
        <div class="divider is-centered"></div>
        <!-- Title -->
        <h2 class="title is-light is-semibold has-text-centered is-spaced"><?= $this->lang->line('joinus') ?></h2>
        <h4 class="subtitle is-6 is-light has-text-centered is-compact"><?= $this->lang->line('joinus_desc') ?></h4>

        <!-- Content wrapper -->
        <div class="content-wrapper is-large">

            <!-- Row -->
            <div class="columns is-vcentered">
                <div class="column is-12">
                    <!-- Side feature -->
                    <div class="side-feature-content">
                        <div class="cta-wrapper has-text-centered">
                            <a href="<?= site_url('home')?>#contact" class="button k-button k-primary raised has-gradient is-fat is-bold is-centered">
                                <span class="text"><?= $this->lang->line('contact') ?></span>
                                <span class="front-gradient"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /Content wrapper -->
    </div>
    <!-- /Container -->
</section>
<!-- /App section -->