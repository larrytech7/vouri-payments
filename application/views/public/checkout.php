<!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="robots" content="index, follow">
    <meta name="description" content="Online mobile and web money transfer and payment aggregation">
    <meta name="keywords" content="payment, money, transfer, aggregation, online, website, ux, ui,web, design, developer, api, business, corporate, finance, online payment">
    <meta name="author" content="Vouri Inc">

    <title><?= $this->lang->line('app_name') ?> - <?= $this->lang->line('app_short_description') ?></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<body>
<!-- Pageloader -->
<div class="container">
    <!-- Landing page Hero -->
    <section class="">
        <div class="row">
            <div class="col col-md-4">

            </div>
            <div class="col col-md-4 align-self-center align-items-center p-5 border border-primary">
                    <a href="#" data-toggle="modal" data-target="#checkoutModal" class="btn btn-secondary btn-lg active rounded-0 d-inline" role="button" aria-pressed="false"
                       data-subscription="3"
                       data-name="MyApp Name"
                       data-description="Service description"
                       data-amount="100"
                       data-currency="CFA"
                       data-api-key="1b4QBL6Edr1OI15SNOMqoujnWU"
                       data-callback="<?= site_url()?>" >
                        Checkout with Vouri
                    </a>
                <div class="col col-md-4">

                </div>
            </div>
        </div>
    </section>

</div>

    <!-- SCRIPTS -->
    <!-- Core js -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>
    <script>
        setup = function(){
            formModal = '<div class="modal" id="checkoutModal" tabindex="-1" role="dialog" aria-labelledby="checkoutModalCenterTitle" aria-hidden="true"> <div class="modal-dialog modal-dialog-centered" role="document"> <div class="modal-content"><div class="d-flex d-none justify-content-center"> <div id="v-load-spin" class="spinner-grow text-success" role="status"><span class="sr-only">Loading...</span> </div><h5 id="status-text" class="text-danger"></h5> </div> <div class="modal-header"> <h5 class="modal-title text-center" id="checkoutModalCenterTitle">Checkout on <span id="app-title"></span></h5> <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button> </div> <div class="modal-body"> <div class="container-fluid"> <div class="row"> <div class="col-md-12 ml-auto"> <div class="align-items-center p-2 bd-highlight align-middle"> <form role="form" id="vouri-payform"> <p class="text-uppercase" id="payDialogConfig" data-toggle="tooltip" data-placement="right" data-title="Error! Either charge amount, currency or API keys are invalid. Contact an administrator for help."> Pay with :</p> <div class="custom-control custom-radio custom-control-inline m-2"> <input type="radio" id="v-payment-momo" name="payment_mode" aria-selected="true" class="custom-control-input selected" value="MMO" checked> <label class="text-uppercase custom-control-label badge badge-success" for="v-payment-momo">MTN Mobile Money <img class="hero-logo" src="https://vouriinc.com/resources/front/assets/images/clients/momo.png" alt="Momo" width="30" height="30"> </label> </div> <div class="custom-control custom-radio custom-control-inline m-2"> <input type="radio" id="v-payment-mode" name="payment_mode" class="custom-control-input" value="OMO"> <label class="text-uppercase custom-control-label badge badge-info" for="v-payment-mode">Orange Money &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img class="hero-logo" src="https://vouriinc.com/resources/front/assets/images/clients/omo.jpg" alt="momo" width="30" height="30"> </label> </div> <hr class="text-primary"> <div class="form-group"> <h1 id="amountHolder" class="text-center"><span id="amountToPay">0</span><sup><span class="currencyTag text-wrap badge badge-success"></span></sup></h1> </div> <div class="form-group"> <label for="v-name">Name</label> <input type="text" data-toggle="tooltip" data-placement="right" data-title="Enter your full name" class="form-control" id="v-name" name="customer_name" placeholder="Client name" required> </div> <div class="form-group"> <label class="form-check-label" for="v-phone">Phone Number</label> <input type="tel" data-toggle="tooltip" data-placement="right" data-title="Phone Number must be in the correct format" class="form-control" min="10" id="v-phone" name="client_phone" placeholder="with country code (e.g 67....)" required> </div> <div class="text-center"> <button type="submit" id="v-button-confirm" class="btn btn-primary btn-lg rounded-0">CONFIRM </button> </div></form> </div> </div> </div> </div> </div><div class="modal-footer"><p class="pull-right">Powered by <a href="https://vouriinc.com">Vouri payments</a></p></div> </div> </div> </div>';
            $('body').append(formModal);
            $("#app-title").html($('[data-name]').attr('data-name'));
        };
        $(document).ready(function(){
            setup();
            $("#v-load-spin").toggleClass("spinner-grow");
            $('#amountToPay').html(Number($('[data-amount]').attr('data-amount')));
            $('.currencyTag').html($('[data-currency]').attr('data-currency'));
            $('#v-button-confirm').click(function(e){
                //validate all fields and submit request
                e.preventDefault();
                $(this).attr('disabled', true);
                //data to submit
                apikey = ($('[data-api-key]').attr('data-api-key'));
                amount = Number($('[data-amount]').attr('data-amount'));
                currency = $('[data-currency]').attr('data-currency');
                client_name = $('#v-name').val();
                client_phone = $('#v-phone').val();
                console.log(apikey, amount, currency);
                if(apikey.length < 9){
                    //toast API key invalid
                    $('#payDialogConfig').tooltip('show', {
                        trigger : 'click',
                        title : "API key appears to be invalid"
                    });
                    return;
                }
                if(( isNaN(amount))){
                    //Toast Amount is invalid
                    $('#payDialogConfig').tooltip('show', {
                        trigger : 'click',
                        title : 'Amount is invalid. Check again'
                    });
                    return;
                }
                if(currency.length > 4){
                    //Toast invalid currency
                    $('#payDialogConfig').tooltip('show', {
                        trigger : 'click',
                        title : 'Currency set for this subscription is invalid. Please correct this or contact an administrator.'
                    });
                    return;
                }
                if(client_name.length < 2){
                    //Toast name appears to be invalid
                    $('#v-name').tooltip('show', {
                        trigger : 'click'
                    });
                    $('#v-name').focus();
                    return;
                }

                payData = {
                    'data-api-key' : apikey,
                    'data-callback' : $('[data-callback]').attr('data-callback'),
                    'data-subscription' : $('[data-subscription]').attr('data-subscription'),
                    'data-name' : $('[data-name]').attr('data-name'),
                    'data-description' : $('[data-description]').attr('data-description'),
                    'data-amount' : amount,
                    'data-currency' : currency,
                    'v-phone' : client_phone,
                    'v-name' : client_name,
                    'v-payment-method' : $('#v-payment-mode').prop('checked') ? $('#v-payment-mode').val() : $('#v-payment-momo').val()
                };
                //make request to confirm payment
                $.ajax({
                        url : 'http://localhost/vouri/api/paysubscribe',
                        headers  : {
                            'X-API-KEY' : apikey
                        },
                        username : 'vouri@restadmin',
                        password  : 'vouri@rest-admin-123',
                        method : 'post',
                        data : payData,
                        dataType : 'json',
                        beforeSend : function(){
                            //apply loading
                            $("#v-button-confirm").attr('disabled', 'disabled');
                            $('#status-text').html('Please confirm the payment on your mobile');
                            $("#v-load-spin").toggleClass("spinner-grow");
                        }
                    }).done(function(data){
                        //parse response then redirect to callback with response
                        console.log('RESPONSE Data', data);
                        $('#status-text').html(data.message);
                        if(data.status_code == 200){
                            window.location = data.transaction.subscription_callback+"?"+ $.param(data.transaction)
                        }
                }).fail(function(error){
                        console.log('Error Response', error);
                        console.log('Error ResponseText', error.statusText);
                        if(error.status == 500 || error.responseJSON != undefined){
                            //payment error
                            alert('Error! A payment Error occurred. You failed to validate payment within 2 minutes. Please try again.\nDetails:\n'+error.statusText)
                        }
                        $('#status-text').html(error.statusText+". Transaction error. Please try again.");
                    }).always(function(){
                        //enable send button,stop loading
                        $("#v-button-confirm").prop('disabled', false);
                        $("#v-load-spin").toggleClass("spinner-grow");
                    })
            })
        })
    </script>

</body>
</html>