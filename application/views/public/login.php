
<!-- Hero Image and Title -->
<div class="hero-body">
    <div class="container">
        <div class="columns is-vcentered">
            <!-- Landing page Title -->
            <div class="column is-5 ico-countdown">
                <div class="ico-card animated preFadeInUp fadeInUp">
                <h4 class="title is-4 is-light is-semibold is-spaced main-title">
                    <?= $this->lang->line('login_heading')?>

                </h4>
                <!-- Form -->
                <?= form_open(site_url('login'), ['class' => 'form-default'
                    , 'role' => 'form'] )?>
                <?= validation_errors()?>
                <!-- Field -->
                <div class="control-material is-secondary">
                    <input class="material-input " type="email" name="email" id="email" required>
                    <span class="material-highlight"></span>
                    <span class="bar"></span>
                    <label><?= $this->lang->line('email') ?></label>
                </div>
                <!-- Field -->
                <div class="control-material is-secondary">
                    <input class="material-input " type="password" name="password" id="password" required>
                    <span class="material-highlight"></span>
                    <span class="bar"></span>
                    <label><?= $this->lang->line('password') ?></label>
                </div>
                <!-- Submit -->
                <div class="has-text-centered">
                    <button class="button is-button k-button k-secondary raised has-gradient is-fat is-bold is-submit">
                        <span class="text"><?= $this->lang->line('login') ?></span>
                        <span class="front-gradient"></span>
                    </button>
                </div>
                <div class="has-text-centered">
                    <br>
                    <span class="title is-5 is-light">
                            <?= $this->lang->line('no_account_text') ?>
                        <a href="<?= site_url('home/signup')?>" class="has-text-primary"> <?= $this->lang->line('signup') ?></a>
                    </span>
                    <br>
                    <span class="title is-5 is-light">
                        <a href="#" class="has-text-success" id="recover_account" title="Recover your account password"><?= $this->lang->line('lost_password') ?></a>
                    </span>
                </div>
                </form>
                <!-- CTA -->
                </div>
            </div>
            <!-- Hero image -->
            <div class="column is-7">
                <figure class="image">
                    <span id="typed-text"></span>
                    <img src="<?= base_url('resources/front/');?>assets/images/illustrations/token-lock.svg" alt="">
                </figure>
            </div>
        </div>
    </div>
</div>

</section>