<!-- PAGE TITLE -->
<section class="slice-xl slice--offset-top has-bg-cover bg-size-cover" style="background-image: url('<?= base_url()?>/resources/assets/images/backgrounds/slider/img-14.jpg'); background-position: bottom;">
    <span class="mask bg-base-1 alpha-5" id="particles-js"></span>
    <div class="text-center">
        <div class="container">
            <div class="py-5">
                <div class="fluid-paragraph text-center">
                    <h3 class="heading heading-xl strong-400 text-normal c-white">
                        <?= $this->lang->line('fees_title') ?>
                    </h3>
                    <p class="lead line-height-1_8 strong-400 c-white mt-3">
                        <?= $this->lang->line('fees_desc') ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="slice-lg sct-color-1">
    <div class="container">

        <div class="row-wrapper">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-12">
                    <div class="icon-block icon-block--style-3 icon-block--style-3-v2">
                        <span class="icon-number bg-base-1 strong-600">1</span>
                        <div class="icon-block-content">
                            <h3 class="heading heading-6 strong-600">
                                <?= $this->lang->line('fees') ?>
                            </h3>
                            <p class="mt-3">
                                <?= $this->lang->line('fee_desc') ?>
                            </p>
                        </div>
                    </div>
                    <div class="icon-block icon-block--style-3 icon-block--style-3-v2">
                        <span class="icon-number bg-base-1 strong-600">2</span>
                        <div class="icon-block-content">
                            <h3 class="heading heading-6 strong-600">
                                <?= $this->lang->line('limits') ?>
                            </h3>
                            <p class="mt-3">
                                <?= $this->lang->line('limits_desc') ?>
                            </p>
                        </div>
                    </div>
                    <div class="icon-block icon-block--style-3 icon-block--style-3-v2">
                        <span class="icon-number bg-base-1 strong-600">3</span>
                        <div class="icon-block-content">
                            <h3 class="heading heading-6 strong-600">
                                <?= $this->lang->line('processing') ?>
                            </h3>
                            <p class="mt-3">
                                <?= $this->lang->line('processing_desc') ?>

                            </p>
                        </div>
                    </div>
                    <div class="icon-block icon-block--style-3 icon-block--style-3-v2">
                        <span class="icon-number bg-base-1 strong-600">4</span>
                        <div class="icon-block-content">
                            <h3 class="heading heading-6 strong-600">
                                <?= $this->lang->line('refund') ?>
                            </h3>
                            <p class="mt-3">
                                <?= $this->lang->line('refund_desc') ?>

                            </p>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</section>

<section class="slice-lg sct-color-4 bg-base-1">
    <div class="container text-center">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title section-title--style-1 text-center mb-4">
                    <h3 class="section-title-inner heading-2 text-normal">
                        <?= $this->lang->line('joinus') ?>
                    </h3>
                    <span class="section-title-delimiter clearfix d-none"></span>
                </div>

                <span class="clearfix"></span>

                <div class="fluid-paragraph fluid-paragraph-sm text-center">
                    <p class="text-lg line-height-1_8 c-light">
                        <?= $this->lang->line('joinus_desc') ?>
                    </p>

                    <a href="#!" class="btn btn-styled btn-base-1 btn-circle mt-5"><?= $this->lang->line('usechat') ?></a>
                </div>
            </div>
        </div>
    </div>
</section>
