<!-- Popup -->
<div class=" mfp-hide contact-pop" style="padding-top: 30px; padding-bottom: 20px; margin-left: 35%;margin-right: 35%;margin-top: 50px;margin-bottom: 200px;">
    <div class="row" style="background-color : white">
        <div class="col-md-12">
            <div class="form-header text-center">
                <div class="form-header-icon">
                    <i class="icon ion-send"></i>
                </div>
            </div>
            <p><?= $this->lang->line('exit_page_message') ?>
            </p>
            <h1 class="text-center">
                <span><?= $this->lang->line('contact') ?></span>
            </h1>
            <?= form_open(site_url('home/contactus'), ['class' => 'form-default', 'role' => 'form'] )?>
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label for="email"><?= $this->lang->line('email') ?></label>
                        <div class="input-group">
                            <input type="email" name="email" id="email" class="form-control form-control-lg" placeholder="<?= $this->lang->line('email_text') ?>" required>
                                    <span class="input-group-addon">
                                        <i class="ion ion-email"></i>
                                    </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group has-feedback">
                        <label for="phone"><?= $this->lang->line('phone') ?></label>
                        <div class="input-group">
                            <input type="tel" name="phone" id="phone" class="form-control form-control-lg" placeholder="<?= $this->lang->line('phone_text') ?>" required>
                                    <span class="input-group-addon">
                                        <i class="ion ion-android-phone-portrait"></i>
                                    </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group has-feedback">
                        <label for="subject"><?= $this->lang->line('subject') ?></label>
                        <div class="input-group">
                            <input type="text" name="subject" id="subject" class="form-control form-control-lg" required>
                                        <span class="input-group-addon">
                                            <i class="ion ion-document-text"></i>
                                        </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group has-feedback">
                        <label for="message"><?= $this->lang->line('message') ?></label>
                        <textarea rows="5" cols="10" name="message" id="message" class="form-control form-control-lg" required></textarea>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-styled btn-lg btn-base-1 mb-4"> <?= $this->lang->line('send') ?></button>
            </form>

        </div>
    </div>
</div>

<!-- FOOTER -->
<footer id="footer" class="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-7">
                    <div class="col">
                        <img src="<?= base_url('resources/')?>assets/images/logo/logo-trans.png">
                        <span class="clearfix"></span>
                        <p class="mt-3">
                            <?= $this->lang->line('app_long_desc') ?>
                        </p>

                    </div>
                </div>

                <div class="col-lg-2">
                    <div class="col">
                        <h4 class="heading heading-xs strong-600 text-uppercase mb-1">
                            Company
                        </h4>

                        <ul class="footer-links">
                            <li><a href="<?= site_url('about') ?>" title="About us">About</a></li>
                            <li><a href="<?= site_url('api') ?>" title="API Integrations">API</a></li>
                            <li><a href="<?= site_url('privacy') ?>" title="Privacy policies">Privacy</a></li>
                            <li><a href="<?= site_url('terms') ?>" title="Terms and conditions">Terms and Conditions</a></li>
                            <li><a href="<?= site_url('fees') ?>" title="Service fees"><?= $this->lang->line('service_fees') ?></a></li>
                            <li><a href="javascript:;" data-fancybox data-src="#fancybox_contact" title="Service fees"><?= $this->lang->line('contact') ?></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="col">
                        <h4 class="heading heading-xs strong-600 text-uppercase mb-1">
                            <?= $this->lang->line('followus') ?>
                        </h4>
                        <?= $this->lang->line('followus_text') ?>
                        <ul class="social-media social-media--style-1-v4">
                            <li>
                                <a href="https://web.facebook.com/vouriinc/" class="facebook" target="_blank" data-toggle="tooltip" data-original-title="Facebook">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://twitter.com/VouriP" class="twitter" target="_blank" data-toggle="tooltip" data-original-title="Twitter">
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
            <div class="col-md-12 copyright">
                <p class="align-center align-items-center">
                    Copyright &copy; <?= date('Y')?>
                    <a href="https://vouriinc.com" target="_parent">
                        <strong><?= $this->lang->line('app_name')?></strong>
                    </a> -
                    <?= $this->lang->line('rights_reserved') ?>
                </p>
            </div>

        </div>
    </div>
</footer>
</div>
</div>
</div><!-- END: st-pusher -->
</div><!-- END: st-container -->
</div><!-- END: body-wrap -->


<div class="d-none">
    <div id="fancybox_makepayment">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="form-header text-center">
                    <div class="form-header-icon">
                        <i class="icon ion-send"></i>
                    </div>
                </div>
                <p>
                    <?= $this->lang->line('payment_dialog_title') ?>
                </p>
                <!--
                <p>Service currently not available but will soon be restored. We are working to fix this. Thanks for your patience.</p>
                -->
                <h1 class="text-center">
                    <span id="amounttag"><?= isset($paymentrequest) ? $paymentrequest[0]->amount : 0.0 ?></span>
                    <span class="badge badge-success" id="currencytag"><?= isset($paymentrequest) ? $paymentrequest[0]->currency : 'USD' ?></span>
                </h1>
                <?= form_open(site_url('sendfunds'), ['class' => 'form-default', 'role' => 'form'] )?>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="amounttosend"><?= $this->lang->line('payment_dialog_amount') ?> </label>  <i class="ion icon-info pull-right" data-toggle="tooltip" data-placement="top" title="How much do you wish to send? Enter the amount in the currency you will indicate below"></i>
                                <div class="input-group">
                                    <input type="number" name="amounttosend" id="amounttosend" min="0" step="1" value="<?= isset($paymentrequest) ? $paymentrequest[0]->amount : 0.0 ?>"
                                           class="form-control form-control-lg" required>
                                        <span class="input-group-addon">
                                            <i class="ion icon-ecommerce-dollar"></i>
                                        </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group has-feedback">
                                <label for="currency"><?= $this->lang->line('payment_dialog_currency') ?></label>  <i class="icon-info pull-right" data-toggle="tooltip" title="Select the currency you wish to use for the transaction. Note that conversion fees may apply for currencies other than USD"></i>
                                <select id="currency" name="currency" class="form-control form-control-lg">
                                    <option selected><?= isset($paymentrequest) ? $paymentrequest[0]->currency : 'USD' ?></option>
                                    <option value="CAD">CAD - Canadian Dollar</option>
                                    <option value="EUR">EUR - Euro</option>
                                    <option value="GBP">GBP - British Pound</option>
                                    <option value="XAF">XAF - Central African Franc</option>
                                    <option value="NGN">NGN - Naira</option>
                                    <option value="AED">AED - Emirates Dirham</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group has-feedback">
                                <label for="sendfrom"><?= $this->lang->line('payment_dialog_gateway') ?></label>  <i class="icon-info pull-right" data-toggle="tooltip" title="Select the payment gateway you wish to use for the transaction."></i>
                                <select id="sendfrom" name="sendfrom" class="form-control form-control-lg">
                                    <option value="STR">STRIPE</option>
                                    <option value="MMO">MTN MOBILE MONEY</option>
                                    <option value="VSA" selected>VISA/MASTERCARD </option>
                                    <option value="SQR">SQUARE</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-styled btn-lg btn-block btn-base-1 mt-4"> <?= $this->lang->line('payment_dialog_go') ?></button>
                </form>
            </div>

            <div class="col-md-2"></div>
        </div>
    </div>
</div>

<div class="d-none">
    <div id="fancybox_contact" style="width: 30%">
        <div class="row">
            <div class="col-md-12">
                <div class="form-header text-center">
                    <div class="form-header-icon">
                        <i class="icon ion-send"></i>
                    </div>
                </div>
                <p><?= $this->lang->line('dialog_contact') ?>
                </p>
                <h1 class="text-center">
                    <span><?= $this->lang->line('contact') ?></span>
                </h1>
                <?= form_open(site_url('home/contactus'), ['class' => 'form-default', 'role' => 'form'] )?>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label for="email"><?= $this->lang->line('email') ?></label>
                            <div class="input-group">
                                <input type="email" name="email" id="email" class="form-control form-control-lg" placeholder="<?= $this->lang->line('email_text') ?>" required>
                                    <span class="input-group-addon">
                                        <i class="ion ion-email"></i>
                                    </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group has-feedback">
                            <label for="phone"><?= $this->lang->line('phone') ?></label>
                            <div class="input-group">
                                <input type="tel" name="phone" id="phone" class="form-control form-control-lg" placeholder="<?= $this->lang->line('phone_text') ?>" required>
                                    <span class="input-group-addon">
                                        <i class="ion ion-android-phone-portrait"></i>
                                    </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group has-feedback">
                            <label for="subject"><?= $this->lang->line('subject') ?></label>
                            <div class="input-group">
                                <input type="text" name="subject" id="subject" class="form-control form-control-lg" required>
                                        <span class="input-group-addon">
                                            <i class="ion ion-document-text"></i>
                                        </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group has-feedback">
                            <label for="message"><?= $this->lang->line('message') ?></label>
                            <textarea rows="5" cols="10" name="message" id="message" class="form-control form-control-lg" required></textarea>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-styled btn-lg btn-base-1 mt-4"> <?= $this->lang->line('send') ?></button>
                </form>

            </div>
        </div>
    </div>
</div>

<div class="d-none">
    <div id="fancybox_recover" >
        <div class="row">
            <div class="col-md-12">
                <div class="form-header text-center">
                    <div class="form-header-icon">
                        <i class="icon ion-locked"></i>
                    </div>
                </div>
                <h1 class="text-center">
                    <span><?= $this->lang->line('reset_password') ?></span>
                </h1>
                <?= form_open(site_url('home/reset'), ['class' => 'form-default', 'role' => 'form'] )?>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <div class="input-group">
                                <input type="email" name="email" id="email" class="form-control form-control-lg" placeholder="<?= $this->lang->line('email_text') ?>" required>
                                    <span class="input-group-addon">
                                        <i class="ion ion-email"></i>
                                    </span>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="align-items-lg-center">
                    <button type="submit" class="pull-left btn btn-styled btn-lg btn-base-1 mt-4"> <?= $this->lang->line('recover') ?></button>
                </p>
                </form>

            </div>
        </div>
    </div>
</div>

<div class="d-none">
    <div id="fancybox_verify_card" style="width: 30%">
        <div class="row">
            <div class="col-md-12">
                <div class="form-header text-center">
                    <div class="form-header-icon">
                        <i class="icon ion-send"></i>
                    </div>
                </div>
                <h4 class="text-center">
                    <span><?= $this->lang->line('verify_card_note') ?></span>
                </h4>
                <?= form_open(site_url('home/verifycard'), ['class' => 'form-default', 'role' => 'form'] )?>
                <div class="row">
                    <div class="col-12">
                        <div class="">
                            <div class="text-center">
                                <script
                                    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                    data-key="pk_live_0qKu51snUVSF5pnlUlgpOLr9"
                                    data-amount="1"
                                    data-name="Vouri Inc - Payments. Card verification"
                                    data-description="Democratizing Online Payments and remittances"
                                    data-image="<?= base_url('resources/assets/images/logo/logo-trans.png')?>"
                                    data-locale="auto"
                                    data-panel-label="<?= $this->lang->line('continue')?>"
                                    data-label="<?= $this->lang->line('verify_button_label')?>"
                                    data-currency="<?= 'USD' ?>">
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="d-none">
    <div id="fancybox_requestpay">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="form-header text-center">
                    <div class="form-header-icon">
                        <i class="icon ion-send"></i>
                    </div>
                </div>
                <p>
                    Send a payment request and get paid easily and fast.
                </p>
                <h1 class="text-center"><span id="">0.0</span><span class="badge badge-success">USD</span></h1>
                <?= form_open(site_url('paymentrequest'), ['class' => 'form-default'
                    , 'role' => 'form'] )?>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label for="amounttosend">Amount requested </label>
                            <div class="input-group">
                                <input type="number" name="amount" id="amounttosend" min="0" step="1" class="form-control form-control-lg" required>
                                    <span class="input-group-addon">
                                        <i class="ion icon-ecommerce-dollar"></i>
                                    </span>
                            </div>
                        </div>
                    </div>
                    </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label for="email">Email </label>
                            <div class="input-group">
                                <input type="email" name="email" class="form-control form-control-lg" required>
                                    <span class="input-group-addon">
                                        <i class="ion icon-basic-mail"></i>
                                    </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label for="phone">Phone number</label>
                            <div class="input-group">
                                <input type="number" name="phone" min="0" step="1" class="form-control form-control-lg" required>
                                    <span class="input-group-addon">
                                        <i class="ion ion-android-phone-portrait"></i>
                                    </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group has-feedback">
                            <label for="currency">Currency</label>
                            <select id="currency" name="currency" class="form-control form-control-lg">
                                <option value="USD" selected>USD - US Dollar</option>
                                <option value="CAD">CAD - Canadian Dollar</option>
                                <option value="EUR">EUR - Euro</option>
                                <option value="GBP">GBP - British Pound</option>
                                <option value="XAF">XAF - Central African Franc</option>
                                <option value="NGN">NGN - Naira</option>
                                <option value="AED">AED - Saudi Dirham</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group has-feedback">
                            <label for="sendfrom">To receive via</label>
                            <select id="sendfrom" name="gateway" class="form-control form-control-lg">
                                <option value="STR">STRIPE</option>
                                <option value="MMO">MTN MOBILE MONEY</option>
                                <option value="OMO">ORANGE MONEY</option>
                                <option value="EWY">eWAY</option>
                                <option value="SQR">SQUARE</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <textarea name="message" class="form-control form-control-lg" placeholder="Any reason for requesting this payment?"></textarea>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-styled btn-lg btn-block btn-base-1 mt-4"> Send Request</button>
                </form>

            </div>

            <div class="col-md-2"></div>
        </div>
    </div>
</div>

<!-- SCRIPTS -->

<!-- Core -->
<script src="<?= base_url('resources/');?>assets/vendor/jquery/jquery.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/popper/popper.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="<?= base_url('resources/');?>assets/js/slidebar/slidebar.js"></script>
<script src="<?= base_url('resources/');?>assets/js/classie.js"></script>
<script src="<?= base_url('resources/');?>assets/js/run.js"></script>
<script src="<?= base_url('resources/');?>assets/js/particles.js"></script>
<script src="<?= base_url('resources/');?>assets/js/app.js"></script>
<script  src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script  src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>


<!-- Bootstrap Extensions -->
<script src="<?= base_url('resources/');?>assets/vendor/bootstrap-notify/bootstrap-growl.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/scrollpos-styler/scrollpos-styler.js"></script>

<!-- Plugins: Sorted A-Z -->
<script src="<?= base_url('resources/');?>assets/vendor/adaptive-backgrounds/adaptive-backgrounds.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/countdown/js/jquery.countdown.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/dropzone/dropzone.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/easy-pie-chart/jquery.easypiechart.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/fancybox/js/jquery.fancybox.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/flatpickr/flatpickr.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/flip/flip.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/footer-reveal/footer-reveal.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/gradientify/jquery.gradientify.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/headroom/headroom.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/headroom/jquery.headroom.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/input-mask/input-mask.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/instafeed/instafeed.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/milestone-counter/jquery.countTo.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/nouislider/js/nouislider.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/paraxify/paraxify.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/select2/js/select2.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/sticky-kit/sticky-kit.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/swiper/js/swiper.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/textarea-autosize/autosize.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/typeahead/typeahead.bundle.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/typed/typed.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/vide/vide.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/viewport-checker/viewportchecker.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/wow/wow.min.js"></script>

<!-- Isotope -->
<script src="<?= base_url('resources/');?>assets/vendor/isotope/isotope.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/imagesloaded/imagesloaded.pkgd.min.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-112810447-2"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-112810447-2');
</script>

<!-- WhatsHelp.io widget -->
<script type="text/javascript">
    (function () {
        var options = {
            facebook: "248027269468693", // Facebook page ID
            whatsapp: "+237 506694857", // WhatsApp number
            company_logo_url: "<?= base_url()?>/resources/assets/images/logo/logo.png", // URL of company logo (png, jpg, gif)
            greeting_message: "Hello, how may we help? Send and receive funds fast and easy with Vouri Inc. ", // Text of greeting message
            call_to_action: "Chat with us", // Call to action
            button_color: "#FF6550", // Color of button
            position: "left", // Position may be 'right' or 'left'
            order: "facebook,whatsapp", // Order of buttons
        };
        var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
        s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
    })();

</script>
<?php if($this->session->info) : ?>
    <script>
        toastr.success('<?= $this->encryption->decrypt($this->session->info) ?>', {
            timeOut : 10000,//10 seconds visibility
            position : 'right-top',
            size : 'lg'
        })
    </script>
<?php endif; ?>
<?php if($this->session->error) : ?>
    <script>
        toastr.error('<?= $this->encryption->decrypt($this->session->error) ?>', {
            timeOut : 10000,//10 seconds visibility
            position : 'right-top',
            size : 'lg'
        })
    </script>
<?php endif; ?>
<?php if(isset($paymentrequest)): ?>
<script>
    $('a.btn_send').click();
</script>
<?php endif; ?>
<script>
    var mouseX = 0;
    var mouseY = 0;
    $(window).on('mousemove', function(e){
        mouseX = e.clientX;
        mouseY = e.clientY;
    });
    $(document).mouseleave(function(){
        firstTime = localStorage.getItem("leaving");
        if(mouseY < 100 && firstTime != "YES" ){
            //show dialog
            $.magnificPopup.open({
                items: {
                    src: $('.contact-pop'), // can be a HTML string, jQuery object, or CSS selector
                    type: 'inline',
                    closeBtnInside : true
                }
            });
            localStorage.setItem("leaving", "YES");
        }
    });
</script>
<!-- App JS -->
<script src="<?= base_url('resources/');?>assets/js/boomerang.min.js"></script>

</body>
</html>