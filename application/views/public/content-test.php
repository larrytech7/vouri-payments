
<section class="swiper-js-container background-image-holder" data-holder-type="hero" data-holder-offset="">
    <div class="swiper-container" data-swiper-autoplay="false" data-swiper-effect="" data-swiper-items="0" data-swiper-space-between="0">
        <div class="swiper-wrapper">
            <div class="swiper-slide" data-swiper-autoplay="">
                <!-- Slide -->
                <div class="slice sct-color-1 holder-item holder-item-dark has-bg-cover bg-size-contain" style="background-image: url(<?= base_url('resources/assets/images/backgrounds/'.$cover)?>); background-position: center center; -webkit-background-size:  cover;background-size:  cover;">
                    <span class="mask bg-base-2 alpha-6"></span>
                    <div class="container container-sm d-flex align-items-center">
                        <div class="col">
                            <div class="row py-3 text-center justify-content-center">
                                <div class="col-12 col-md-10">
                                    <h4 class="heading heading-sm text-light text-uppercase mb-0"><?= $this->lang->line('home_page_title_one') ?></h4>
                                    <span class="sd-1 sd-sm sd-thick-3px sd-center"></span>
                                    <h2 class="heading heading-2 strong-600 mt-3 text-light animated" data-animation-in="fadeIn" data-animation-delay="600">
                                        <?= $this->lang->line('home_page_title_one_desc') ?>
                                        <span id="type_1" class="type-this text-primary"
                                              data-type-this="<?= $this->lang->line('home_page_type_1') ?>"></span>
                                    </h2>
                                    <h2 class="heading-2 strong text-white">Currently under maintenance. We will be back Live In a moment </h2>  <div class="countdown text-white" data-countdown-date="07/30/2019" data-countdown-label="show"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Slide -->
            <div class="swiper-slide" data-swiper-autoplay="">
                <!-- Slide -->
                <div class="slice sct-color-1 holder-item holder-item-dark has-bg-cover bg-size-contain" style="background-image: url(<?= base_url('resources/assets/images/backgrounds/'.$cover2)?>); background-position: center 100% ; background-size: cover;">
                    <span class="mask bg-base-2 alpha-6"></span>
                    <div class="container container-sm d-flex align-items-center">
                        <div class="col">
                            <div class="row py-3 text-center justify-content-center">
                                <div class="col-12 col-md-10">
                                    <h4 class="heading heading-sm text-light text-uppercase mb-0"><?= $this->lang->line('home_page_title_two') ?></h4>
                                    <span class="sd-1 sd-sm sd-thick-3px sd-center"></span>
                                    <h2 class="heading heading-2 strong-600 mt-3 text-light animated" data-animation-in="fadeIn" data-animation-delay="400">
                                        <?= $this->lang->line('home_page_title_two_desc') ?>
                                        <span id="type_2" class="type-this c-base-1" data-type-this=" <?= $this->lang->line('home_page_type_2') ?> "></span>
                                    </h2>
                                    <a href="<?= site_url('home/api')?>" class="btn btn-styled btn-base-1 btn-circle px-4 mt-5">Integrate the API</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>

        <!-- Add Arrows -->
        <div class="swiper-button swiper-button-next"></div>
        <div class="swiper-button swiper-button-prev"></div>
    </div>
</section>

<section class="slice sct-color-2 border-bottom">
    <div class="container">
        <div class="row align-items-md-center">
            <div class="col-md-6 col-sm-12 col-12">
                <span class="mask bg-base-1 alpha-3" id="particles-js"></span>
                <img src="<?= base_url('resources')?>/assets/images/backgrounds/slider/img-37.jpg" class="img-fluid img-center">
            </div>

            <div class="col-md-6 col-sm-12 col-12">
                <div class="px-3 py-3 text-center text-lg-left">
                    <h3 class="heading heading-2 strong-500">
                        <?= $this->lang->line('f4') ?>
                    </h3>
                    <p class="text-lg line-height-1_8 mt-4">
                        <?= $this->lang->line('f4t') ?>
                    </p>
                    <!--
                    <a href="#" class="link link-sm link--style-2 strong-600 mt-2">
                        <i class="ion-ios-arrow-right"></i> See DEMO
                    </a>
                    -->

                    <span class="space-xs-md"></span>

                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="milestone-counter text-center b-md-right">
                                <div class="milestone-count c-gray-dark strong-600" data-from="0" data-to="0" data-speed="3000" data-refresh-interval="100"></div>
                                <h4 class="milestone-info heading-6 c-gray-light text-normal"><?= $this->lang->line('f5') ?></h4>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="milestone-counter text-center">
                                <div class="milestone-count c-gray-dark strong-600" data-from="0" data-to="150" data-speed="5000" data-refresh-interval="50"></div>
                                <h4 class="milestone-info heading-6 c-gray-light text-normal"><?= $this->lang->line('f6') ?></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="slice sct-color-1">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="text-center">
                    <div class="heading heading-2 strong-600 line-height-1_8">
                        <?= $this->lang->line('how') ?>
                    </div>

                    <span class="clearfix"></span>

                    <div class="fluid-paragraph fluid-paragraph-sm">
                        <p>
                            <?= $this->lang->line('how_desc') ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="sct-color-1" id="sct_timeline">
    <div class="container">
        <div class="timeline">
            <div class="timeline-block">
                <span class="timeline-img"></span>
                <div class="timeline-content">
                    <div class="">
                        <img src="<?= base_url('resources/assets/images/backgrounds/step1.PNG')?>" class="img-fluid">
                        <div class="timeline-body">
                           <a href="#" class="heading heading-4 d-block strong-500 mt-1"><?= $this->lang->line('sender_info') ?></a>
                            <p class="mt-3">
                                <?= $this->lang->line('step1') ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="timeline-block">
                <span class="timeline-img"></span>
                <div class="timeline-content">
                    <div class="">
                        <img src="<?= base_url('resources/assets/images/backgrounds/step2.PNG')?>" class="img-fluid">
                        <div class="timeline-body">
                            <a href="#" class="heading heading-4 d-block strong-500 mt-1"><?= $this->lang->line('receiver_info') ?></a>
                            <p class="mt-3">
                                <?= $this->lang->line('step2') ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="timeline-block">
                <span class="timeline-img"></span>
                <div class="timeline-content">
                        <img class="card-img" src="<?= base_url('resources/assets/images/backgrounds/step3.PNG')?>" alt="Card image">
                           <div class="timeline-body">
                                <a class="heading heading-4 d-block strong-500 mt-1"><?= $this->lang->line('make_payment') ?></a>
                                <p class="mt-3"><?= $this->lang->line('step3') ?></p>
                            </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="slice sct-color-2 border-top border-bottom">
    <div class="container">
        <div class="row align-items-center cols-xs-space cols-md-space cols-sm-space">
            <div class="col-lg-5">
                <div class="text-center text-lg-left">
                    <h3 class="heading heading-2 strong-500">
                        <?= $this->lang->line('vouri_exchange') ?>
                    </h3>
                    <footer class="blockquote-footer">
                        <cite title="Source Title"><?= $this->lang->line('source_title') ?></cite>
                    </footer>
                    <p class="mt-4">
                        <?= $this->lang->line('exchange_text') ?>
                    </p>
                </div>
            </div>

            <div class="col-lg-6 ml-lg-auto">
                <div class="table table-active">
                    <table class="table table-condensed table-striped">
                        <?php foreach($currencies as $currency => $exchange ): ?>
                            <tr>
                                <td></td>
                                <td><?= $currency ?></td>
                                <?php foreach($exchange as $ex => $val): ?>
                                <td><?= $ex.' '.$val ?></td>
                                <?php endforeach; ?>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<!--
<section class="slice sct-color-1 border-top">
    <div class="container">
        <div class="swiper-js-container">
            <div class="swiper-container" data-swiper-items="6" data-swiper-space-between="25" data-swiper-md-items="4" data-swiper-md-space-between="25" data-swiper-sm-items="3" data-swiper-sm-space-between="20" data-swiper-xs-items="2" data-swiper-sm-space-between="20">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="client-logo client-logo--style-4">
                            <div class="view view-first">
                                <a href="#">
                                    <img src="<?= base_url('resources/')?>assets/images/prv/clients/client-1.png" alt="">
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="swiper-slide">
                        <div class="client-logo client-logo--style-4">
                            <div class="view view-first">
                                <a href="#">
                                    <img src="<?= base_url('resources/')?>assets/images/prv/clients/client-2.png" alt="">
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="swiper-slide">
                        <div class="client-logo client-logo--style-4">
                            <div class="view view-first">
                                <a href="#">
                                    <img src="<?= base_url('resources/')?>assets/images/prv/clients/client-3.png" alt="">
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="swiper-slide">
                        <div class="client-logo client-logo--style-4">
                            <div class="view view-first">
                                <a href="#">
                                    <img src="<?= base_url('resources/')?>assets/images/prv/clients/client-4.png" alt="">
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="swiper-slide">
                        <div class="client-logo client-logo--style-4">
                            <div class="view view-first">
                                <a href="#">
                                    <img src="<?= base_url('resources/')?>assets/images/prv/clients/client-5.png" alt="">
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="swiper-slide">
                        <div class="client-logo client-logo--style-4">
                            <div class="view view-first">
                                <a href="#">
                                    <img src="<?= base_url('resources/')?>assets/images/prv/clients/client-6.png" alt="">
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="swiper-slide">
                        <div class="client-logo client-logo--style-4">
                            <div class="view view-first">
                                <a href="#">
                                    <img src="<?= base_url('resources/')?>assets/images/prv/clients/client-7.png" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <span class="delimiter delimiter--style-1 mt-5 mb-5"></span>

    </div>

</section>
-->
