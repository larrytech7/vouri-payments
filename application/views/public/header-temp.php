<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<meta name="robots" content="index, follow">
	<meta name="description" content="Online mobile and web money transfer and payment aggregation">
	<meta name="keywords" content="payment, money, transfer, aggregation, online, website, ux, ui,web, design, developer, api, business, corporate, finance, online payment">
	<meta name="author" content="Vouri Inc">

	<title><?= $this->lang->line('app_name') ?> - <?= $this->lang->line('app_short_description') ?></title>

	<!-- Bootstrap -->
	<link rel="stylesheet" href="<?= base_url('resources/');?>assets/vendor/bootstrap/css/bootstrap.min.css" type="text/css">

	<!-- Fonts -->
	<link href="<?= base_url('resources/');?>fonts.googleapis.com/cssfb86.css?family=Nunito:400,600,700,800|Roboto:400,500,700" rel="stylesheet">

	<!-- Plugins -->
	<link rel="stylesheet" href="<?= base_url('resources/');?>assets/vendor/swiper/css/swiper.min.css">
	<link rel="stylesheet" href="<?= base_url('resources/');?>assets/vendor/hamburgers/hamburgers.min.css" type="text/css">
	<link rel="stylesheet" href="<?= base_url('resources/');?>assets/vendor/animate/animate.min.css" type="text/css">
	<link rel="stylesheet" href="<?= base_url('resources/');?>assets/vendor/fancybox/css/jquery.fancybox.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">

	<!-- Icons -->
	<link rel="stylesheet" href="<?= base_url('resources/');?>assets/fonts/font-awesome/css/font-awesome.min.css" type="text/css">
	<link rel="stylesheet" href="<?= base_url('resources/');?>assets/fonts/ionicons/css/ionicons.min.css" type="text/css">
	<link rel="stylesheet" href="<?= base_url('resources/');?>assets/fonts/line-icons/line-icons.css" type="text/css">
	<link rel="stylesheet" href="<?= base_url('resources/');?>assets/fonts/line-icons-pro/line-icons-pro.css" type="text/css">

	<!-- Linea Icons -->
	<link rel="stylesheet" href="<?= base_url('resources/');?>assets/fonts/linea/arrows/linea-icons.css" type="text/css">
	<link rel="stylesheet" href="<?= base_url('resources/');?>assets/fonts/linea/basic/linea-icons.css" type="text/css">
	<link rel="stylesheet" href="<?= base_url('resources/');?>assets/fonts/linea/ecommerce/linea-icons.css" type="text/css">
	<link rel="stylesheet" href="<?= base_url('resources/');?>assets/fonts/linea/software/linea-icons.css" type="text/css">

	<!-- Global style (main) -->
	<link id="stylesheet" type="text/css" href="<?= base_url('resources/');?>assets/css/boomerang.min.css" rel="stylesheet" media="screen">

	<!-- Custom style - Remove if not necessary -->
	<link type="text/css" href="<?= base_url('resources/');?>assets/css/custom-style.css" rel="stylesheet">

	<!-- Favicon -->
	<link href="<?= base_url('resources/');?>assets/images/favicon.png" rel="icon" type="image/png">
	<link rel="apple-touch-icon" sizes="57x57" href="<?= base_url('resources/');?>assets/images/iconvouri/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?= base_url('resources/');?>assets/images/iconvouri/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?= base_url('resources/');?>assets/images/iconvouri/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?= base_url('resources/');?>assets/images/iconvouri/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?= base_url('resources/');?>assets/images/iconvouri/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?= base_url('resources/');?>assets/images/iconvouri/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?= base_url('resources/');?>assets/images/iconvouri/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?= base_url('resources/');?>assets/images/iconvouri/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?= base_url('resources/');?>assets/images/iconvouri/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?= base_url('resources/');?>assets/images/iconvouri/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?= base_url('resources/');?>assets/images/iconvouri/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?= base_url('resources/');?>assets/images/iconvouri/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('resources/');?>assets/images/iconvouri/favicon-16x16.png">
	<link rel="manifest" href="<?= base_url('resources/');?>assets/images/iconvouri/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?= base_url('resources/');?>assets/images/iconvouri/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

</head>
<body>


<!-- MAIN WRAPPER -->
<div class="body body-wrap" data-template-mode="cards">
	<div id="st-container" class="st-container">
		<div class="st-pusher">
			<div class="st-content">
				<div class="st-content-inner">
					<div class="header">
						<!-- Top Bar -->
						<div class="top-navbar">
							<div class="container">
								<div class="row">
									<div class="col-md-6">
										<a class="navbar-brand" href="<?= site_url()?>">
											<img src="<?= base_url('resources')?>/assets/images/logo/logo-trans.png" class="" alt="Vouri Inc Payments">
										</a>
									</div>
									<div class="col-md-6">
										<nav class="top-navbar-menu">

											<ul class="top-menu">
												<?php if($this->session->loggedin): ?>
													<li><a href="<?= site_url('admin') ?>"><?= $this->lang->line('menu_dashboard') ?></a></li>
												<?php else: ?>
													<li><a href="<?= site_url('login') ?>"><?= $this->lang->line('login') ?></a></li>
													<li><a href="<?= site_url('signup') ?>"><?= $this->lang->line('signup') ?> </a></li>
												<?php endif; ?>
                                                <li>
                                                    <a href="javascript:;" id="contact_us" data-fancybox data-src="#fancybox_contact" title="Service fees"><?= $this->lang->line('contact') ?></a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;" id="verify_card" data-fancybox data-src="#fancybox_verify_card" title="Verify Your card compatibility with Vouri">
														<?= $this->lang->line('verify_card') ?>
													</a>
                                                </li>
												<li class="aux-languages dropdown">
													<a href="#">
														<img src="<?= base_url('resources/')?>assets/images/icons/flags/ro.png">
													</a>
													<ul id="auxLanguages" class="sub-menu">
														<li>
															<a href="<?= site_url('home/en')?>">
																<span class="language <?= $this->session->userdata('lang') == 'english' ? 'language-active' : '' ?>">English</span>
															</a>
														</li>
														<li>
															<a href="<?= site_url('home/fre')?>">
																<span class="language <?= $this->session->userdata('lang') == 'french' ? 'language-active' : '' ?>">French</span>
															</a>
														</li>
													</ul>
												</li>
											</ul>

										</nav>
									</div>
								</div>
							</div>
						</div>
					</div>