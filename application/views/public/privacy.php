<!-- PAGE TITLE -->
<section class="slice-xl slice--offset-top has-bg-cover bg-size-cover" style="background-image: url('<?= base_url()?>/resources/assets/images/backgrounds/bridge-bg.jpg'); background-position: bottom;">
    <span class="mask bg-base-1 alpha-5"></span>
    <div class="text-center">
        <div class="container">
            <div class="py-5">
                <div class="fluid-paragraph text-center">
                    <h3 class="heading heading-xl strong-400 text-normal c-white">
                        <?= $this->lang->line('privacy_title') ?>
                    </h3>
                    <p class="lead line-height-1_8 strong-400 c-white mt-3">
                        <?= $this->lang->line('privacy_desc') ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="slice-lg sct-color-1">
    <div class="container">

        <div class="row-wrapper">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-12">
                    <p>
                        <?= $this->lang->line('privacy_extended') ?> &raquo; <a href="https://stripe.com/us/privacy">HERE</a>
                    </p>
                </div>
                <div class="col-lg-12">
                    <div class="icon-block icon-block--style-3 icon-block--style-3-v2">
                        <span class="icon-number bg-base-1 strong-600">0</span>
                        <div class="icon-block-content">
                            <h3 class="heading heading-6 strong-600">
                                <?= $this->lang->line('privacy_preamble') ?>
                            </h3>
                            <p class="mt-3">
                                <?= $this->lang->line('privacy_preamble_desc') ?>
                            </p>
                        </div>
                    </div>
                    <div class="icon-block icon-block--style-3 icon-block--style-3-v2">
                        <span class="icon-number bg-base-1 strong-600">1</span>
                        <div class="icon-block-content">
                            <h3 class="heading heading-6 strong-600">
                                <?= $this->lang->line('privacy_1') ?>
                            </h3>
                            <p class="mt-3">
                                <?= $this->lang->line('privacy_1_desc') ?>
                            </p>
                        </div>
                    </div>
                    <div class="icon-block icon-block--style-3 icon-block--style-3-v2">
                        <span class="icon-number bg-base-1 strong-600">2</span>
                        <div class="icon-block-content">
                            <h3 class="heading heading-6 strong-600">
                                <?= $this->lang->line('privacy_2') ?>
                            </h3>
                            <p class="mt-3">
                                <?= $this->lang->line('privacy_2_desc') ?>
                            </p>
                        </div>
                    </div>
                    <div class="icon-block icon-block--style-3 icon-block--style-3-v2">
                        <span class="icon-number bg-base-1 strong-600">3</span>
                        <div class="icon-block-content">
                            <h3 class="heading heading-6 strong-600">
                                <?= $this->lang->line('privacy_3') ?>
                            </h3>
                            <p class="mt-3">
                                <?= $this->lang->line('privacy_3_desc') ?>
                            </p>
                        </div>
                    </div>
                    <div class="icon-block icon-block--style-3 icon-block--style-3-v2">
                        <span class="icon-number bg-base-1 strong-600">4</span>
                        <div class="icon-block-content">
                            <h3 class="heading heading-6 strong-600">
                                <?= $this->lang->line('privacy_4') ?>
                            </h3>
                            <p class="mt-3">
                                <?= $this->lang->line('privacy_4_desc') ?>
                            </p>
                        </div>
                    </div>
                    <div class="icon-block icon-block--style-3 icon-block--style-3-v2">
                        <span class="icon-number bg-base-1 strong-600">5</span>
                        <div class="icon-block-content">
                            <h3 class="heading heading-6 strong-600">
                                <?= $this->lang->line('privacy_5') ?>
                            </h3>
                            <p class="mt-3">
                                <?= $this->lang->line('privacy_5_desc') ?>
                            </p>
                        </div>
                    </div>
                    <div class="icon-block icon-block--style-3 icon-block--style-3-v2">
                        <span class="icon-number bg-base-1 strong-600">6</span>
                        <div class="icon-block-content">
                            <h3 class="heading heading-6 strong-600">
                                <?= $this->lang->line('privacy_6') ?>
                            </h3>
                            <p class="mt-3">
                                <?= $this->lang->line('privacy_6_desc') ?>
                            </p>
                        </div>
                    </div>
                    <div class="icon-block icon-block--style-3 icon-block--style-3-v2">
                        <span class="icon-number bg-base-1 strong-600">7</span>
                        <div class="icon-block-content">
                            <h3 class="heading heading-6 strong-600">
                                <?= $this->lang->line('privacy_7') ?>
                            </h3>
                            <p class="mt-3">
                                <?= $this->lang->line('privacy_7_desc') ?>
                            </p>
                        </div>
                    </div>
                    <div class="icon-block icon-block--style-3 icon-block--style-3-v2">
                        <span class="icon-number bg-base-1 strong-600">8</span>
                        <div class="icon-block-content">
                            <h3 class="heading heading-6 strong-600">
                                <?= $this->lang->line('privacy_8') ?>
                            </h3>
                            <p class="mt-3">
                                <?= $this->lang->line('privacy_8_desc') ?>
                            </p>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</section>