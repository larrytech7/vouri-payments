<!-- Popup -->
<div class=" mfp-hide contact-pop" style="padding-top: 30px; padding-bottom: 20px; margin-left: 35%;margin-right: 35%;margin-top: 50px;margin-bottom: 200px;">
    <div class="row" style="background-color : white">
        <div class="col-md-12">
            <div class="form-header text-center">
                <div class="form-header-icon">
                    <i class="icon ion-send"></i>
                </div>
            </div>
            <p><?= $this->lang->line('exit_page_message') ?>
            </p>
            <h1 class="text-center">
                <span><?= $this->lang->line('contact') ?></span>
            </h1>
            <?= form_open(site_url('home/contactus'), ['class' => 'form-default', 'role' => 'form'] )?>
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label for="email"><?= $this->lang->line('email') ?></label>
                        <div class="input-group">
                            <input type="email" name="email" id="email" class="form-control form-control-lg" placeholder="<?= $this->lang->line('email_text') ?>" required>
                                    <span class="input-group-addon">
                                        <i class="ion ion-email"></i>
                                    </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group has-feedback">
                        <label for="phone"><?= $this->lang->line('phone') ?></label>
                        <div class="input-group">
                            <input type="tel" name="phone" id="phone" class="form-control form-control-lg" placeholder="<?= $this->lang->line('phone_text') ?>" required>
                                    <span class="input-group-addon">
                                        <i class="ion ion-android-phone-portrait"></i>
                                    </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group has-feedback">
                        <label for="subject"><?= $this->lang->line('subject') ?></label>
                        <div class="input-group">
                            <input type="text" name="subject" id="subject" class="form-control form-control-lg" required>
                                        <span class="input-group-addon">
                                            <i class="ion ion-document-text"></i>
                                        </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group has-feedback">
                        <label for="message"><?= $this->lang->line('message') ?></label>
                        <textarea rows="5" cols="10" name="message" id="message" class="form-control form-control-lg" required></textarea>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-styled btn-lg btn-base-1 mb-4"> <?= $this->lang->line('send') ?></button>
            </form>

        </div>
    </div>
</div>
<footer class="krypton-footer">
    <div class="container">
        <!-- Logo -->
        <div class="footer-logo">
            <a href="#">
                <img class="rotating" src="<?= base_url('resources/front/');?>assets/images/logo/krypton-gradient.svg" alt="">
                <div class="brand-name"><?= $this->lang->line('app_name') ?></div>
                <div class="brand-subtitle is-white"><?= $this->lang->line('app_long_desc') ?></div>
            </a>
        </div>

        <!-- Columns -->
        <div class="columns footer-columns is-vcentered">
            <div class="column is-4">
                <!-- Links group -->
                <ul class="footer-links">
                    <li>
                        <a href="<?= site_url('about') ?>" title="About us"><?= $this->lang->line('about') ?></a>
                    </li>

                    <li>
                        <a href="<?= site_url('api') ?>" title="API Integrations"><?= $this->lang->line('api') ?></a>
                    </li>

                    <li>
                        <a href="<?= site_url('privacy') ?>" title="Privacy policies"><?= $this->lang->line('privacy') ?></a>
                    </li>
                </ul>
            </div>
            <!-- Newsletter -->
            <div class="column is-4">
                <div class="subscribe-block">
                    <form>
                        <!-- Field -->
                        <div class="control">
                            <!-- Special input -->
                            <input class="krypton-subscribe-input" type="email" name="email" placeholder="">
                            <button class="subscribe-button">
                                <span>Subscribe</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- Links group -->
            <div class="column is-4">
                <ul class="footer-links">
                    <li>
                        <a href="<?= site_url('terms') ?>" title="Terms and conditions"><?= $this->lang->line('terms') ?></a>
                    </li>

                    <li>
                        <a href="<?= site_url('fees') ?>" title="Service fees"><?= $this->lang->line('service_fees') ?></a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- Copyright -->
        <p class="k-copyright">&copy; 2019 - <?= date('Y') ?> | Vouri Inc. All Rights Reserved</p>
        <br>
    </div>

    <!-- Absolute image -->
    <img class="solar-system" src="<?= base_url('resources/front/');?>assets/images/bg/solar.svg" alt="">
</footer>

<div class="modal" id="modal-makepayment">
    <div class="modal-background"></div>
    <div class="modal-content">
        <div class="box is-vcentered">
            <div class="col-md-12">
                <h2 class="title is-2 has-text-centered"><?= $this->lang->line('f1')?></h2>
                <h2 class="title is-2 has-text-centered has-text-success">
                    <span id="amounttag"><?= isset($paymentrequest) ? $paymentrequest[0]->amount : 0.0 ?></span>
                    <span class="badge badge-success" id="currencytag"><?= isset($paymentrequest) ? $paymentrequest[0]->currency : 'USD' ?></span>
                </h2>
                <?= form_open(site_url('sendfunds'), ['class' => 'form-default', 'role' => 'form'] )?>
                    <div class="columns is-vcentered">
                        <div class="column is-2"></div>
                        <div class="column is-8">
                            <div class="control-material is-primary is-large">
                                <input type="number" name="amounttosend" id="amounttosend" min="0" step="any" value="<?= isset($paymentrequest) ? $paymentrequest[0]->amount : 0.0 ?>"
                                       class="material-input input is-primary has-text-dark" required/>
                                <span class="material-highlight"></span>
                                <span class="bar"></span>
                                <label><?= $this->lang->line('payment_dialog_amount') ?></label>
                            </div>
                            <div class="control has-text-centered">
                                <div class=" select is-primary is-medium">
                                    <select id="currency" name="currency" class="material-input input is-primary">
                                        <option disabled selected><?= $this->lang->line('payment_dialog_currency') ?></option>
                                        <option ><?= isset($paymentrequest) ? $paymentrequest[0]->currency : 'USD' ?></option>
                                        <option value="CAD">CAD - Canadian Dollar</option>
                                        <option value="EUR">EUR - Euro</option>
                                        <option value="GBP">GBP - British Pound</option>
                                        <option value="XAF">XAF - Central African Franc</option>
                                        <option value="NGN">NGN - Naira</option>
                                        <option value="AED">AED - Emirates Dirham</option>
                                        <option value="BTC">BTC - BITCOIN</option>
                                    </select>
                                    <span class="material-highlight"></span>
                                    <span class="bar"></span>
                                </div>
                            </div>
                            <br>
                            <span class="clear-fix"></span>
                            <div class="control has-text-centered">
                                <div class="select is-primary is-medium has-text-centered">
                                    <select id="sendfrom" name="sendfrom" class="material-input input is-primary">
                                        <option disabled selected><?= $this->lang->line('payment_dialog_gateway') ?></option>
                                        <option value="MMO">MTN MOBILE MONEY</option>
                                        <option value="OMO">ORANGE MONEY</option>
                                        <option value="VSA">VISA/MASTERCARD </option>
                                        <option value="BTC">Bitcoin Wallet</option>
                                    </select>
                                    <span class="material-highlight"></span>
                                    <span class="bar"></span>
                                </div>
                            </div>
                        </div>
                        <div class="column is-2"></div>
                    </div>
                <br>
                <span class="clear-fix"></span>
                    <div class="has-text-centered">
                        <button class="button is-button k-button k-secondary raised has-gradient is-fat is-bold is-submit" type="submit">
                            <span class="text"><?= $this->lang->line('payment_dialog_go') ?></span>
                            <span class="front-gradient"></span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <button class="modal-close is-large" aria-label="close"></button>
</div>

<div class="modal">
    <div id="fancybox_contact" style="width: 30%">
        <div class="row">
            <div class="col-md-12">
                <div class="form-header text-center">
                    <div class="form-header-icon">
                        <i class="icon ion-send"></i>
                    </div>
                </div>
                <p><?= $this->lang->line('dialog_contact') ?>
                </p>
                <h1 class="text-center">
                    <span><?= $this->lang->line('contact') ?></span>
                </h1>
                <?= form_open(site_url('home/contactus'), ['class' => 'form-default', 'role' => 'form'] )?>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label for="email"><?= $this->lang->line('email') ?></label>
                            <div class="input-group">
                                <input type="email" name="email" id="email" class="form-control form-control-lg" placeholder="<?= $this->lang->line('email_text') ?>" required>
                                    <span class="input-group-addon">
                                        <i class="ion ion-email"></i>
                                    </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group has-feedback">
                            <label for="phone"><?= $this->lang->line('phone') ?></label>
                            <div class="input-group">
                                <input type="tel" name="phone" id="phone" class="form-control form-control-lg" placeholder="<?= $this->lang->line('phone_text') ?>" required>
                                    <span class="input-group-addon">
                                        <i class="ion ion-android-phone-portrait"></i>
                                    </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group has-feedback">
                            <label for="subject"><?= $this->lang->line('subject') ?></label>
                            <div class="input-group">
                                <input type="text" name="subject" id="subject" class="form-control form-control-lg" required>
                                        <span class="input-group-addon">
                                            <i class="ion ion-document-text"></i>
                                        </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group has-feedback">
                            <label for="message"><?= $this->lang->line('message') ?></label>
                            <textarea rows="5" cols="10" name="message" id="message" class="form-control form-control-lg" required></textarea>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-styled btn-lg btn-base-1 mt-4"> <?= $this->lang->line('send') ?></button>
                </form>

            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal-reset-password" style="z-index: 999 !important;">
    <div class="modal-background"></div>
    <div class="modal-content" >
        <div class="box is-vcentered">
            <div class="col-md-8">
                <h4 class="title is-4 has-text-danger has-text-centered">
                    <span><?= $this->lang->line('reset_password') ?></span>
                </h4>
                <?= form_open(site_url('home/reset'), ['class' => 'form-default', 'role' => 'form'] )?>
                <div class="columns is-vcentered">
                    <div class="column is-12">
                        <div class="control-material is-primary">
                            <input class="material-input input is-primary has-text-dark" type="email" name="email" id="email" required>
                            <span class="material-highlight"></span>
                            <span class="bar"></span>
                            <label><?= $this->lang->line('email') ?></label>
                        </div>
                        <div class="has-text-centered">
                            <button class="button is-button k-button k-secondary raised has-gradient is-fat is-bold is-submit">
                                <span class="text"><?= $this->lang->line('recover') ?></span>
                                <span class="front-gradient"></span>
                            </button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    <button class="modal-close is-large" aria-label="close"></button>
</div>

<div class="modal">
    <div id="fancybox_verify_card" style="width: 30%">
        <div class="row">
            <div class="col-md-12">
                <div class="form-header text-center">
                    <div class="form-header-icon">
                        <i class="icon ion-send"></i>
                    </div>
                </div>
                <h4 class="text-center">
                    <span><?= $this->lang->line('verify_card_note') ?></span>
                </h4>
                <?= form_open(site_url('home/verifycard'), ['class' => 'form-default', 'role' => 'form'] )?>
                <div class="row">
                    <div class="col-12">
                        <div class="">
                            <div class="text-center">
                                <script
                                    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                    data-key="pk_live_0qKu51snUVSF5pnlUlgpOLr9"
                                    data-amount="1"
                                    data-name="Vouri Inc - Payments. Card verification"
                                    data-description="Democratizing Online Payments and remittances"
                                    data-image="<?= base_url('resources/assets/images/logo/logo-trans.png')?>"
                                    data-locale="auto"
                                    data-panel-label="<?= $this->lang->line('continue')?>"
                                    data-label="<?= $this->lang->line('verify_button_label')?>"
                                    data-currency="<?= 'USD' ?>">
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal-requestpayment">
    <div class="modal-background"></div>
    <div class="modal-content">
        <div class="box is-vcentered">
            <div class="col-md-8">
                <h4 class="title is-4 has-text-danger has-text-centered">
                    <span><?= $this->lang->line('payment_request_string') ?></span>
                </h4>
                <h1 class="title has-text-centered is-2 has-text-success">
                    <span id="amount_tag">0.0</span><span class="badge badge-success" id="currency_tag">USD</span>
                </h1>
                <?= form_open(site_url('paymentrequest'), ['class' => 'form-default'
                    , 'role' => 'form'] )?>
                <div class="columns is-vcentered">
                    <div class="column is-2"></div>
                    <div class="column  is-8">
                        <div class="control-material is-primary is-large">
                            <input class="material-input input is-primary has-text-dark" type="number" step="any" name="amount" id="amounttosend" required>
                            <span class="material-highlight"></span>
                            <span class="bar"></span>
                            <label><?= $this->lang->line('amount_requested') ?></label>
                        </div>
                        <div class="control-material is-primary is-large">
                            <label for="currency"><?= $this->lang->line('payment_dialog_currency') ?></label>
                            <select id="currency" name="currency" class="material-input input is-primary" required>
                                <option disabled selected><?= $this->lang->line('payment_dialog_currency') ?></option>
                                <option value="USD">USD - US Dollar</option>
                                <option value="CAD">CAD - Canadian Dollar</option>
                                <option value="EUR">EUR - Euro</option>
                                <option value="GBP">GBP - British Pound</option>
                                <option value="XAF">XAF - Central African Franc</option>
                                <option value="NGN">NGN - Naira</option>
                                <option value="AED">AED - Saudi Dirham</option>
                            </select>
                            <span class="material-highlight"></span>
                            <span class="bar"></span>
                        </div>
                        <div class="control-material is-primary is-large">
                            <input class="material-input input is-secondary has-text-dark" type="email" name="email" required>
                            <span class="material-highlight"></span>
                            <span class="bar"></span>
                            <label><?= $this->lang->line('email') ?></label>
                        </div>
                        <div class="control-material is-primary is-large">
                            <input class="material-input input is-primary has-text-dark" type="number" name="phone" min="0" step="1" required>
                            <span class="material-highlight"></span>
                            <span class="bar"></span>
                            <label><?= $this->lang->line('phone') ?></label>
                        </div>
                        <div class="control-material is-primary is-large">
                            <label for="sendfrom"><?= $this->lang->line('receiver_gateway') ?></label>
                            <select id="sendfrom" name="gateway" class="material-input input is-primary">
                                <option disabled selected><?= $this->lang->line('receiver_gateway') ?></option>
                                <option value="MMO">MTN MOBILE MONEY</option>
                                <option value="OMO">ORANGE MONEY</option>
                            </select>
                            <span class="material-highlight"></span>
                            <span class="bar"></span>
                        </div>
                        <div class="control-material is-secondary">
                            <textarea rows="3" cols="5" name="message" id="message" class="material-input has-text-dark"></textarea>
                            <span class="material-highlight"></span>
                            <span class="bar"></span>
                            <label><?= $this->lang->line('request_message') ?></label>
                        </div>
                    </div>
                    <div class="column is-2"></div>
                </div>
                <div class="has-text-centered">
                    <button class="button is-button k-button k-secondary raised has-gradient is-fat is-bold is-submit" type="submit">
                        <span class="text"><?= $this->lang->line('request') ?></span>
                        <span class="front-gradient"></span>
                    </button>
                </div>

                </form>

            </div>

            <div class="col-md-2"></div>
        </div>
    </div>
    <button class="modal-close is-large" aria-label="close"></button>
</div>

<!-- SCRIPTS -->
<!-- Core js -->
<script src="<?= base_url('resources/');?>assets/vendor/jquery/jquery.min.js"></script>
<script src="<?= base_url('resources/');?>assets/js/run.js"></script>

<script src="<?= base_url('resources/front/');?>assets/js/app.js"></script>
<!--script src="assets/js/particlesjs/particles.min.js"></script-->
<script src="<?= base_url('resources/front/');?>assets/js/aos/aos.js"></script>


<script src="<?= base_url('resources/front/');?>assets/js/timer.js"></script>
<script src="<?= base_url('resources/front/');?>assets/js/timeline.js"></script>
<script src="<?= base_url('resources/front/');?>assets/js/roadmap.js"></script>
<script src="<?= base_url('resources/front/');?>assets/js/main.js" defer="defer"></script>
<script src="https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>

<script src="<?= base_url('resources/');?>assets/vendor/popper/popper.min.js"></script>
<script  src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script  src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>

<script src="<?= base_url('resources/');?>assets/vendor/fancybox/js/jquery.fancybox.min.js"></script>
<script src="<?= base_url('resources/');?>assets/vendor/typeahead/typeahead.bundle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/typed.js@2.0.9"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-112810447-2"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-112810447-2');
</script>

<!-- WhatsHelp.io widget -->
<script type="text/javascript">
    (function () {
        var options = {
            facebook: "248027269468693", // Facebook page ID
            whatsapp: "+237 678656032", // WhatsApp number
            company_logo_url: "<?= base_url()?>/resources/front/assets/images/logo/logo.jpg", // URL of company logo (png, jpg, gif)
            greeting_message: "Hello, how may we help? Send and receive funds fast and easy with Vouri Inc. ", // Text of greeting message
            call_to_action: "Chat with us", // Call to action
            button_color: "#FF6550", // Color of button
            position: "left", // Position may be 'right' or 'left'
            order: "facebook,whatsapp" // Order of buttons
        };
        var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
        s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
    })();

</script> <!-- -->
<?php if($this->session->info) : ?>
    <script>
        toastr.info('<?= $this->encryption->decrypt($this->session->info) ?>', {
            timeOut : 20000,//10 seconds visibility
            position : 'right-top',
            size : 'lg'
        })
    </script>
<?php endif; ?>
<?php if($this->session->error) : ?>
    <script>
        toastr.error('<?= $this->encryption->decrypt($this->session->error) ?>', {
            timeOut : 20000,//20 seconds visibility
            position : 'right-top',
            size : 'lg'
        })
    </script>
<?php endif; ?>

<?php if($this->session->success) : ?>
    <script>
        toastr.success('<?= $this->encryption->decrypt($this->session->success) ?>', {
            timeOut : 20000,//20 seconds visibility
            position : 'right-top',
            size : 'lg'
        })
    </script>
<?php endif; ?>
<?php if(isset($paymentrequest)): ?>
<script>
    $('a#btn-send').click();
</script>
<?php endif; ?>
<script>
    var mouseX = 0;
    var mouseY = 0;
    $(window).on('mousemove', function(e){
        mouseX = e.clientX;
        mouseY = e.clientY;
    });
    $(document).mouseleave(function(){
        firstTime = localStorage.getItem("leaving");
        if(mouseY < 100 && firstTime != "YES" ){
            //show dialog
            $.magnificPopup.open({
                items: {
                    src: $('.contact-pop'), // can be a HTML string, jQuery object, or CSS selector
                    type: 'inline',
                    closeBtnInside : true
                }
            });
            localStorage.setItem("leaving", "YES");
        }
    });
    $(document).ready(function(){
        $('#sendto').change();
        //set typed text
        var typed = new Typed("#typed-text", {
            strings : ["<?= $this->lang->line('home_page_type_1') ?>", "<?= $this->lang->line('home_page_type_2') ?>"],
            loop : true,
            backDelay : 3000,
            typeSpeed : 40
        });
        $('a#recover_account').click(function(e){
            e.preventDefault();
            $('#modal-reset-password').toggleClass('is-active');
        });
        $('#btn-send').click(function(e){
            e.preventDefault();
            $('#modal-makepayment').toggleClass('is-active');
        });
        $('#btn-receive').click(function(e){
            e.preventDefault();
            $('#modal-requestpayment').toggleClass('is-active');
        });

        $(document).on('click', '.modal-close', function(e){
            $(this).parent().toggleClass('is-active');
        });
        $(document).on('click', '.modal-background', function(e){
            $(this).parent().toggleClass('is-active');
        });
        $(document).on('change', '#amounttosend', function(e){
            $("#amount_tag").html($(this).val());
        });
        $(document).on('change', '#currency', function(e){
            $("#currency_tag").html($(this).val());
        });
    })

</script>

</body>
</html>
