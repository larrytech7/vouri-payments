<section class="slice-lg sct-color-1" style="background-image: url(<?= base_url('resources/assets/images/backgrounds/'.$cover)?>); background-position: center center; -webkit-background-size:  cover;background-size:  cover;">
    <div class="container">
        <div class="row cols-xs-space cols-md-space cols-sm-space">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div>
                    <div class="card no-border px-5 py-5">
                        <div class="card-header">
                            <h2 class="heading heading--base heading-3">
                                <?= $this->lang->line('password_reset_heading')?>
                            </h2>
                        </div>
                        <?= form_open(site_url('home/reset_password/'.($id ?? 'reset')), ['class' => 'form-default'
                            , 'role' => 'form'], ['userid' => $id ?? ''] )?>
                        <?php if($this->session->flashdata('error')):?>
                            <div class="alert alert-danger" data-dsimiss="alert">
                                <?= $this->encryption->decrypt($this->session->flashdata('error')) ?>
                                <button type="button" class="close pull-right" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php endif; ?>
                        <?php if($this->session->flashdata('info')):?>
                            <div class="alert alert-info" data-dsimiss="alert">
                                <?= $this->encryption->decrypt($this->session->flashdata('info')) ?>
                                <button type="button" class="close pull-right" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php endif; ?>
                        <?= validation_errors()?>

                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="password"><?= $this->lang->line('new_password') ?> </label>
                                    <div class="input-group">
                                        <input type="password" name="password" id="password" class="form-control form-control-lg" placeholder="<?= $this->lang->line('password') ?>" required>
                                        <span class="input-group-addon">
                                            <i class="ion ion-android-lock"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="password"><?= $this->lang->line('password_confirm') ?> </label>
                                    <div class="input-group">
                                        <input type="password" name="re-password" id="re-password" class="form-control form-control-lg" placeholder="<?= $this->lang->line('password') ?>" required>
                                        <span class="input-group-addon">
                                            <i class="ion ion-android-lock"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-styled btn-lg btn-block btn-base-1 mt-4"><i class="ion ion-log-in icon-lg"></i> <?= $this->lang->line('reset') ?></button>
                        </form><br>
                        <?= $this->lang->line('no_account_text') ?> <a href="<?= site_url('home/signup')?>" class="text-primary"> <?= $this->lang->line('signup') ?></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
            </div>
        </div>
    </div>
</section>