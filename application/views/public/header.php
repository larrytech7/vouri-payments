<!DOCTYPE html>
<html>
<head>
	<!-- Required meta tags always come first -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="robots" content="index, follow">
	<meta name="description" content="Online mobile and web money transfer and payment aggregation">
	<meta name="keywords" content="payment, money, transfer, aggregation, online, website, ux, ui,web, design, developer, api, business, corporate, finance, online payment">
	<meta name="author" content="Vouri Inc">

	<title><?= $this->lang->line('app_name') ?> - <?= $this->lang->line('app_short_description') ?></title>
	<link rel="icon" type="image/png" href="<?= base_url('resources/front/');?>assets/images/favicon.png" />

	<link href="https://fonts.googleapis.com/css?family=Cabin" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400" rel="stylesheet">

	<!--Core CSS -->
	<link rel="stylesheet" href="<?= base_url('resources/front/');?>assets/css/bulma.css">
	<link rel="stylesheet" href="<?= base_url('resources/front/');?>assets/css/core.css">
	<!--Libraries-->
	<link rel="stylesheet" href="<?= base_url('resources/front/');?>assets/fonts/cryptofont/css/cryptofont.min.css">
	<link rel="stylesheet" href="<?= base_url('resources/front/');?>assets/js/modalvideo/modal-video.min.css">
	<link rel="stylesheet" href="<?= base_url('resources/front/');?>assets/js/aos/aos.css">
	<link rel="stylesheet" href="<?= base_url('resources/');?>assets/vendor/animate/animate.min.css" type="text/css">

	<link rel="stylesheet" href="<?= base_url('resources/');?>assets/vendor/fancybox/css/jquery.fancybox.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">

	<script src="https://www.google.com/recaptcha/api.js" async defer></script>

	<style>
	.hero.is-transparent, .section.is-transparent, .section.is-darkest{
		background: #209cee !important;
	}
	.modal{
		z-index: 999 !important;
	}
</style>
</head>
<body>
<!-- Pageloader -->
<div class="pageloader is-theme"></div>
<div class="infraloader is-active"></div>
<div class="dark-wrapper">
	<!-- Landing page Hero -->
	<section class="hero is-fullheight is-transparent">
		<div class="hero-head">

			<!-- Cloned navbar -->
			<!-- Cloned navbar that comes in on scroll -->
			<nav id="navbar-clone" class="navbar is-fixed is-dark">
				<div class="container">
					<!-- Brand -->
					<div class="navbar-brand">
						<a href="<?= site_url('');?>" class="navbar-item">
							<img class="" src="<?= base_url('resources/front/');?>assets/images/logo/logo.jpg" alt="vouri Inc payments">
							<span class="brand-name">VOURI PAYMENTS</span>
						</a>
						<!-- Responsive toggle -->
                                <span class="navbar-burger burger" data-target="cloneNavbarMenu">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </span>
					</div>
					<!-- Menu -->
					<div id="cloneNavbarMenu" class="navbar-menu">
						<div class="navbar-end">
							<!-- Menu item -->
							<div class="navbar-item is-nav-link">
								<a class="is-centered-responsive" href="<?= site_url('#media');?>">Media</a>
							</div>
							<!-- Menu item -->
							<div class="navbar-item is-nav-link">
								<a class="is-centered-responsive" href="<?= site_url('#roadmap');?>">Roadmap</a>
							</div>
							<!-- Menu item
							<div class="navbar-item is-nav-link">
								<a class="is-centered-responsive" href="javascript:;" id="verify_card" data-fancybox data-src="#fancybox_verify_card" title="Verify Your card compatibility with Vouri">
									<?= $this->lang->line('verify_card') ?>
								</a>
							</div>
							<!-- Menu item -->
							<div class="navbar-item is-nav-link">
								<a class="is-centered-responsive" href="#contact" title="Service fees">
									<?= $this->lang->line('contact') ?>
								</a>
							</div>
							<?php if($this->session->loggedin): ?>
								<div class="navbar-item is-nav-link">
									<a class="is-centered-responsive" href="<?= site_url('admin') ?>">
										<img class="telegram" src="<?= base_url('resources/front/');?>assets/images/logo/telegram.svg" alt="">
										<?= $this->lang->line('menu_dashboard') ?>
									</a>
								</div>
							<?php else: ?>
								<!-- Sign up -->
								<div class="navbar-item is-nav-link">
									<a href="<?= site_url('login') ?>" class="button k-button k-primary raised has-gradient slanted">
										<span class="text"><?= $this->lang->line('login') ?></span>
										<span class="front-gradient"></span>
									</a>
								</div>
								<div class="navbar-item is-nav-link">
									<a href="<?= site_url('signup') ?>" class="button k-button k-primary raised has-gradient slanted">
										<span class="text"><?= $this->lang->line('signup') ?></span>
										<span class="front-gradient"></span>
									</a>
								</div>
							<?php endif; ?>
							<div class="navbar-item is-nav-link">
								<a href="#">
									<img src="<?= base_url('resources/')?>assets/images/icons/flags/ro.png">
								</a>
								<ul id="auxLanguages" class="sub-menu">
									<li>
										<a href="<?= site_url('home/en')?>">
											<span class="language <?= $this->session->userdata('lang') == 'english' ? 'language-active' : '' ?>">English</span>
										</a>
									</li>
									<li>
										<a href="<?= site_url('home/fre')?>">
											<span class="language <?= $this->session->userdata('lang') == 'french' ? 'language-active' : '' ?>">French</span>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</nav>
			<!-- /Cloned navbar -->
			<!-- Static navbar -->
			<!-- Static navbar -->
			<nav class="navbar is-light">
				<div class="container">
					<!-- Brand -->
					<div class="navbar-brand">
						<a href="<?= site_url('');?>" class="navbar-item">
							<img class="" src="<?= base_url('resources/front/');?>assets/images/logo/logo.jpg" alt="vouri Inc payments">
							<span class="brand-name">VOURI PAYMENTS</span>
						</a>
						<!-- Responsive toggle -->
                                <span class="navbar-burger burger" data-target="navbarMenu">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </span>
					</div>
					<!-- Menu -->
					<div id="navbarMenu" class="navbar-menu light-menu">
						<div class="navbar-end">
							<!-- Menu item -->
							<div class="navbar-item is-nav-link">
								<a class="is-centered-responsive" href="<?= site_url('#media');?>">Media</a>
							</div>
							<!-- Menu item -->
							<div class="navbar-item is-nav-link">
								<a class="is-centered-responsive" href="<?= site_url('#roadmap');?>">Roadmap</a>
							</div>
							<!-- Menu item
							<div class="navbar-item is-nav-link">
								<a class="is-centered-responsive" href="javascript:;" id="verify_card" data-fancybox data-src="#fancybox_verify_card" title="Verify Your card compatibility with Vouri">
									<?= $this->lang->line('verify_card') ?>
								</a>
							</div>
							<!-- Menu item -->
							<div class="navbar-item is-nav-link">
								<a class="is-centered-responsive" href="#contact" title="Service fees">
									<?= $this->lang->line('contact') ?>
								</a>
							</div>
							<?php if($this->session->loggedin): ?>
								<div class="navbar-item is-nav-link">
									<a class="is-centered-responsive" href="<?= site_url('admin') ?>">
										<img class="telegram" src="<?= base_url('resources/front/');?>assets/images/logo/telegram.svg" alt="">
										<?= $this->lang->line('menu_dashboard') ?>
									</a>
								</div>
							<?php else: ?>
								<!-- Sign up -->
								<div class="navbar-item is-nav-link">
									<a href="<?= site_url('login') ?>" class="is-centered-responsive">
										<span class="text"><?= $this->lang->line('login') ?></span>
										<span class="front-gradient"></span>
									</a>
								</div>
								<div class="navbar-item is-nav-link">
									<a href="<?= site_url('signup') ?>" class="is-centered-responsive">
										<span class="text"><?= $this->lang->line('signup') ?></span>
										<span class="front-gradient"></span>
									</a>

								</div>
							<?php endif; ?>
							<div class="navbar-item is-nav-link">
								<a href="#">
									<img src="<?= base_url('resources/')?>assets/images/icons/flags/ro.png">
								</a>
								<ul id="auxLanguages" class="sub-menu">
									<li>
										<a href="<?= site_url('home/en')?>">
											<span class="language <?= $this->session->userdata('lang') == 'english' ? 'language-active' : '' ?>">English</span>
										</a>
									</li>
									<li>
										<a href="<?= site_url('home/fre')?>">
											<span class="language <?= $this->session->userdata('lang') == 'french' ? 'language-active' : '' ?>">French</span>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>


				</div>
			</nav>
			<!-- /Static navbar -->
		</div>

		<!-- Animated particles -->
		<div id="particles-js"></div>