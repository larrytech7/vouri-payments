
<!-- Hero Image and Title -->
<div class="hero-body">
    <div class="container">
        <div class="columns is-vcentered">

            <!-- Landing page Title -->
            <div class="column is-5 ico-countdown">
                <div class="ico-card animated preFadeInUp fadeInUp">
                    <h4 class="title is-4 is-light is-semibold is-spaced main-title">
                        <?= $this->lang->line('create_account_text') ?>
                    </h4>
                    <h5 class="is-5" style="color: #ff0000;"><?= validation_errors()?></h5>
                    <br>
                    <!-- Form -->
                    <?= form_open(site_url('signup'), ['class' => 'form-default', 'role' => 'form'] )?>
                    <!-- Field -->
                    <div class="control-material is-secondary">
                        <input type="text" name="name" id="name" value="<?= set_value('name') ?>" class="material-input" required>
                        <span class="material-highlight"></span>
                        <span class="bar"></span>
                        <label><?= $this->lang->line('name_text') ?></label>
                    </div>
                    <div class="control-material is-secondary">
                        <input class="material-input " type="email" name="email" value="<?= set_value('email')?>" id="email" required>
                        <span class="material-highlight"></span>
                        <span class="bar"></span>
                        <label><?= $this->lang->line('email') ?></label>
                    </div>
                    <div class="control-material is-secondary">
                        <input type="tel" name="phone" id="phone" value="<?= set_value('phone')?>" class="material-input" required>
                        <span class="material-highlight"></span>
                        <span class="bar"></span>
                        <label><?= $this->lang->line('phone') ?></label>
                    </div>
                    <!-- Field -->
                    <div class="control-material is-secondary">
                        <input class="material-input " type="password" name="password" id="password" required>
                        <span class="material-highlight"></span>
                        <span class="bar"></span>
                        <label><?= $this->lang->line('password') ?></label>
                    </div>
                    <!-- Field -->
                    <div class="control-material is-secondary has-icon-right">
                        <input type="password" name="repassword" id="repassword" class="material-input" required>
                        <span class="material-highlight"></span>
                        <span class="bar"></span>
                        <label for="repassword"><?= $this->lang->line('password_confirm') ?></label>
                    </div>
                    <div class="field">
                        <label class="checkbox">
                            <input type="checkbox" name="check_terms" id="check_terms" required>
                            <?= $this->lang->line('signup_note') ?> <?= anchor(site_url('terms'), 'Terms & Conditions', ['class'=> 'has-text-primary'])?>
                        </label>
                    </div>
                    <div class="field">
                        <div class="g-recaptcha" data-sitekey="6Le10boUAAAAAIYaDKuYesgJRh85inFFGi2EtteG"></div>
                    </div>
                    <!-- Submit -->
                    <div class="has-text-centered">
                        <button class="button is-button k-button k-secondary raised has-gradient is-fat is-bold is-submit">
                            <span class="text"><?= $this->lang->line('signup') ?></span>
                            <span class="front-gradient"></span>
                        </button>
                    </div>
                    <div class="has-text-centered">
                        <br>
                        <span class="title is-6 is-light">
                                <?= $this->lang->line('login_heading') ?>
                            <a href="<?= site_url('home/login')?>" class="has-text-danger"> <?= $this->lang->line('login') ?></a>
                        </span>
                    </div>
                    </form>
                    <!-- CTA -->
                </div>
            </div>
            <!-- Hero image -->
            <div class="column is-7">
                <figure class="image">
                    <span id="typed-text"></span>
                    <img src="<?= base_url('resources/front/');?>assets/images/team/mike.svg" alt="">
                </figure>
            </div>
        </div>
    </div>
</div>

</section>