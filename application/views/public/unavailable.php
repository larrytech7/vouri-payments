
<div class="hero-body has-text-centered">
    <div class="columns">
        <div class="column is-8 is-offset-2">
            <div class="token-image">
                <img src="<?= base_url('resources/front/');?>assets/images/logo/logo.jpg" width="200" height="200" alt="">
            </div>
            <h3 class="subtitle is-3 is-thin has-text-danger">
                <?= $this->lang->line('msg_unavailable')?>
            </h3>
            <!-- CTA -->
            <p>
                <span id="typed-text"></span>
            </p>
            <p>
                <a href="<?= site_url() ?>" class="button is-button k-button k-secondary raised has-gradient is-fat is-bold is-submit" type="submit">
                    <span class="text"><?= $this->lang->line('back') ?></span>
                    <span class="front-gradient"></span>
                </a>
            </p>
        </div>
    </div>
</div>
</section>