<html lang="en">
<head></head>
<body>
<div style="font-family:HelveticaNeue-Light,Arial,sans-serif;background-color:#eeeeee">
	<table align="center" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee">
		<tbody>
		<tr>
			<td>
				<table align="center" width="750px" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee" style="width:750px!important">
					<tbody>
					<tr>
						<td>
							<table width="690" align="center" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee">
								<tbody>
								<tr>
									<td colspan="3" height="80" align="center" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee" style="padding:0;margin:0;font-size:0;line-height:0">
										<table width="690" align="center" border="0" cellspacing="0" cellpadding="0">
											<tbody>
											<tr>
												<td width="30"></td>
												<td align="left" valign="middle" style="padding:0;margin:0;font-size:0;line-height:0">
													<a href="<?= site_url()?>" target="_blank">
														<img src="<?= base_url('resources/assets/images/logo/logo-1-c.png')?>" alt="ElPaga">
													</a>
												</td>
												<td width="30"></td>
											</tr>
											</tbody>
										</table>
									</td>
								</tr>
								<tr>
									<td colspan="3" align="center">
										<table width="630" align="center" border="0" cellspacing="0" cellpadding="0">
											<tbody>
											<tr>
												<td colspan="3" height="60"></td></tr><tr><td width="25"></td>
												<td align="center">
													<h2 style="font-family:HelveticaNeue-Light,arial,sans-serif;font-size:48px;color:#404040;line-height:48px;font-weight:bold;margin:0;padding:0">Vouri Payments</h2>
												</td>
												<td width="25"></td>
											</tr>
											<tr>
												<td colspan="3" height="40"></td>
											</tr>
											<tr>
												<td colspan="5">
													Hello!
													<p align="center" style="color:#404040;font-size:16px;line-height:24px;font-weight:lighter;padding:0;margin:0">
														This email will help you recover your account credentials
													</p><br>
												</td>
											</tr>

											</tbody>
										</table>
									</td>
								</tr>

								<tr bgcolor="#ffffff">
									<td width="30" bgcolor="#eeeeee"></td>
									<td>
										<table width="570" align="center" border="0" cellspacing="0" cellpadding="0">
											<tbody>
											<tr>
												<td colspan="4" align="center">&nbsp;</td>
											</tr>
											<tr>
												<td colspan="4" align="center">
													<h2 style="font-size:24px">Password Recovery</h2>
												</td>
											</tr>
											</tbody>
										</table>
										<table width="570" align="center" border="0" cellspacing="0" cellpadding="0">
											<tbody>
											<tr>
												<td>
													<h2 style="color:#404040;font-size:22px;font-weight:bold;line-height:26px;padding:0;margin:0">&nbsp;</h2>
													<div style="color:#404040;font-size:16px;line-height:22px;font-weight:lighter;padding:0;margin:0">
														You have requested to reset your account password. Click the button below if you are the one who initiated this request
														else disregard or ignore this email if you did not ask to reset your password.
														<br>
														If this was actually not done by you, please contact us to provide more support on the security of your account.
														Thanks!
													</div>
												</td>
											</tr>
											<tr>
												<td align="center">
													<div style="text-align:center;width:100%;padding:40px 0">
														<table align="center" cellpadding="0" cellspacing="0" style="margin:0 auto;padding:0">
															<tbody>
															<tr>
																<td align="center" style="margin:0;text-align:center">
																	<?= anchor($link, 'RESET PASSWORD',
																		['class' => 'btn',
																			'style' => 'font-size:21px;line-height:22px;text-decoration:none;color:#ffffff;font-weight:bold;border-radius:2px;background-color:#0096d3;padding:14px 40px;display:block;letter-spacing:1.2px',
																			'target' =>'_blank'])?>
																</td>
															</tr>
															</tbody>
														</table>
													</div>
												</td>
											</tr><tr><td>&nbsp;</td>
											</tr></tbody></table></td>
									<td width="30" bgcolor="#eeeeee"></td>
								</tr>
								</tbody>
							</table>
							<table align="center" width="750px" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee" style="width:750px!important">
								<tbody>
								<tr>
									<td>
										<table width="630" align="center" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee">
											<tbody>
											<tr><td colspan="2" height="30"></td></tr>
											<tr>
												<td width="360" valign="top">
													<div style="color:#a3a3a3;font-size:12px;line-height:12px;padding:0;margin:0">&copy; <?= date('Y')?> Vouri Inc. All rights reserved.</div>
													<div style="line-height:5px;padding:0;margin:0">&nbsp;</div>
												</td>
											</tr>
											<tr><td colspan="2" height="5"></td></tr>

											</tbody>
										</table>
									</td>
								</tr>
								</tbody>
							</table>
						</td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
		</tbody>
	</table>
</div>
<div id="UMS_TOOLTIP" style="position: absolute; cursor: pointer; z-index: 2147483647; background: transparent; top: -100000px; left: -100000px;"></div>
</body>
<umsdataelement id="UMSSendDataEventElement"></umsdataelement>
</html>