<html lang="en">
<head></head>
<body>
<div style="font-family:HelveticaNeue-Light,Arial,sans-serif;background-color:#eeeeee">
	<table align="center" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee">
		<tbody>
		<tr>
			<td>
				<table align="center" width="750px" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee" style="width:750px!important">
					<tbody>
					<tr>
						<td>
							<table width="690" align="center" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee">
								<tbody>
								<tr>
									<td colspan="3" height="80" align="center" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee" style="padding:0;margin:0;font-size:0;line-height:0">
										<table width="690" align="center" border="0" cellspacing="0" cellpadding="0">
											<tbody>
											<tr>
												<td width="30"></td>
												<td align="left" valign="middle" style="padding:0;margin:0;font-size:0;line-height:0">
													<a href="<?= site_url()?>" target="_blank">
														<img src="<?= base_url('resources/assets/images/logo/logo-trans.png')?>" alt="<?= $this->lang->line('app_name')?>">
													</a>
												</td>
												<td width="30"></td>
											</tr>
											</tbody>
										</table>
									</td>
								</tr>
								<tr>
									<td colspan="3" align="center">
										<table width="630" align="center" border="0" cellspacing="0" cellpadding="0">
											<tbody>
											<tr>
												<td colspan="3" height="60"></td></tr><tr><td width="25"></td>
												<td align="center">
													<h2 style="font-family:HelveticaNeue-Light,arial,sans-serif;font-size:48px;color:#404040;line-height:48px;font-weight:bold;margin:0;padding:0">Vouri Inc</h2>
												</td>
												<td width="25"></td>
											</tr>
											<tr>
												<td colspan="3" height="40"></td>
											</tr>
											<tr>
												<td colspan="5">
													Hello!
													<p align="center" style="color:#404040;font-size:16px;line-height:24px;font-weight:lighter;padding:0;margin:0">
														Here's your transaction summary.
													</p><br>
												</td>
											</tr>

											</tbody>
										</table>
									</td>
								</tr>

								<tr bgcolor="#ffffff">
									<td width="30" bgcolor="#eeeeee"></td>
									<td>
										<table width="570" align="center" border="0" cellspacing="0" cellpadding="0">
											<tbody>
											<tr>
												<td colspan="4" align="center">
													<h2 style="font-size:24px">Transaction</h2>
												</td>
											</tr>

											</tbody>
										</table>
										<table width="570" align="center" border="1" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td>Message</td>
													<td><?= $message ?></td>
												</tr>
												<tr>
													<td>Sending</td>
													<td><?= $amount.' '.$currency ?></td>
												</tr>
												<tr>
													<td>Receiving</td>
													<td><?= $amount_to_receive.' '.$receive_currency ?></td>
												</tr>
												<tr>
													<td>Sent from</td>
													<td><?= $sendfrom ?></td>
												</tr>
												<tr>
													<td>Sent To</td>
													<td><?= $receive_gateway.'<br/>'.$name.' | ' . $phone.' | '. $email ?></td>
												</tr>
												<?php if($receive_gateway == 'VSA'): ?>
													<tr>
														<td>Bank Name </td>
														<td><?= $bank_name ?></td>
													</tr>
													<tr>
														<td>Routing Number</td>
														<td><?= $routing_number ?></td>
													</tr>
													<tr>
														<td>Bank Account Number</td>
														<td><?= $account_number ?></td>
													</tr>
													<tr>
														<td>Account Type</td>
														<td><?= $account_type ?></td>
													</tr>
												<?php endif; ?>
												<tr>
													<td>Transaction status</td>
													<td><?= isset($status) ? $status < 0 ? 'PENDING' : 'SUCCESS' : '' ?></td>
												</tr>
											</tbody>

										</table></td>
									<td width="30" bgcolor="#eeeeee"></td>
								</tr>
								</tbody>
							</table>
							<br>
							<table align="center" width="750px" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee" style="width:750px!important">
								<tbody>
								<tr>
									<td>
										<table width="630" align="center" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee">
											<tbody>
											<tr><td colspan="2" height="30"></td></tr>
											<tr>
												<td width="360" valign="top">
													<div style="color:#a3a3a3;font-size:12px;line-height:12px;padding:0;margin:0">&copy; <?= date('Y')?> <?= $this->lang->line('app_name').' '.$this->lang->line('rights_reserved') ?> </div>
													<div style="line-height:5px;padding:0;margin:0">&nbsp;</div>
												</td>
											</tr>
											<tr><td colspan="2" height="5"></td></tr>

											</tbody>
										</table>
									</td>
								</tr>
								</tbody>
							</table>
						</td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
		</tbody>
	</table>
</div>
<div id="UMS_TOOLTIP" style="position: absolute; cursor: pointer; z-index: 2147483647; background: transparent; top: -100000px; left: -100000px;"></div>
</body>
<umsdataelement id="UMSSendDataEventElement"></umsdataelement>
</html>