<html lang="en">
<head></head>
<body>
<div style="font-family:HelveticaNeue-Light,Arial,sans-serif;background-color:#eeeeee">
	<table align="center" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee">
		<tbody>
		<tr>
			<td>
				<table align="center" width="750px" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee" style="width:750px!important">
					<tbody>
					<tr>
						<td>
							<table width="690" align="center" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee">
								<tbody>
								<tr>
									<td colspan="3" height="80" align="center" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee" style="padding:0;margin:0;font-size:0;line-height:0">
										<table width="690" align="center" border="0" cellspacing="0" cellpadding="0">
											<tbody>
											<tr>
												<td width="30"></td>
												<td align="left" valign="middle" style="padding:0;margin:0;font-size:0;line-height:0">
													<a href="<?= site_url()?>" target="_blank">
														<img src="<?= base_url('resources/assets/images/logo/logo-1-c.png')?>" alt="Vouri Payments	">
													</a>
												</td>
												<td width="30"></td>
											</tr>
											</tbody>
										</table>
									</td>
								</tr>
								<tr>
									<td colspan="3" align="center">
										<table width="630" align="center" border="0" cellspacing="0" cellpadding="0">
											<tbody>
											<tr>
												<td align="center">
													<h2 style="font-family:HelveticaNeue-Light,arial,sans-serif;font-size:48px;color:#404040;line-height:48px;font-weight:bold;margin:0;padding:0">Vouri Payments</h2>
												</td>
											</tr>
											<tr>
												<td colspan="5">
													Hello!
												</td>
											</tr>

											</tbody>
										</table>
									</td>
								</tr>

								<tr bgcolor="#ffffff">
									<td>
										<table width="570" align="center" border="0" cellspacing="0" cellpadding="0">
											<tbody>
											<tr>
												<td>
													<h2 style="color:#404040;font-size:22px;font-weight:bold;line-height:26px;padding:0;margin:0">&nbsp;</h2>
													<div style="color:#404040;font-size:16px;line-height:22px;font-weight:lighter;padding:0;margin:0">
														<?= $message ?>
														<br>
														Thank you for using Vouri.
													</div>
												</td>
											</tr>
											<tr>
												<td>&nbsp;</td>
											</tr>
											</tbody>
										</table>
									</td>
									<td width="30" bgcolor="#eeeeee"></td>
								</tr>
								</tbody>
							</table>
							<table align="center" width="750px" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee" style="width:750px!important">
								<tbody>
								<tr>
									<td>
										<table width="630" align="center" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee">
											<tbody>
											<tr><td colspan="2" height="30"></td></tr>
											<tr>
												<td width="360" valign="top">
													<div style="color:#a3a3a3;font-size:12px;line-height:12px;padding:0;margin:0">&copy; <?= date('Y')?> Vouri Inc. All rights reserved.</div>
													<div style="line-height:5px;padding:0;margin:0">&nbsp;</div>
												</td>
											</tr>
											<tr><td colspan="2" height="5"></td></tr>

											</tbody>
										</table>
									</td>
								</tr>
								</tbody>
							</table>
						</td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
		</tbody>
	</table>
</div>
<div id="UMS_TOOLTIP" style="position: absolute; cursor: pointer; z-index: 2147483647; background: transparent; top: -100000px; left: -100000px;"></div>
</body>
<umsdataelement id="UMSSendDataEventElement"></umsdataelement>
</html>