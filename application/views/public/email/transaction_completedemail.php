<html lang="en">
<head></head>
<body>
<div style="font-family:HelveticaNeue-Light,Arial,sans-serif;background-color:#eeeeee">
	<table align="center" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee">
		<tbody>
		<tr>
			<td>
				<table align="center" width="750px" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee" style="width:750px!important">
					<tbody>
					<tr>
						<td>
							<table width="690" align="center" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee">
								<tbody>
								<tr>
									<td colspan="3" height="80" align="center" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee" style="padding:0;margin:0;font-size:0;line-height:0">
										<table width="690" align="center" border="0" cellspacing="0" cellpadding="0">
											<tbody>
											<tr>
												<td width="30"></td>
												<td align="left" valign="middle" style="padding:0;margin:0;font-size:0;line-height:0">
													<a href="<?= site_url()?>" target="_blank">
														<img src="<?= base_url('resources/assets/images/logo/logo-trans.png')?>" alt="Vouri payments">
													</a>
												</td>
												<td width="30"></td>
											</tr>
											</tbody>
										</table>
									</td>
								</tr>
								<tr>
									<td colspan="3" align="center">
										<table width="630" align="center" border="0" cellspacing="0" cellpadding="0">
											<tbody>
											<tr>
												<td colspan="3" height="60"></td></tr><tr><td width="25"></td>
												<td align="center">
													<h2 style="font-family:HelveticaNeue-Light,arial,sans-serif;font-size:48px;color:#404040;line-height:48px;font-weight:bold;margin:0;padding:0">Vouri  Inc</h2>
												</td>
												<td width="25"></td>
											</tr>
											<tr>
												<td colspan="3" height="40"></td>
											</tr>
											<tr>
												<td colspan="5">
													Hello!
													<p align="center" style="color:#404040;font-size:16px;line-height:24px;font-weight:lighter;padding:0;margin:0">
														Your transaction has just completed. See the details below. <br>
													</p><br>
												</td>
											</tr>

											</tbody>
										</table>
									</td>
								</tr>

								<tr bgcolor="#ffffff">
									<td width="30" bgcolor="#eeeeee"></td>
									<td>
										<table width="570" align="center" border="0" cellspacing="0" cellpadding="0">
											<tbody>
											<tr>
												<td colspan="4" align="center">&nbsp;</td>
											</tr>
											<tr>
												<td colspan="4" align="center">
													<h2 style="font-size:24px">Transaction Completed Details</h2>
												</td>
											</tr>
											</tbody>
										</table>
										<table width="570" align="center" border="0" cellspacing="0" cellpadding="0">
											<tbody>
											<tr>
												<td>
													<h2 style="color:#404040;font-size:22px;font-weight:bold;line-height:26px;padding:0;margin:0">&nbsp;</h2>
													<div style="color:#404040;font-size:16px;line-height:22px;font-weight:lighter;padding:0;margin:0">
														Here are the details of the Transaction
													</div>
												</td>
											</tr>
											<tr>
												<td align="center">
													<div style="width:100%;padding:40px 0">
														<table>
															<tbody>
																<tr>
																	<td>Receiver Email </td>
																	<td><?= $receiver_email ?></td>
																</tr>
																<tr>
																	<td>Receiver Phone</td>
																	<td><?= $receiver_phone ?></td>
																</tr>
																<tr style="background-color: #0000FF">
																	<td></td>
																	<td></td>
																</tr>
																<tr>
																	<td>Amount Sent</td>
																	<td><?= $amount.' '.$currency?></td>
																</tr>
																<tr>
																	<td>Amount Received</td>
																	<td><?= $amount_sent.' '.$receiving_currency?></td>
																</tr>
																<tr>
																	<td>Received VIA</td>
																	<td><?= $receive_gateway?></td>
																</tr>
																<tr>
																	<td>Fee (VAT)</td>
																	<td><?= round((0.04 * $amount),2) .' '.$currency?></td>
																</tr>
																<tr>
																	<td>Message</td>
																	<td><?= $message ?></td>
																</tr>
															</tbody>
														</table>
														<table align="center" cellpadding="0" cellspacing="0" style="margin:0 auto;padding:0">
															<tbody>
															<tr>
																<td align="center" style="margin:0;text-align:center">
																	<?= anchor(site_url('admin'), 'View Transactions',
																		['class' => 'btn',
																			'style' => 'font-size:21px;line-height:22px;text-decoration:none;color:#ffffff;font-weight:bold;border-radius:2px;background-color:#0096d3;padding:14px 40px;display:block;letter-spacing:1.2px',
																			'target' =>'_blank'])?>
																</td>
															</tr>
															</tbody>
														</table>
													</div>
												</td>
											</tr>
											<tr>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td>&nbsp;</td>
											</tr>
											</tbody>
										</table></td>
									<td width="30" bgcolor="#eeeeee"></td>
								</tr>
								</tbody>
							</table>
							<table align="center" width="750px" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee" style="width:750px!important">
								<tbody>
								<tr>
									<td>
										<table width="630" align="center" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee">
											<tbody>
											<tr><td colspan="2" height="30"></td></tr>
											<tr>
												<td width="360" valign="top">
													<div style="color:#a3a3a3;font-size:12px;line-height:12px;padding:0;margin:0">&copy; <?= date('Y')?> Vouri Inc. All rights reserved.</div>
													<div style="line-height:5px;padding:0;margin:0">&nbsp;</div>
												</td>
											</tr>
											<tr><td colspan="2" height="5"></td></tr>

											</tbody>
										</table>
									</td>
								</tr>
								</tbody>
							</table>
						</td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
		</tbody>
	</table>
</div>
<div id="UMS_TOOLTIP" style="position: absolute; cursor: pointer; z-index: 2147483647; background: transparent; top: -100000px; left: -100000px;"></div>
</body>
<umsdataelement id="UMSSendDataEventElement"></umsdataelement>
</html>