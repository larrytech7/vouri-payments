<html lang="en">
<head></head>
<body>
<div style="font-family:HelveticaNeue-Light,Arial,sans-serif;background-color:#eeeeee">
	<table align="center" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee">
		<tbody>
		<tr>
			<td>
				<table align="center" width="750px" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee" style="width:750px!important">
					<tbody>
					<tr>
						<td>
							<table width="690" align="center" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee">
								<tbody>
								<tr>
									<td colspan="3" align="center">
										<table width="630" align="center" border="0" cellspacing="0" cellpadding="0">
											<tbody>
											<tr>
												<td colspan="3" height="60"></td></tr><tr><td width="25"></td>
												<td align="center">
													<h2 style="font-family:HelveticaNeue-Light,arial,sans-serif;font-size:48px;color:#404040;line-height:48px;font-weight:bold;margin:0;padding:0">Vouriinc.com</h2>
												</td>
												<td width="25"></td>
											</tr>
											<tr>
												<td colspan="3" height="40"></td>
											</tr>
											<tr>
												<td colspan="5">
													Hello,
												</td>
											</tr>

											</tbody>
										</table>
									</td>
								</tr>

								<tr bgcolor="#ffffff">
									<td width="30" bgcolor="#eeeeee"></td>
									<td>
										<table width="570" align="center" border="0" cellspacing="0" cellpadding="0">
											<tbody>
											<tr>
												<td colspan="4" align="center">
													<h2 style="font-size:24px">New Contact us Message</h2>
												</td>
											</tr>
											</tbody>
										</table>
										<table width="570" align="center" border="0" cellspacing="0" cellpadding="0">
											<tbody>
											<tr>
												<td>
													<h2 style="color:#404040;font-size:22px;font-weight:bold;line-height:26px;padding:0;margin:0">&nbsp;From</h2>
													<?= $email ?> | <?= $phone ?>
													<h3> Subject : <?= $subject ?></h3>
													<div style="color:#404040;font-size:16px;line-height:22px;font-weight:lighter;padding:0;margin:0">
														<?= $message ?>
													</div>
												</td>
											</tr>
											</tbody>
										</table>
									</td>
									<td width="30" bgcolor="#eeeeee"></td>
								</tr>
								</tbody>
							</table>
							<table align="center" width="750px" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee" style="width:750px!important">
								<tbody>
								<tr>
									<td>
										<table width="630" align="center" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee">
											<tbody>
											<tr>
												<td width="360" valign="top">
													<div style="color:#a3a3a3;font-size:12px;line-height:12px;padding:0;margin:0">&copy; <?= date('Y')?> Vouri Inc. All rights reserved.</div>
													<div style="line-height:5px;padding:0;margin:0">&nbsp;</div>
												</td>
											</tr>
											<tr><td colspan="2" height="5"></td></tr>

											</tbody>
										</table>
									</td>
								</tr>
								</tbody>
							</table>
						</td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
		</tbody>
	</table>
</div>
<div id="UMS_TOOLTIP" style="position: absolute; cursor: pointer; z-index: 2147483647; background: transparent; top: -100000px; left: -100000px;"></div>
</body>
<umsdataelement id="UMSSendDataEventElement"></umsdataelement>
</html>