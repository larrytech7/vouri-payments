<!-- Hero Image and Title -->
<div class="hero-body">
    <div class="container">
        <div class="columns is-vcentered">
            <!-- Landing page Title -->
            <div class="column is-6 ico-countdown">
                <div class="ico-card animated preFadeInUp fadeInUp">
                    <span class="btn btn-icon has-text-white is-light"> <a href="<?= site_url(''); ?>"><i class="ion ion-reply"></i> <?= $this->lang->line('back') ?></a></span>
                    <h4 class="title is-4 is-light is-semibold is-spaced main-title">
                        <?= $this->lang->line('sending_header')?>
                    </h4>
                    <h4 class="title is-4 has-text-centered has-text-success">
                        <span class="text-center" data-toggle="tooltip" title="See transaction fees"><?= $this->lang->line('charge_amount_note') ?> : </span>
                    </h4>
                    <h1 class="title is-1 has-text-centered has-text-white">
                        <span class="is-hidden" id="balancetag"><?= $amount_to_send ?></span>
                        <span class="is-hidden" id="funding_instrument"><?= $sendfrom ?></span>
                        <span id=""><?= number_format($amount_to_send,2) ?></span>
                        <span class="has-text-success" id="ctag"><?= $currency ?></span>
                    </h1>
                    <h4 class="is-4 has-text-centered"><span id="sendtag"></span></h4>
                    <!-- Form -->
                    <?= form_open(site_url('send'), ['class' => 'form-default'
                        , 'role' => 'form'],[
                        'amount_to_send' => $this->input->post('amounttosend'),
                        'currency' => $this->input->post('currency'),
                        'sendfrom' => $this->input->post('sendfrom'),
                    ] )?>
                    <?= validation_errors()?>
                    <!-- Field -->
                    <div class="control has-text-centered">
                        <div class="select is-primary is-medium has-text-centered">
                            <label for="sendto"><?= $this->lang->line('sending_source') ?>
                                <span class="icon has-text-info">
                                  <i class="fas fa-info-circle"></i>
                                </span>
                            </label>
                            <select id="sendto" name="sendto" class="material-input input is-primary">
                                <option disabled>SELECT OPTION</option>
                                <option value="MMO" selected>MTN MOBILE MONEY</option>
                                <option value="OMO">ORANGE MONEY </option>
                                <option value="VSA">BANK ACCOUNT (Only for USA)</option>
                            </select>
                            <span class="material-highlight"></span>
                            <span class="bar"></span>
                        </div>
                    </div>
                    <div class="archives-widget">
                        <div class="widget-title">
                            Select
                        </div>
                        <div class="select" tabindex="1">

                        </div>
                    </div>
                    <br>
                    <!-- Field -->
                    <div class="control-material is-secondary">
                        <input class="material-input " type="text" name="name" id="name" required>
                        <span class="material-highlight"></span>
                        <span class="bar"></span>
                        <label><?= $this->lang->line('receiver_name') ?></label>
                    </div>
                    <div class="control-material is-secondary">
                        <input class="material-input " type="email" name="email" id="email">
                        <span class="material-highlight"></span>
                        <span class="bar"></span>
                        <label><?= $this->lang->line('email') ?></label>
                    </div>
                    <div class="control-material is-secondary">
                        <input class="material-input " type="number" name="phone" id="phone" required>
                        <span class="material-highlight"></span>
                        <span class="bar"></span>
                        <label><?= $this->lang->line('phone_number') ?></label>
                    </div>
                    <div class="control-material is-secondary">
                        <textarea rows="3" name="message" id="message" placeholder="<?= $this->lang->line('message')?>"></textarea>
                        <span class="material-highlight"></span>
                        <span class="bar"></span>
                        <label><?= $this->lang->line('message_title') ?></label>
                    </div>
                    <!-- Submit -->
                    <?php if($this->session->has_userdata('loggedin') && $isCompliant): ?>
                        <?php if($sendfrom == 'MMO' || $sendfrom == 'OMO'): ?>
                            <button class="button is-button k-button k-secondary raised has-gradient is-fat is-bold is-submit">
                                <span class="text"><?= $this->lang->line('send') ?></span>
                                <span class="front-gradient"></span>
                            </button>
                        <?php else: ?>
                            <div class="has-text-centered">
                                <button class="button is-button k-button k-secondary raised has-gradient is-fat is-bold is-submit" type="submit">
                                    <span class="text"><?= $this->lang->line('continue') ?></span>
                                    <span class="front-gradient"></span>
                                </button>
                            </div>
                        <?php endif; ?>
                        <?php elseif(!$isCompliant):?>
                    <?php else:  ?>
                        <?php $this->session->set_userdata('temp_url', site_url('sendfunds')); ?>
                        <div class="has-text-centered">
                            <a href="<?= site_url('login') ?>" class="button is-button k-secondary raised has-gradient is-fat is-bold is-submit">
                                <i class="ion ion-android-lock icon-lg"></i>
                                <?= $this->lang->line('login_transact') ?>
                            </a>
                        </div>
                    <?php endif; ?>

                        <div class="has-text-centered"><br>
                            <span class="title is-5 is-light">
                                    <?= $this->lang->line('no_account_text') ?>
                                <a href="<?= site_url('home/signup')?>" class="has-text-primary"> <?= $this->lang->line('signup') ?></a>
                            </span>
                                <br>
                            <span class="title is-5 is-light">
                                <a href="#" class="has-text-success" id="recover_account" title="Recover your account password"><?= $this->lang->line('lost_password') ?></a>
                            </span>
                        </div>
                    </form>
                    <!-- CTA -->
                </div>
            </div>
            <!-- Hero image -->
            <div class="column is-6">
                <figure class="image">
                    <span id="typed-text"></span>
                    <img src="<?= base_url('resources/front/');?>assets/images/illustrations/token-lock.svg" alt="">
                </figure>
            </div>
        </div>
    </div>
</div>

</section>

<!--
                        <div class="row bank d-none">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="bank_name">Bank Name </label>  <i class="ion icon-info pull-right" data-placement="right" data-toggle="tooltip" title="Name of the Bank (USA only)"></i>
                                    <div class="input-group">
                                        <input type="text" name="bank_name" id="bank_name" class="form-control form-control-lg" placeholder="Bank Name (USA only)">
                                        <span class="input-group-addon">
                                            <i class="ion ion-ios-home-outline"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="routing">Routing Number </label>  <i class="ion icon-info pull-right" data-placement="right" data-toggle="tooltip" title="Routing Number (ABA)"></i>
                                    <div class="input-group">
                                        <input type="text" name="routing_number" id="routing" class="form-control form-control-lg" placeholder="Routing Number (ABA)">
                                        <span class="input-group-addon">
                                            <i class="ion ion-code-working"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="account">Account Number </label>  <i class="ion icon-info pull-right" data-placement="right" data-toggle="tooltip" title="ACCOUNT Number"></i>
                                    <div class="input-group">
                                        <input type="text" name="account_number" id="account" class="form-control form-control-lg" placeholder="Account Number (17+- digits)">
                                        <span class="input-group-addon">
                                            <i class="ion ion-ios-compose"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="type">Account Type </label>  <i class="ion icon-info pull-right" data-placement="right" data-toggle="tooltip" title="Bank Account type"></i>
                                    <div class="input-group">
                                        <input type="text" name="account_type" id="type" class="form-control form-control-lg" placeholder="Account Type (e.g CHECKING, CURRENT, SAVINGS ...)">
                                        <span class="input-group-addon">
                                            <i class="ion ion-android-bookmark"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
-->