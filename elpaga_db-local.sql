-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 18, 2019 at 10:54 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `elpaga_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `contact_messages`
--

DROP TABLE IF EXISTS `contact_messages`;
CREATE TABLE IF NOT EXISTS `contact_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `subject` mediumtext NOT NULL,
  `message` text NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delete_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 COMMENT='hold contact us messages received from website';

--
-- Dumping data for table `contact_messages`
--

INSERT INTO `contact_messages` (`id`, `email`, `phone`, `subject`, `message`, `create_time`, `update_time`, `delete_time`) VALUES
(9, 'clokam16@gmail.com', '678656032', 'Hello', 'ok lst', '2019-02-25 09:56:15', '2019-02-25 09:56:15', NULL),
(8, 'clokam16@gmail.com', '9802989359', 'demoing', 'Hello, Trying this thing out', '2019-02-25 09:50:46', '2019-02-25 09:50:46', NULL),
(7, 'larryakah@gmail.com', '678656032', 'Does it work', 'Help reduce fees', '2019-02-08 10:41:07', '2019-02-08 10:41:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
CREATE TABLE IF NOT EXISTS `notification` (
  `message_id` varchar(255) NOT NULL,
  `sender` varchar(200) DEFAULT NULL,
  `receiver` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delete_time` datetime DEFAULT NULL,
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='reviews table holding reviews about given articles on the site';

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`message_id`, `sender`, `receiver`, `message`, `created_time`, `updated_time`, `delete_time`) VALUES
('2a7832537660aa49266cb37c75a41ca3335b1f3e', 'Vouri Payments', '15rPRcBV58kmk', 'ok testing  mass email once more', '2019-03-12 12:49:58', '2019-03-12 12:49:58', NULL),
('56bd0a5222ca9f706e74b48180c003fd0b3e6130', 'Vouri Payments', '15rPRcBV58kmk', 'demo testing mass emails', '2019-03-12 12:48:08', '2019-03-12 12:48:08', NULL),
('8b6af53fe2fdb4f36fef2755ad62b87a47cc0c16', 'Vouri Payments', '15rPRcBV58kmk', 'last testing mass email', '2019-03-12 13:02:36', '2019-03-12 13:02:36', NULL),
('b6e0fe4aab40a8a8efc09f86e1b54b109b7d5d1d', 'Vouri Payments', '15mTrGYglxEzc', 'ok testing  mass email once more', '2019-03-12 12:49:58', '2019-03-12 12:49:58', NULL),
('be71891ae94f2be2acedabc8b21afbcd35a14332', 'Vouri Payments', '15mTrGYglxEzc', 'demo testing mass emails', '2019-03-12 12:48:08', '2019-03-12 12:48:08', NULL),
('ebbc8bbff4f82503c56b99b566c2aa6173ce7252', 'Vouri Payments', '15mTrGYglxEzc', 'last testing mass email', '2019-03-12 13:02:36', '2019-03-12 13:02:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `payment_instrument`
--

DROP TABLE IF EXISTS `payment_instrument`;
CREATE TABLE IF NOT EXISTS `payment_instrument` (
  `instrument_id` varchar(255) NOT NULL,
  `instrument_name` varchar(255) DEFAULT NULL,
  `instrument_number` varchar(255) NOT NULL,
  `instrument_fingerprint` varchar(255) NOT NULL,
  `instrument_type` varchar(255) NOT NULL,
  `instrument_expiration` varchar(255) NOT NULL,
  `instrument_origin` varchar(255) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delete_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`instrument_id`(50)),
  UNIQUE KEY `fingerprint_number index` (`instrument_fingerprint`(50),`instrument_number`(50))
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='hold meta data reference to customer payment instrument';

--
-- Dumping data for table `payment_instrument`
--

INSERT INTO `payment_instrument` (`instrument_id`, `instrument_name`, `instrument_number`, `instrument_fingerprint`, `instrument_type`, `instrument_expiration`, `instrument_origin`, `create_time`, `update_time`, `delete_time`) VALUES
('card_1E9CYKIU3jG2iY8t1dzScRdc\r\n', 'ngouacharles@yahoo.fr\r\n', '•••• 6928', 'cfS2SwvTjT7IRuRP', 'Visa credit card', '12 / 2019', 'Argentina', '2019-03-24 15:01:30', '2019-03-24 15:01:30', NULL),
('card_1E9CVVIU3jG2iY8t5zK4STtg\r\n', 'ngouacharles@yahoo.fr\r\n', '•••• 3955\r\n', '174dUHKfvFOEjcqu', 'Visa credit card\r\n', '10 / 2021\r\n', 'Argentina', '2019-03-24 15:43:20', '2019-03-24 15:43:20', NULL),
('card_1E9BRbIU3jG2iY8t6rGZojyU\r\n', 'ngouacharles@yahoo.fr\r\n', '•••• 0221\r\n', 'kJJx9TltFmLLJzWi', 'MasterCard credit card\r\n', '3 / 2020\r\n', 'Mexico', '2019-03-24 15:49:33', '2019-03-24 15:49:33', NULL),
('card_1E88HCIU3jG2iY8tCQQA6gQ6\r\n', 'chasles25@gmail.com\r\n', '•••• 8969\r\n', 'GJHV0ELohoLbzlfC', 'MasterCard credit card\r\n', '3 / 2021\r\n', 'France', '2019-03-24 15:49:33', '2019-03-24 15:49:33', NULL),
('card_1E8TvKIU3jG2iY8tndosTDdf', 'chasles25@gmail.com', '•••• 1009', 'l3nVtdVpLnVjUAgz', 'MasterCard credit card\r\n', '11 / 2020', 'Brazil', '2019-03-24 15:57:12', '2019-03-24 15:57:12', NULL),
('card_1E1xi6IU3jG2iY8tf6yv1XkC', 'chasles25@gmail.com\r\n', '•••• 6874\r\n', 'GvMxCIV1otPaaZ3w', 'Visa debit card\r\n', '1 / 2021\r\n', 'Cameroon', '2019-03-24 15:57:12', '2019-03-24 15:57:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `payment_requests`
--

DROP TABLE IF EXISTS `payment_requests`;
CREATE TABLE IF NOT EXISTS `payment_requests` (
  `request_id` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `amount` float NOT NULL,
  `gateway` varchar(128) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `message` text,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delete_time` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`request_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='hold data for payment requests sent via application';

--
-- Dumping data for table `payment_requests`
--

INSERT INTO `payment_requests` (`request_id`, `email`, `currency`, `amount`, `gateway`, `name`, `message`, `create_time`, `update_time`, `delete_time`, `status`) VALUES
('$1$A1TNiRfi$k8QuMpwiFySw5zbKl5odT.', 'larryakah@gmail.com', 'USD', 10000000, 'OMO', NULL, 'Yes there is just to reason', '2019-01-28 21:48:32', '2019-01-28 21:48:32', NULL, 1),
('$1$AYadzGVD$JUo6sx32Gg6OutgRY1F0W0', 'ndemeyvan@gmail.com', 'GBP', 5000, 'MMO', NULL, 'yes', '2019-02-18 06:35:00', '2019-02-18 06:35:00', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
CREATE TABLE IF NOT EXISTS `transactions` (
  `transaction_id` bigint(20) NOT NULL,
  `amount` float NOT NULL,
  `amount_sent` float NOT NULL,
  `currency` varchar(3) NOT NULL,
  `receiving_currency` varchar(15) DEFAULT 'XAF',
  `sender_id` varchar(255) NOT NULL,
  `receiver_phone` varchar(255) DEFAULT NULL,
  `receiver_email` varchar(255) DEFAULT NULL,
  `receiver_name` varchar(255) DEFAULT NULL,
  `send_gateway` varchar(45) NOT NULL,
  `receive_gateway` varchar(45) NOT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_route` int(11) DEFAULT NULL,
  `bank_account_number` int(11) DEFAULT NULL,
  `bank_account_type` varchar(255) DEFAULT NULL,
  `message` text,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delete_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`transaction_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`transaction_id`, `amount`, `amount_sent`, `currency`, `receiving_currency`, `sender_id`, `receiver_phone`, `receiver_email`, `receiver_name`, `send_gateway`, `receive_gateway`, `bank_name`, `bank_route`, `bank_account_number`, `bank_account_type`, `message`, `status`, `create_time`, `update_time`, `delete_time`) VALUES
(4587194407, 204, 115499, 'USD', 'XAF', '15rPRcBV58kmk', '678656032', 'larryakah@gmail.com', NULL, 'VSA', 'OMO', NULL, NULL, NULL, NULL, NULL, 0, '2019-01-07 19:16:47', '2019-01-07 19:16:47', NULL),
(3775770117, 506, 329290, 'EUR', 'XAF', '15rPRcBV58kmk', '678656032', 'larryakah@gmail.com', NULL, 'VSA', 'MMO', NULL, NULL, NULL, NULL, NULL, 0, '2018-12-28 23:56:44', '2018-12-28 23:56:44', NULL),
(3605457379, 104, 100, 'USD', 'USD', '15rPRcBV58kmk', '678656032', 'larryakah@gmail.com', NULL, 'VSA', 'STR', NULL, NULL, NULL, NULL, NULL, 0, '2019-01-07 16:24:41', '2019-01-07 16:24:41', NULL),
(3168703707, 5100, 8.65, 'XAF', 'USD', '15rPRcBV58kmk', '678656032', 'larryakah@gmail.com', NULL, 'MMO', 'STR', NULL, NULL, NULL, NULL, NULL, -1, '2018-12-28 23:24:50', '2018-12-28 23:25:03', NULL),
(3274165914, 304, 300, 'USD', 'USD', '15rPRcBV58kmk', '678656032', 'larryakah@gmail.com', NULL, 'MMO', 'STR', NULL, NULL, NULL, NULL, NULL, -1, '2018-12-28 23:23:17', '2018-12-28 23:23:29', NULL),
(4361786832, 510, 136, 'AED', 'USD', '15rPRcBV58kmk', '+237678656032', 'larryakah@gmail.com', NULL, 'VSA', 'STR', NULL, NULL, NULL, NULL, NULL, 0, '2019-01-09 23:16:37', '2019-01-09 23:16:37', NULL),
(3632586427, 103, 100, 'USD', 'USD', '15rPRcBV58kmk', '695032710', 'larryakah@gmail.com', NULL, 'VSA', 'STR', NULL, NULL, NULL, NULL, NULL, -1, '2019-01-22 09:25:09', '2019-01-22 09:25:28', NULL),
(3730113276, 103, 57749.3, 'USD', 'XAF', '15rPRcBV58kmk', '697285032', 'icep603@gmail.com', NULL, 'VSA', 'OMO', NULL, NULL, NULL, NULL, NULL, -1, '2019-01-22 10:02:56', '2019-01-22 10:02:58', NULL),
(3144424053, 103, 57749.3, 'USD', 'XAF', '15rPRcBV58kmk', '698203252', 'larryakah@gmail.com', NULL, 'VSA', 'OMO', NULL, NULL, NULL, NULL, NULL, 0, '2019-01-22 10:11:49', '2019-01-22 10:11:49', NULL),
(3527239200, 104, 57749.3, 'USD', 'XAF', '15rPRcBV58kmk', '678656032', 'icep603@gmail.com', NULL, 'VSA', 'MMO', NULL, NULL, NULL, NULL, NULL, 0, '2019-01-22 10:33:48', '2019-01-22 10:33:48', NULL),
(4254716110, 104, 57749.3, 'USD', 'XAF', '15rPRcBV58kmk', '678656032', 'icep603@gmail.com', NULL, 'VSA', 'MMO', NULL, NULL, NULL, NULL, NULL, 0, '2019-01-22 10:35:14', '2019-01-22 10:35:14', NULL),
(4167226628, 104, 73507.5, 'GBP', 'XAF', '15rPRcBV58kmk', '678656032', 'icep603@gmail.com', NULL, 'VSA', 'MMO', NULL, NULL, NULL, NULL, NULL, 0, '2019-01-22 10:44:43', '2019-01-22 10:44:43', NULL),
(4259095543, 104.3, 73507.5, 'GBP', 'XAF', '15rPRcBV58kmk', '678656032', 'icep603@gmail.com', NULL, 'VSA', 'MMO', NULL, NULL, NULL, NULL, NULL, 0, '2019-01-22 11:11:18', '2019-01-22 11:11:18', NULL),
(3799270348, 1040.3, 735075, 'GBP', 'XAF', '15rPRcBV58kmk', '678656032', 'icep603@gmail.com', NULL, 'VSA', 'MMO', NULL, NULL, NULL, NULL, NULL, 0, '2019-01-22 11:13:50', '2019-01-22 11:13:50', NULL),
(3883435104, 208.5, 200, 'USD', 'USD', '15rPRcBV58kmk', '678656032', 'ndemeyvan@gmail.com', NULL, 'VSA', 'STR', NULL, NULL, NULL, NULL, NULL, 0, '2019-01-22 11:19:33', '2019-01-22 11:19:33', NULL),
(4344716243, 260.5, 107580, 'CAD', 'XAF', '15rPRcBV58kmk', '678656032', 'ndemeyvan@gmail.com', NULL, 'VSA', 'MMO', NULL, NULL, NULL, NULL, NULL, 0, '2019-01-22 11:31:30', '2019-01-22 11:31:30', NULL),
(3249395121, 208.5, 131191, 'EUR', 'XAF', '15rPRcBV58kmk', '678656032', 'icep603@gmail.com', NULL, 'VSA', 'MMO', NULL, NULL, NULL, NULL, NULL, 0, '2019-01-22 12:07:47', '2019-01-22 12:07:47', NULL),
(4441493515, 208.5, 110590, 'USD', 'XAF', '15rPRcBV58kmk', '678656032', 'icep603@gmail.com', NULL, 'VSA', 'OMO', NULL, NULL, NULL, NULL, NULL, 0, '2019-01-22 12:19:30', '2019-01-22 12:19:30', NULL),
(3221681257, 104.5, 55150.6, 'USD', 'XAF', '15rPRcBV58kmk', '678656032', 'larryakah@gmail.com', NULL, 'VSA', 'MMO', NULL, NULL, NULL, NULL, NULL, 0, '2019-01-22 13:28:00', '2019-01-22 13:28:00', NULL),
(3482674406, 208.5, 29698.8, 'AED', 'XAF', '15rPRcBV58kmk', '678656032', 'icep603@gmail.com', NULL, 'VSA', 'OMO', NULL, NULL, NULL, NULL, NULL, 0, '2019-01-22 13:34:57', '2019-01-22 13:34:57', NULL),
(4281339921, 104.5, 55150.6, 'USD', 'XAF', '15rPRcBV58kmk', '678656032', 'icep603@gmail.com', NULL, 'VSA', 'MMO', NULL, NULL, NULL, NULL, NULL, 0, '2019-01-22 14:29:44', '2019-01-22 14:29:44', NULL),
(3567343772, 104.5, 41095, 'CAD', 'XAF', '15rPRcBV58kmk', '678656032', 'ndemeyvan@gmail.com', NULL, 'VSA', 'MMO', NULL, NULL, NULL, NULL, NULL, 0, '2019-01-22 14:37:29', '2019-01-22 14:37:29', NULL),
(3760885374, 104.5, 55151, 'USD', 'XAF', '15rPRcBV58kmk', '678656032', 'icep603@gmail.com', NULL, 'VSA', 'MMO', NULL, NULL, NULL, NULL, NULL, -1, '2019-01-22 16:33:19', '2019-01-22 16:33:24', NULL),
(4200499141, 104.5, 55151, 'USD', 'XAF', '15rPRcBV58kmk', '678656032', 'larryakah@gmail.com', NULL, 'VSA', 'MMO', NULL, NULL, NULL, NULL, NULL, 0, '2019-01-22 16:34:37', '2019-01-22 16:34:37', NULL),
(3586198112, 104.5, 55151, 'USD', 'XAF', '15rPRcBV58kmk', '678656032', 'larryakah@gmail.com', NULL, 'VSA', 'MMO', NULL, NULL, NULL, NULL, NULL, -1, '2019-01-22 16:35:06', '2019-01-22 16:35:08', NULL),
(4398292908, 104.5, 55151, 'USD', 'XAF', '15rPRcBV58kmk', '678656032', 'larryakah@gmail.com', NULL, 'VSA', 'MMO', NULL, NULL, NULL, NULL, NULL, -1, '2019-01-22 16:38:38', '2019-01-22 16:38:39', NULL),
(3820283866, 104.5, 55151, 'USD', 'XAF', '15rPRcBV58kmk', '678656032', 'larryakah@gmail.com', NULL, 'VSA', 'MMO', NULL, NULL, NULL, NULL, NULL, -1, '2019-01-22 16:42:37', '2019-01-22 16:42:39', NULL),
(3872468378, 104.5, 55151, 'USD', 'XAF', '15rPRcBV58kmk', '9802989359', 'icep603@gmail.com', NULL, 'VSA', 'OMO', NULL, NULL, NULL, NULL, NULL, 0, '2019-01-22 18:36:08', '2019-01-22 18:36:08', NULL),
(3322528720, 104.5, 55151, 'USD', 'XAF', '15rPRcBV58kmk', '9802989359', 'icep603@gmail.com', NULL, 'VSA', 'OMO', NULL, NULL, NULL, NULL, NULL, 0, '2019-01-22 18:42:35', '2019-01-22 18:42:35', NULL),
(3195597063, 104.5, 62644, 'EUR', 'XAF', '15rPRcBV58kmk', '678656032', 'icep603@gmail.com', NULL, 'VSA', 'MMO', NULL, NULL, NULL, NULL, NULL, 1, '2019-01-22 18:43:52', '2019-02-04 11:40:57', NULL),
(3761186375, 260.5, 103061, 'CAD', 'XAF', '15rPRcBV58kmk', '678656032', 'larryakah@gmail.com', NULL, 'VSA', 'MMO', NULL, NULL, NULL, NULL, NULL, -1, '2019-01-22 18:44:56', '2019-01-22 18:45:00', NULL),
(3695680074, 104.5, 55151, 'USD', 'XAF', '15rPRcBV58kmk', '678656032', 'larryakah@gmail.com', NULL, 'VSA', 'OMO', NULL, NULL, NULL, NULL, NULL, -1, '2019-01-22 18:50:21', '2019-01-22 18:50:25', NULL),
(3219731965, 312.3, 211334, 'GBP', 'XAF', '15rPRcBV58kmk', '678656032', 'larryakah@gmail.com', NULL, 'VSA', 'MMO', NULL, NULL, NULL, NULL, NULL, -1, '2019-01-22 18:51:17', '2019-01-22 18:51:19', NULL),
(3469712925, 104.5, 55151, 'USD', 'XAF', '15rPRcBV58kmk', '678656032', 'larryakah@gmail.com', NULL, 'VSA', 'MMO', NULL, NULL, NULL, NULL, NULL, 0, '2019-01-22 19:05:14', '2019-01-22 19:05:14', NULL),
(3360493424, 260.5, 157102, 'EUR', 'XAF', '15rPRcBV58kmk', '678656032', 'larryakah@gmail.com', NULL, 'VSA', 'MMO', NULL, NULL, NULL, NULL, NULL, 0, '2019-01-22 19:22:33', '2019-01-22 19:22:33', NULL),
(4424375866, 5100, 8, 'XAF', 'USD', '15rPRcBV58kmk', '678656032', 'larryakah@gmail.com', NULL, 'MMO', 'STR', NULL, NULL, NULL, NULL, NULL, 0, '2019-01-22 19:37:53', '2019-01-22 19:37:53', NULL),
(4348127210, 5100, 8, 'XAF', 'USD', '15rPRcBV58kmk', '678656032', 'larryakah@gmail.com', NULL, 'MMO', 'STR', NULL, NULL, NULL, NULL, NULL, 0, '2019-01-22 19:40:03', '2019-01-22 19:40:03', NULL),
(3734128684, 208.5, 110590, 'USD', 'XAF', '15rPRcBV58kmk', '678656032', 'larryakah@gmail.com', NULL, 'VSA', 'MMO', NULL, NULL, NULL, NULL, NULL, 0, '2019-01-24 14:25:21', '2019-01-24 14:25:21', NULL),
(3703975069, 104.5, 55151, 'USD', 'XAF', '15rPRcBV58kmk', '678656032', 'larryakah@gmail.com', NULL, 'VSA', 'MMO', NULL, NULL, NULL, NULL, NULL, 0, '2019-01-25 19:54:22', '2019-01-25 19:54:22', NULL),
(3922028316, 104.5, 62644, 'EUR', 'XAF', '15rPRcBV58kmk', '678656032', 'larryakah@gmail.com', NULL, 'VSA', 'MMO', NULL, NULL, NULL, NULL, NULL, 0, '2019-01-25 19:57:23', '2019-01-25 19:57:23', NULL),
(4462220727, 2080.5, 1259110, 'EUR', 'XAF', '15rPRcBV58kmk', '678656032', 'larryakah@gmail.com', NULL, 'VSA', 'MMO', NULL, NULL, NULL, NULL, NULL, 0, '2019-01-27 15:30:52', '2019-01-27 15:30:52', NULL),
(3360012810, 104.5, 57363, 'USD', 'XAF', '15rPRcBV58kmk', '678656032', 'larryakah@gmail.com', NULL, 'VSA', 'MMO', NULL, NULL, NULL, NULL, NULL, 0, '2019-02-07 16:50:01', '2019-02-07 16:50:01', NULL),
(3136349803, 104.5, 57363, 'USD', 'XAF', '15rPRcBV58kmk', '678656032', 'larryakah@gmail.com', NULL, 'VSA', 'OMO', NULL, NULL, NULL, NULL, NULL, 0, '2019-02-07 16:57:12', '2019-02-07 16:57:12', NULL),
(4479587099, 104.5, 77, 'USD', 'GBP', '15rPRcBV58kmk', '678656032', 'fossapaulmartin@gmail.com', 'Mina Wen', 'VSA', 'Bank Account', 'GT Bank', 500236, 2147483647, 'CHECKING', NULL, 1, '2019-02-20 12:07:11', '2019-02-26 22:52:23', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` varchar(255) NOT NULL,
  `role` varchar(45) DEFAULT 'subscriber',
  `name` varchar(255) DEFAULT NULL COMMENT 'name of user type',
  `phone` varchar(15) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` text NOT NULL,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `delete_time` timestamp NULL DEFAULT NULL,
  `logged_in` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'logged-in status. 1 or 0',
  `auth_token` text COMMENT 'authentitcation token for verification',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'determines user status. Account confirmed or not',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='hold admin user credentials';

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `role`, `name`, `phone`, `email`, `password`, `create_time`, `update_time`, `delete_time`, `logged_in`, `auth_token`, `status`) VALUES
('15rPRcBV58kmk', 'admin', 'Akah Harvey n', '678656032', 'larryakah@gmail.com', '$2y$10$3d4f2bf07dc1be38b20cduaGshiNz1DEbYcDZfGMzz5RHW/N/U3Im', '2019-01-09 23:06:34', '2019-03-24 12:11:16', NULL, 0, NULL, 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
