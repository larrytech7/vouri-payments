#!/bin/bash

# Install system packages
apt-get update && apt-get -y install git gnupg2 netcat apt-transport-https \
    lsb-release ca-certificates libmcrypt-dev libsqlite3-dev libfreetype6-dev \
    libjpeg62-turbo-dev libpng-dev zlib1g-dev software-properties-common wget

# Install PHP-Docker extensions
docker-php-ext-install pdo_mysql mysqli
docker-php-ext-install mbstring mcrypt pdo_pgsql curl json intl gd xml zip bz2 opcache
docker-php-ext-enable pdo_mysql mysqli
docker-php-ext-configure pdo_mysql --with-pdo-mysql=mysqlnd

# install mysql client
apt-get -y install default-mysql-client

# Add code coverage tooling
pecl install xdebug && docker-php-ext-enable xdebug

